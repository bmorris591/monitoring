package uk.co.borismorris.monitoring.influxclient

import com.influxdb.client.InfluxDBClientOptions
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.boot.autoconfigure.service.connection.ConnectionDetails
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.client.RestClient

interface InfluxdbConnectionDetails : ConnectionDetails {
    val url: String?
    val token: String?
    val bucket: String?
    val org: String?
}

data class PropertiesInfluxdbConnectionDetails(
    override val url: String?,
    override val token: String?,
    override val bucket: String?,
    override val org: String?,
) : InfluxdbConnectionDetails {
    constructor(properties: InfluxDB2Properties) : this(properties.url, properties.token, properties.bucket, properties.org)
}

@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties(InfluxDB2Properties::class)
class InfluxConnectionDetailsAutoconfiguration {
    @ConditionalOnMissingBean
    @Bean
    fun influxDbConnectionDetails(properties: InfluxDB2Properties): InfluxdbConnectionDetails = PropertiesInfluxdbConnectionDetails(properties)
}

@Configuration(proxyBeanMethods = false)
class InfluxClientAutoConfiguration {
    @Bean
    fun influxConfig(connectionDetails: InfluxdbConnectionDetails): InfluxDBClientOptions {
        val builder = InfluxDBClientOptions.builder()
            .apply {
                connectionDetails.url?.let { url(it) }
                connectionDetails.token?.let { authenticateToken(it.toCharArray()) }
            }
            .bucket(connectionDetails.bucket)
            .org(connectionDetails.org)
        val ctor = InfluxDBClientOptions::class.java.getDeclaredConstructor(InfluxDBClientOptions.Builder::class.java)
        ctor.trySetAccessible()
        return ctor.newInstance(builder)
    }

    @Bean
    @ConditionalOnMissingBean
    fun influxClient(influxConfig: InfluxDBClientOptions, restClientBuilder: RestClient.Builder) = RestClientInfluxClient(restClientBuilder, influxConfig)
}
