package uk.co.borismorris.monitoring.influxclient

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonValue
import com.fasterxml.jackson.databind.json.JsonMapper
import com.influxdb.Cancellable
import com.influxdb.client.InfluxDBClientOptions
import com.influxdb.client.domain.Dialect
import com.influxdb.client.domain.Dialect.AnnotationsEnum
import com.influxdb.client.domain.Dialect.AnnotationsEnum.DATATYPE
import com.influxdb.client.domain.Dialect.AnnotationsEnum.DEFAULT
import com.influxdb.client.domain.Dialect.AnnotationsEnum.GROUP
import com.influxdb.client.domain.Query
import com.influxdb.client.domain.Query.TypeEnum
import com.influxdb.client.internal.AbstractWriteClient.BatchWriteData
import com.influxdb.client.internal.AbstractWriteClient.BatchWriteDataPoint
import com.influxdb.client.write.Point
import com.influxdb.client.write.WriteParameters
import com.influxdb.query.FluxTable
import com.influxdb.query.internal.FluxCsvParser
import okio.buffer
import okio.source
import org.springframework.core.io.Resource
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.MediaType.ALL
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.http.MediaType.TEXT_PLAIN
import org.springframework.http.converter.json.AbstractJackson2HttpMessageConverter
import org.springframework.web.client.RestClient
import org.springframework.web.client.body
import uk.co.borismorris.monitoring.logging.KotlinLogging
import java.nio.charset.StandardCharsets.UTF_8

private val logger = KotlinLogging.logger {}
private val DEFAULT_DIALECT = Dialect()
    .annotations(listOf(DATATYPE, GROUP, DEFAULT))

class RestClientInfluxClient(
    restClient: RestClient.Builder,
    private val options: InfluxDBClientOptions,
) : InfluxClient {

    private val fluxCsvParser = FluxCsvParser()
    private val mapper = JsonMapper.builder()
        .findAndAddModules()
        .addMixIn(TypeEnum::class.java, InfluxEnumMixin::class.java)
        .addMixIn(AnnotationsEnum::class.java, InfluxEnumMixin::class.java)
        .serializationInclusion(JsonInclude.Include.NON_NULL)
        .build()
    private val restClient = restClient
        .messageConverters { converters ->
            converters.find { it is AbstractJackson2HttpMessageConverter }?.let { converter ->
                (converter as AbstractJackson2HttpMessageConverter).registerObjectMappersForType(Query::class.java) {
                    it[ALL] = mapper
                }
            }
        }
        .baseUrl(options.url)
        .also { builder ->
            options.token?.let {
                builder.defaultHeader(HttpHeaders.AUTHORIZATION, "Token ${String(it)}")
            }
        }
        .build()

    override fun query(query: String): List<FluxTable> = restClient.post()
        .uri {
            it.path("api/v2/query")
                .queryParam("org", options.org)
                .build()
        }
        .contentType(APPLICATION_JSON)
        .body(Query().query(query).dialect(DEFAULT_DIALECT))
        .retrieve()
        .body<Resource>()?.let {
            val consumer = fluxCsvParser.FluxResponseConsumerTable()
            it.inputStream.use { stream ->
                fluxCsvParser.parseFluxResponse(stream.source().buffer(), noopCancellable, consumer)
            }
            consumer.tables
        } ?: error("No response for query $query")

    override fun writePoints(points: List<Point>, parameters: WriteParameters) {
        parameters.check(options)
        points
            .groupBy { it.precision }
            .forEach { (precision, grouped) ->
                val groupParameters = parameters.copy(precision, options)
                write(
                    groupParameters,
                    grouped.asSequence().map { BatchWriteDataPoint(it, options) },
                )
            }
    }

    private fun write(parameters: WriteParameters, stream: Sequence<BatchWriteData>) {
        val lineProtocol = stream.map { it.toLineProtocol() }
            .filterNot { it.isNullOrEmpty() }
            .joinToString(separator = "\n")
        if (lineProtocol.isEmpty()) {
            logger.atWarn().setMessage("The writes do not contain any Line Protocol, skipping")
                .addKeyValue("writes", stream)
                .log()
            return
        }
        val organization = parameters.orgSafe(options)
        val bucket = parameters.bucketSafe(options)
        val precision = parameters.precisionSafe(options)
        val consistency = parameters.consistencySafe(options)
        logger.atTrace().setMessage("Writing time-series data into InfluxDB")
            .addKeyValue("organization", organization)
            .addKeyValue("bucket", bucket)
            .addKeyValue("precision", precision)
            .log()
        val response = restClient.post()
            .uri {
                it.path("api/v2/write")
                    .queryParam("org", organization)
                    .queryParam("bucket", bucket)
                    .queryParam("precision", precision)
                    .queryParam("consistency", consistency)
                    .build()
            }
            .contentType(MediaType(TEXT_PLAIN, UTF_8))
            .accept(APPLICATION_JSON)
            .body(lineProtocol)
            .retrieve()
            .toBodilessEntity()
        logger.atInfo().setMessage("Got response").addKeyValue("response", response).log()
    }
}

interface InfluxEnumMixin {
    @JsonValue
    fun getValue(): String
}

val noopCancellable = object : Cancellable {
    override fun cancel() {}

    override fun isCancelled() = false
}
