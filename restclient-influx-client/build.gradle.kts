plugins {
    `java-library`
}

group = "uk.co.borismorris.monitoring.influx-client"

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-web")

    api(libs.bundles.influx) {
        exclude("io.micrometer")
        exclude("com.squareup.retrofit2")
        exclude("io.reactivex.rxjava3")
        exclude("com.google.code.gson")
    }

    testImplementation(projects.wiremockSpringBoot)

    integrationTestImplementation("org.testcontainers:junit-jupiter")
    integrationTestImplementation("org.testcontainers:influxdb")
}
