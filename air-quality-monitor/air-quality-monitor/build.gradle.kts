plugins {
    monitoring.`application-conventions`
    monitoring.`jib-conventions`
}

group = "uk.co.borismorris.dyson.airqualitymonitor"

dependencies {
    implementation(projects.airQualityMonitor.kmqttMqttClient)
    implementation(projects.airQualityMonitor.dnsDeviceLocator)

    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.modulith:spring-modulith-api")

    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")

    implementation(projects.restclientInfluxClient)

    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.springframework.modulith:spring-modulith-starter-test")
    testImplementation(projects.wiremockSpringBoot)

    integrationTestImplementation(libs.bundles.kmqtt)
    integrationTestImplementation(libs.kmqtt.broker)
}

application {
    mainClass.set("uk.co.borismorris.dyson.airqualitymonitor.AirQualityMonitorApplicationKt")
}
