package uk.co.borismorris.dyson.airqualitymonitor

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.github.tomakehurst.wiremock.client.WireMock.exactly
import com.github.tomakehurst.wiremock.client.WireMock.findUnmatchedRequests
import com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor
import com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo
import com.github.tomakehurst.wiremock.client.WireMock.verify
import io.github.davidepianca98.mqtt.broker.Broker
import io.github.davidepianca98.mqtt.broker.interfaces.Authentication
import io.github.davidepianca98.mqtt.broker.interfaces.PacketInterceptor
import io.github.davidepianca98.mqtt.packets.MQTTPacket
import io.github.davidepianca98.mqtt.packets.Qos
import io.github.davidepianca98.mqtt.packets.mqtt.MQTTPublish
import org.assertj.core.api.Assertions.assertThat
import org.awaitility.Awaitility.await
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Primary
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.wiremock.spring.ConfigureWireMock
import org.wiremock.spring.EnableWireMock
import uk.co.borismorris.dyson.airqualitymonitor.device.DeviceMetaData
import uk.co.borismorris.dyson.airqualitymonitor.locator.DysonDeviceLocator
import uk.co.borismorris.dyson.airqualitymonitor.locator.LocatedDevice
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.CurrentDeviceState
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.DeviceStateChange
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.FanSpeed
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.MessageType
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.MessageType.REQUEST_ENVIRONMENT_DATA
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.MqttDeviceStatusChangeResponse
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.MqttDeviceStatusResponse
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.MqttEnvironmentDataResponse
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.MqttRequestMessage
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.MqttResponseMessage
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.MutableEnvironmentData
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.PropertyChange
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.ToggleState
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.heatCapableDeviceModule
import java.net.InetSocketAddress
import java.net.ServerSocket
import java.time.Duration
import java.time.ZonedDateTime
import java.util.concurrent.BlockingQueue
import java.util.concurrent.CompletableFuture
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.TimeUnit.MINUTES

const val MOCK_FAN_SERIAL = "AA1-UK-ABC01234D"
const val MOCK_FAN_COMMAND_TOPIC = "527/${MOCK_FAN_SERIAL}/command"
const val MOCK_FAN_STATUS_TOPIC = "527/${MOCK_FAN_SERIAL}/status/current"

@SpringBootTest(
    properties = [
        "dyson.polling-interval=PT10S",
        "influx.url=\${wiremock.baseUrl}",
    ],
)
@EnableWireMock(
    ConfigureWireMock(
        filesUnderClasspath = ["uk/co/borismorris/dyson/airqualitymonitor/wiremock"],
    ),
)
@ActiveProfiles("int-test")
class AirQualityMonitorApplicationTest {
    companion object {
        lateinit var mqttBroker: Broker
        lateinit var mqttBrokerThread: CompletableFuture<*>
        lateinit var messageQueue: BlockingQueue<MQTTPublish>

        @BeforeAll
        @JvmStatic
        fun startMockFan() {
            val ss = ServerSocket(0).use { it.localPort }
            mqttBroker = Broker(
                port = ss,
                authentication = object : Authentication {
                    override fun authenticate(
                        clientId: String,
                        username: String?,
                        password: UByteArray?,
                    ) = when (username) {
                        "air-quality-monitor" -> password!!.toByteArray().decodeToString() == "password"
                        "mock-fan" -> password!!.toByteArray().decodeToString() == "password"
                        else -> false
                    }
                },
                packetInterceptor = object : PacketInterceptor {
                    override fun packetReceived(
                        clientId: String,
                        username: String?,
                        password: UByteArray?,
                        packet: MQTTPacket,
                    ) {
                        if (packet is MQTTPublish && packet.topicName == MOCK_FAN_COMMAND_TOPIC) {
                            messageQueue.add(packet)
                        }
                    }
                },
            )
            mqttBrokerThread = CompletableFuture.runAsync {
                mqttBroker.listen()
            }

            messageQueue = LinkedBlockingQueue()
        }

        @AfterAll
        @JvmStatic
        fun stopMockFan() {
            mqttBroker.stop()
            mqttBrokerThread.get(1, MINUTES)
        }

        @JvmStatic
        @DynamicPropertySource
        fun registerDysonProperties(registry: DynamicPropertyRegistry) {
            registry.add("dyson.mqtt-port") { mqttBroker.port }
            registry.add("dyson.devices[0].serial") { MOCK_FAN_SERIAL }
            registry.add("dyson.devices[0].name") { "dyson-device" }
            registry.add("dyson.devices[0].type") { "527" }
            registry.add("dyson.devices[0].connectionType") { "wss" }
            registry.add("dyson.devices[0].credentials.serial") { "air-quality-monitor" }
            registry.add("dyson.devices[0].credentials.password") { "password" }
        }
    }

    @BeforeEach
    fun setup() {
        messageQueue.clear()
    }

    @AfterEach
    fun noUnmatched() {
        assertThat(findUnmatchedRequests()).isEmpty()
    }

    @Test
    fun `Integration test environment data poll`() {
        waitForPoll()

        val environmentData = MutableEnvironmentData().apply {
            temperature = 700.0
            humidity = 73
        }
        val responseMessage = MqttEnvironmentDataResponse(environmentData).apply {
            time = ZonedDateTime.parse("2020-02-10T19:03:42.486Z")
            type = MessageType.ENVIRONMENT_DATA
        }

        publishMessage(responseMessage)

        await().atMost(Duration.ofMinutes(1)).pollInterval(Duration.ofSeconds(1)).untilAsserted {
            verify(exactly(1), postRequestedFor(urlPathEqualTo("/api/v2/write")))
        }
    }

    @Test
    fun `Integration test purecool state`() {
        waitForPoll()

        val deviceState = CurrentDeviceState().apply {
            fanStatus = ToggleState.ON
            nightModeSpeed = 1
            fanSpeed = FanSpeed.forValue(10.toString())
        }
        val responseMessage = MqttDeviceStatusResponse(deviceState).apply {
            time = ZonedDateTime.parse("2020-02-10T19:03:42.486Z")
            type = MessageType.DEVICE_CURRENT_STATE
        }

        publishMessage(responseMessage)

        await().atMost(Duration.ofMinutes(1)).pollInterval(Duration.ofSeconds(1)).untilAsserted {
            verify(exactly(1), postRequestedFor(urlPathEqualTo("/api/v2/write")))
        }
    }

    @Test
    fun `Integration test purecool state change`() {
        waitForPoll()

        val deviceState = DeviceStateChange().apply {
            fanStatusChange = PropertyChange.change(ToggleState.OFF, ToggleState.ON)
            nightModeSpeedChange = PropertyChange.unchanged(3)
            fanSpeedChange = PropertyChange.change(FanSpeed.forValue("AUTO"), FanSpeed.forValue(5.toString()))
        }
        val responseMessage = MqttDeviceStatusChangeResponse(deviceState).apply {
            time = ZonedDateTime.parse("2020-02-10T19:03:42.486Z")
            type = MessageType.DEVICE_STATE_CHANGE
        }

        publishMessage(responseMessage)

        await().atMost(Duration.ofMinutes(1)).pollInterval(Duration.ofSeconds(1)).untilAsserted {
            verify(exactly(1), postRequestedFor(urlPathEqualTo("/api/v2/write")))
        }
    }

    private fun waitForPoll() {
        val message = messageQueue.poll(1, MINUTES)!!
        val request = objectMapper.readValue<MqttRequestMessage>(message.payload?.toByteArray()!!)
        with(request) {
            assertThat(type).isEqualTo(REQUEST_ENVIRONMENT_DATA)
            assertThat(time).isBeforeOrEqualTo(ZonedDateTime.now())
        }
    }

    private fun publishMessage(responseMessage: MqttResponseMessage) {
        mqttBroker.publish(
            false,
            MOCK_FAN_STATUS_TOPIC,
            Qos.AT_LEAST_ONCE,
            null,
            objectMapper.writeValueAsBytes(responseMessage).toUByteArray(),
        )
    }

    @TestConfiguration
    class AirQualityMonitorTestConfiguration {
        @Primary
        @Bean
        fun mockDeviceLocator() = object : DysonDeviceLocator {
            override fun locateDevice(device: DeviceMetaData) = DockerLocatedDevice(device, InetSocketAddress(mqttBroker.host, mqttBroker.port))
        }
    }

    data class DockerLocatedDevice(val device: DeviceMetaData, override val mqttAddress: InetSocketAddress) :
        LocatedDevice,
        DeviceMetaData by device
}

private val objectMapper = Jackson2ObjectMapperBuilder.json()
    .findModulesViaServiceLoader(true)
    .modulesToInstall(heatCapableDeviceModule)
    .build<ObjectMapper>()
