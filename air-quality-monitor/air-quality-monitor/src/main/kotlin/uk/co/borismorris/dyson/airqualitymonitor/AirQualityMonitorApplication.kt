package uk.co.borismorris.dyson.airqualitymonitor

import io.micrometer.observation.ObservationRegistry
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.http.client.ClientHttpRequestFactoryBuilder
import org.springframework.boot.http.client.ClientHttpRequestFactorySettings
import org.springframework.boot.runApplication
import org.springframework.boot.web.client.RestClientCustomizer
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.annotation.EnableScheduling
import uk.co.borismorris.dyson.airqualitymonitor.config.DysonConfig
import uk.co.borismorris.dyson.airqualitymonitor.config.InfluxDB2Properties
import uk.co.borismorris.dyson.airqualitymonitor.locator.DysonDeviceLocator
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.DysonMqttClient
import uk.co.borismorris.dyson.airqualitymonitor.worker.MonitorAirQuality
import uk.co.borismorris.monitoring.influxclient.InfluxClient
import java.time.Duration

@EnableConfigurationProperties(InfluxDB2Properties::class)
@SpringBootApplication(proxyBeanMethods = false)
class AirQualityMonitorApplication {
    @Bean
    fun restClientCustomizer() = RestClientCustomizer {
        it.requestFactory(
            ClientHttpRequestFactoryBuilder.simple()
                .build(
                    ClientHttpRequestFactorySettings.defaults()
                        .withConnectTimeout(Duration.ofMinutes(5))
                        .withReadTimeout(Duration.ofMinutes(5)),
                ),
        )
    }

    @ConfigurationProperties(prefix = "dyson")
    @Bean
    fun dysonConfig() = DysonConfig()

    @Bean
    fun monitorAirQuality(
        observationRegistry: ObservationRegistry,
        config: DysonConfig,
        deviceLocator: DysonDeviceLocator,
        mqttClient: DysonMqttClient,
        influxClient: InfluxClient,
    ) = MonitorAirQuality(observationRegistry, config, deviceLocator, mqttClient, influxClient)

    @Configuration(proxyBeanMethods = false)
    @ConditionalOnProperty(value = ["scheduling.enabled"], havingValue = "true", matchIfMissing = true)
    @EnableScheduling
    class Scheduling
}

@Suppress("SpreadOperator")
fun main(args: Array<String>) {
    runApplication<AirQualityMonitorApplication>(*args)
}
