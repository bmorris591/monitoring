package uk.co.borismorris.dyson.airqualitymonitor.worker

import com.influxdb.client.domain.WritePrecision
import com.influxdb.client.write.WriteParameters
import io.micrometer.observation.Observation
import io.micrometer.observation.ObservationRegistry
import jakarta.annotation.PostConstruct
import org.slf4j.MDC
import org.springframework.scheduling.annotation.Scheduled
import uk.co.borismorris.dyson.airqualitymonitor.config.DeviceMetadataConfig
import uk.co.borismorris.dyson.airqualitymonitor.config.DysonConfig
import uk.co.borismorris.dyson.airqualitymonitor.influx.EnvironmentObservationMeasurementConverter.convert
import uk.co.borismorris.dyson.airqualitymonitor.locator.DysonDeviceLocator
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.DysonMqttClient
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.DysonMqttConnection
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.DysonMqttSubscription
import uk.co.borismorris.monitoring.influxclient.InfluxClient
import uk.co.borismorris.monitoring.logging.KotlinLogging
import java.util.concurrent.CopyOnWriteArrayList
import java.util.function.Supplier

private val logger = KotlinLogging.logger {}

class MonitorAirQuality(
    private val observationRegistry: ObservationRegistry,
    private val config: DysonConfig,
    private val deviceLocator: DysonDeviceLocator,
    private val mqttClient: DysonMqttClient,
    private val influxClient: InfluxClient,
) {
    private val connections = CopyOnWriteArrayList<DysonDeviceConnection>()

    @PostConstruct
    fun monitor() {
        logger.info("Starting monitoring!")
        val devices = config.devices
        logger.atInfo().setMessage("Monitoring devices").addKeyValue("devices", devices).log()
        devices.map { DysonDeviceConnection(observationRegistry, mqttClient, deviceLocator, it) }
            .toCollection(connections)
    }

    @Scheduled(initialDelay = 0, fixedRateString = "\${dyson.polling-interval}")
    fun pollDevices() {
        connections.forEach { it.pollDevice() }
    }

    inner class DysonDeviceConnection(
        private val observationRegistry: ObservationRegistry,
        private val mqttClient: DysonMqttClient,
        private val deviceLocator: DysonDeviceLocator,
        private val device: DeviceMetadataConfig,
    ) : AutoCloseable {

        lateinit var connection: DysonMqttConnection
        lateinit var subscription: DysonMqttSubscription
        val connected: Boolean
            get() = this::subscription.isInitialized

        private fun connect() {
            Observation.createNotStarted("connect", observationRegistry)
                .lowCardinalityKeyValue("device.serial", device.serial)
                .lowCardinalityKeyValue("device.name", device.name)
                .lowCardinalityKeyValue("device.type", device.type.toString())
                .observe {
                    connection = mqttClient.connect(deviceLocator.locateDevice(device)) {
                        influxClient.writePoints(it.convert().toList(), WriteParameters(WritePrecision.MS, null))
                    }
                    subscription = connection.monitorDevice()
                }
        }

        @Suppress("TooGenericExceptionCaught")
        fun pollDevice() = Observation.createNotStarted("poll-device", observationRegistry)
            .lowCardinalityKeyValue("device.serial", device.serial)
            .lowCardinalityKeyValue("device.name", device.name)
            .lowCardinalityKeyValue("device.type", device.type.toString())
            .observe(
                Supplier {
                    MDC.put("deviceSerial", device.serial)
                    MDC.put("deviceName", device.name)
                    try {
                        if (!connected) {
                            logger.info("Not connected establishing connection")
                            connect()
                        }
                        try {
                            subscription.pollEnvironmentState()
                        } catch (ex: Exception) {
                            logger.info("An error occurred polling , disconnecting")
                            try {
                                close()
                            } catch (suppress: Exception) {
                                ex.addSuppressed(suppress)
                            }
                            throw ex
                        }
                    } finally {
                        MDC.remove("deviceSerial")
                        MDC.remove("deviceName")
                    }
                },
            ) ?: error("Failed to poll $device")

        @Suppress("TooGenericExceptionCaught")
        override fun close() {
            Observation.createNotStarted("close", observationRegistry)
                .lowCardinalityKeyValue("device.serial", device.serial)
                .lowCardinalityKeyValue("device.name", device.name)
                .lowCardinalityKeyValue("device.type", device.type.toString())
                .observe {
                    val errors = mutableListOf<Throwable>()
                    if (this::subscription.isInitialized) {
                        try {
                            subscription.close()
                        } catch (ex: Exception) {
                            errors.add(ex)
                        }
                    }
                    if (this::connection.isInitialized) {
                        try {
                            connection.close()
                        } catch (ex: Exception) {
                            errors.add(ex)
                        }
                    }
                    if (errors.isNotEmpty()) {
                        throw RuntimeException("Failed to close connection to $device").also { ex ->
                            errors.forEach {
                                ex.addSuppressed(
                                    it,
                                )
                            }
                        }
                    }
                }
        }
    }
}
