package uk.co.borismorris.dyson.airqualitymonitor.influx

import com.influxdb.client.domain.WritePrecision
import com.influxdb.client.write.Point
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.HeatCapableDeviceStatusChange
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.HeatCapableDeviceStatusData
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.MqttDeviceStatusChangeResponse
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.MqttDeviceStatusResponse
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.MqttEnvironmentDataResponse
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.MqttResponseMessage

object EnvironmentObservationMeasurementConverter {

    fun MqttResponseMessage.convert() = when (this) {
        is MqttEnvironmentDataResponse -> doConvert()
        is MqttDeviceStatusResponse -> doConvert()
        is MqttDeviceStatusChangeResponse -> doConvert()
    }.filter { it.hasFields() }

    private fun MqttEnvironmentDataResponse.doConvert() = sequenceOf(
        measurement("environment-reading") {
            data.apply {
                temperature?.also { addField("temperature", it) }
                humidity?.also { addField("humidity", it) }
                pm25?.also { addField("pm25", it) }
                pm10?.also { addField("pm10", it) }
                voc?.also { addField("voc", it) }
                nox?.also { addField("nox", it) }
                p25r?.also { addField("p25r", it) }
                p10r?.also { addField("p10r", it) }
            }
        },
    )

    private fun MqttDeviceStatusResponse.doConvert() = sequenceOf(
        measurement("device-status") {
            data.apply {
                addField("fan_status", fanStatus.toString())
                addField("fan_direction", fanDirection.toString())
                addField("auto_mode", autoMode.toString())
                addField("oscillation_state", oscillationState.toString())
                addField("night_mode", nightMode.toString())
                addField("fan_state", fanState.toString())
                addField("night_mode_speed", nightModeSpeed)
                if (!fanSpeed.auto) addField("fan_speed", fanSpeed.speed)
                addField("carbon_filter_state", carbonFilterState)
                addField("hepa_filter_state", hepaFilterState)
            }
            if (device.heatCapable) {
                (data as HeatCapableDeviceStatusData).apply {
                    addField("tilt_state", tiltState.toString())
                    addField("heat_target", heatTarget)
                    addField("heat_mode", heatMode.toString())
                    addField("heat_state", heatState.toString())
                }
            }
        },
    )

    private fun MqttDeviceStatusChangeResponse.doConvert() = sequenceOf(
        measurement("device-status") {
            data.apply {
                fanStatusChange.ifChanged { addField("fan_status", newValue.toString()) }
                fanDirectionChange.ifChanged { addField("fan_direction", newValue.toString()) }
                autoModeChange.ifChanged { addField("auto_mode", newValue.toString()) }
                oscillationStateChange.ifChanged { addField("oscillation_state", newValue.toString()) }
                nightModeChange.ifChanged { addField("night_mode", newValue.toString()) }
                fanStateChange.ifChanged { addField("fan_state", newValue.toString()) }
                nightModeSpeedChange.ifChanged { addField("night_mode_speed", newValue) }
                fanSpeedChange.ifChanged { if (!newValue.auto) addField("fan_speed", newValue.speed) }
                carbonFilterStateChange.ifChanged { addField("carbon_filter_state", newValue) }
                hepaFilterStateChange.ifChanged { addField("hepa_filter_state", newValue) }
            }
            if (device.heatCapable) {
                (data as HeatCapableDeviceStatusChange).apply {
                    tiltStateChange.ifChanged { addField("tilt_state", newValue.toString()) }
                    heatTargetChange.ifChanged { addField("heat_target", newValue) }
                    heatModeChange.ifChanged { addField("heat_mode", newValue.toString()) }
                    heatStateChange.ifChanged { addField("heat_state", newValue.toString()) }
                }
            }
        },
    )
}

internal fun MqttResponseMessage.measurement(
    measurement: String,
    measurementBuilder: Point.() -> Unit,
) = Point(measurement).apply {
    time(this@measurement.time.toInstant(), WritePrecision.MS)
    device.apply {
        addTag("serial", serial)
        addTag("name", name)
        addTag("type", type.toString())
    }
    measurementBuilder(this)
}
