package uk.co.borismorris.dyson.airqualitymonitor.config

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties(prefix = "influx")
class InfluxDB2Properties {
    /**
     * URL to connect to InfluxDB.
     */
    var url: String? = null

    /**
     * Token to use for the authorization.
     */
    var token: String? = null

    /**
     * Default destination organization for writes and queries.
     */
    var org: String? = null

    /**
     * Default destination bucket for writes.
     */
    var bucket: String? = null
}
