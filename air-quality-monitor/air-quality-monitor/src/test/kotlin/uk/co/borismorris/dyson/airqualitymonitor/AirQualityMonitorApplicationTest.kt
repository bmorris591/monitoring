package uk.co.borismorris.dyson.airqualitymonitor

import org.junit.jupiter.api.Test
import org.springframework.modulith.core.ApplicationModules
import org.springframework.modulith.docs.Documenter

class AirQualityMonitorApplicationTest {

    @Test
    fun writeDocumentationSnippets() {
        var modules = ApplicationModules.of(AirQualityMonitorApplication::class.java).verify()
        Documenter(modules)
            .writeModulesAsPlantUml()
            .writeIndividualModulesAsPlantUml()
    }
}
