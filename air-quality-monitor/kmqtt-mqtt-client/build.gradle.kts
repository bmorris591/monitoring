plugins {
    `java-library`
}

group = "uk.co.borismorris.dyson.airqualitymonitor"

dependencies {
    api(projects.airQualityMonitor.dysonMqttClient)

    implementation("org.springframework.boot:spring-boot-autoconfigure")
    implementation(libs.bundles.kmqtt)
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")

    integrationTestImplementation(projects.airQualityMonitor.dysonMqttClient) {
        targetConfiguration = "integrationTestArtifacts"
    }
}
