package uk.co.borismorris.dyson.airqualitymonitor.mqtt.kmqtt

import com.fasterxml.jackson.databind.ObjectMapper
import io.micrometer.observation.ObservationRegistry
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.task.AsyncTaskExecutor
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.DysonMqttClient
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.DysonMqttConf

@Configuration(proxyBeanMethods = false)
class KmqttDysonMqttAutoConfiguration {
    @Bean
    @ConditionalOnMissingBean(DysonMqttClient::class)
    fun kmqttDysonMqttClient(
        observationRegistry: ObservationRegistry,
        taskExecutor: AsyncTaskExecutor,
        objectMapper: ObjectMapper,
        dysonMqttConf: DysonMqttConf,
    ) = KmqttDysonMqttClient(observationRegistry, taskExecutor, objectMapper, dysonMqttConf)
}
