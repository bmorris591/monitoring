package uk.co.borismorris.dyson.airqualitymonitor.mqtt.kmqtt

import com.fasterxml.jackson.databind.InjectableValues
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import io.github.davidepianca98.MQTTClient
import io.github.davidepianca98.mqtt.MQTTVersion.MQTT3_1_1
import io.github.davidepianca98.mqtt.Subscription
import io.github.davidepianca98.mqtt.packets.Qos.AT_LEAST_ONCE
import io.github.davidepianca98.mqtt.packets.mqtt.MQTTPublish
import io.github.davidepianca98.mqtt.packets.mqttv5.ReasonCode.SUCCESS
import io.github.davidepianca98.mqtt.packets.mqttv5.SubscriptionOptions
import io.micrometer.observation.Observation
import io.micrometer.observation.ObservationRegistry
import org.springframework.core.task.AsyncTaskExecutor
import uk.co.borismorris.dyson.airqualitymonitor.locator.LocatedDevice
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.DysonMqttClient
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.DysonMqttConf
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.DysonMqttConnection
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.DysonMqttNotConnectedError
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.DysonMqttSubscription
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.MessageType.REQUEST_ENVIRONMENT_DATA
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.MqttRequestMessage
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.MqttResponseMessage
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.commandTopic
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.registerDeviceStateModule
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.statusTopic
import uk.co.borismorris.monitoring.logging.KotlinLogging
import java.nio.charset.StandardCharsets.UTF_8
import java.security.SecureRandom
import java.time.ZonedDateTime
import java.util.concurrent.CompletableFuture
import java.util.concurrent.TimeUnit.MILLISECONDS
import java.util.concurrent.atomic.AtomicBoolean
import java.util.function.Supplier

private val logger = KotlinLogging.logger {}

class KmqttDysonMqttClient(
    private val observationRegistry: ObservationRegistry,
    private val executor: AsyncTaskExecutor,
    private val objectMapper: ObjectMapper,
    private val dysonConfig: DysonMqttConf,
) : DysonMqttClient {

    override fun connect(device: LocatedDevice, messageHandler: (MqttResponseMessage) -> Unit): DysonMqttConnection = Observation.createNotStarted("connect", observationRegistry)
        .lowCardinalityKeyValue("device.serial", device.serial)
        .lowCardinalityKeyValue("device.name", device.name)
        .lowCardinalityKeyValue("device.type", device.type.toString())
        .lowCardinalityKeyValue("device.mqtt", device.mqttAddress.toString())
        .observe(
            Supplier {
                val connection =
                    KmqttDysonMqttConnection(
                        observationRegistry,
                        dysonConfig,
                        device,
                        executor,
                        objectMapper,
                        messageHandler,
                    )
                connection.connect()
                logger.atInfo().setMessage("Connected").addKeyValue("device", device.serial).log()
                connection
            },
        ) ?: error("Failed to connect to $device")

    override fun close() {}
}

class KmqttDysonMqttConnection(
    private val observationRegistry: ObservationRegistry,
    private val dysonConfig: DysonMqttConf,
    override val device: LocatedDevice,
    private val executor: AsyncTaskExecutor,
    objectMapper: ObjectMapper,
    messageHandler: (MqttResponseMessage) -> Unit,
) : DysonMqttConnection {
    private val objectMapper =
        objectMapper.copy()
            .setInjectableValues(InjectableValues.Std().addValue(LocatedDevice::class.java, device))
            .registerDeviceStateModule(device)
    private val statusTopic = device.statusTopic()
    private val mqttClient = MQTTClient(
        mqttVersion = MQTT3_1_1,
        address = device.mqttAddress.hostString,
        port = device.mqttAddress.port,
        clientId = "aqm-${SecureRandom().nextInt(1000)}",
        userName = device.credentials.serial,
        password = device.credentials.password.toByteArray(UTF_8).toUByteArray(),
        tls = null,
    ) { messageHandler(convertMessage(it)) }

    private lateinit var promise: CompletableFuture<*>
    private val connected = AtomicBoolean(false)

    private fun MqttRequestMessage.publish() {
        mqttClient.publish(false, AT_LEAST_ONCE, device.commandTopic(), toBytes().toUByteArray())
    }

    private fun MqttRequestMessage.toBytes() = objectMapper.writeValueAsBytes(this)

    fun connect() {
        Observation.createNotStarted("connect", observationRegistry).observe {
            promise = executor.submitCompletable {
                while (mqttClient.isRunning()) {
                    mqttClient.step()
                    Thread.yield()
                    MILLISECONDS.sleep(100)
                }
            }
            promise.whenComplete { _, t ->
                if (t != null) {
                    logger.atError().setCause(t).setMessage("Mqtt Client error").addKeyValue("device", device).log()
                }
            }
            logger.atInfo().setMessage("Connected").addKeyValue("device", device).log()
            connected.set(true)
        }
    }

    private fun convertMessage(message: MQTTPublish): MqttResponseMessage {
        logger.atInfo().setMessage("Got message")
            .addKeyValue("message", message)
            .addKeyValue("device", device)
            .log()
        val responseMessage =
            objectMapper.readValue<MqttResponseMessage>(checkNotNull(message.payload?.toByteArray()))
        logger.atInfo().setMessage("Decoded message")
            .addKeyValue("message", responseMessage)
            .addKeyValue("device", device)
            .log()
        return responseMessage
    }

    override fun monitorDevice(): DysonMqttSubscription {
        if (!connected.get()) {
            throw DysonMqttNotConnectedError(device)
        }
        mqttClient.subscribe(listOf(Subscription(statusTopic, SubscriptionOptions(AT_LEAST_ONCE))))
        logger.atInfo().setMessage("Connected to MQTT state topic")
            .addKeyValue("statusTopic", statusTopic)
            .addKeyValue("device", device)
            .log()

        return object : DysonMqttSubscription {
            override fun pollEnvironmentState() {
                if (!connected.get()) {
                    throw DysonMqttNotConnectedError(device)
                }
                val request = MqttRequestMessage(REQUEST_ENVIRONMENT_DATA, ZonedDateTime.now())
                logger.atInfo().setMessage("Sending environment status request")
                    .addKeyValue("request", request)
                    .addKeyValue("device", device)
                    .log()
                request.publish()
            }

            override fun close() {
                logger.atInfo().setMessage("State stream closed. Send unsubscribe")
                    .addKeyValue("device", device)
                    .log()
                mqttClient.unsubscribe(listOf(statusTopic))
            }
        }
    }

    override fun close() {
        Observation.createNotStarted("close", observationRegistry)
            .lowCardinalityKeyValue("device.serial", device.serial)
            .lowCardinalityKeyValue("device.name", device.name)
            .lowCardinalityKeyValue("device.type", device.type.toString())
            .lowCardinalityKeyValue("device.mqtt", device.mqttAddress.toString())
            .observe {
                connected.set(false)
                logger.atInfo().setMessage("Dispose MQTT connection")
                    .addKeyValue("mqttClient", mqttClient)
                    .addKeyValue("device", device)
                    .log()
                mqttClient.disconnect(SUCCESS)
                promise.cancel(true)
                logger.atInfo().setMessage("MQTT connection disposed").addKeyValue("device", device).log()
            }
    }
}
