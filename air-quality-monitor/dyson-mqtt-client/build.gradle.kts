plugins {
    `java-library`
}

group = "uk.co.borismorris.dyson.airqualitymonitor"

dependencies {
    api(projects.airQualityMonitor.deviceLocator)
    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")

    implementation("org.springframework.boot:spring-boot-autoconfigure")

    testImplementation("org.springframework:spring-web")
    testImplementation("org.springframework.boot:spring-boot-starter-test")

    integrationTestImplementation(libs.bundles.kmqtt)
    integrationTestImplementation(libs.kmqtt.broker)
    integrationTestImplementation("io.micrometer:micrometer-observation-test")
}

configurations.create("integrationTestArtifacts") {
    extendsFrom(configurations["integrationTestImplementation"])
}

val integrationTestJar by tasks.registering(Jar::class) {
    archiveClassifier.set("integrationTest")
    dependsOn("integrationTestClasses")
    from(sourceSets["integrationTest"].output)
}

artifacts {
    add("integrationTestArtifacts", integrationTestJar)
}

configure<PublishingExtension> {
    publications {
        maybeCreate<MavenPublication>("maven").artifacts.artifact(integrationTestJar)
    }
}
