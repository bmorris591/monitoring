package uk.co.borismorris.dyson.airqualitymonitor.mqtt.client

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.util.concurrent.atomic.AtomicBoolean

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class PropertyChangeTest {
    @Test
    fun `When old == new then changed is false`() {
        val oldVal = "AAA"
        val newVal = "AAA"

        val propChange = PropertyChange.change(oldVal, newVal)

        assertThat(propChange.changed()).isFalse
    }

    @Test
    fun `When old != new then changed is true`() {
        val oldVal = "AAA"
        val newVal = "BBB"

        val propChange = PropertyChange.change(oldVal, newVal)

        assertThat(propChange.changed()).isTrue
    }

    @Test
    fun `When old == new then isChanged is not executed`() {
        val oldVal = "AAA"
        val newVal = "AAA"

        val propChange = PropertyChange.change(oldVal, newVal)

        val prop = AtomicBoolean(true)
        propChange.ifChanged({ prop.set(false) })

        assertThat(prop).isTrue
    }

    @Test
    fun `When old != new then isChanged is executed`() {
        val oldVal = "AAA"
        val newVal = "BBB"

        val propChange = PropertyChange.change(oldVal, newVal)

        val prop = AtomicBoolean(true)
        propChange.ifChanged({ prop.set(false) })

        assertThat(prop).isFalse
    }
}
