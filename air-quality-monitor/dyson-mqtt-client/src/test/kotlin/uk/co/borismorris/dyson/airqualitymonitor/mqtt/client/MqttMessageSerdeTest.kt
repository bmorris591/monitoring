package uk.co.borismorris.dyson.airqualitymonitor.mqtt.client

import com.fasterxml.jackson.databind.InjectableValues.Std
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoExtension
import org.mockito.kotlin.whenever
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder
import uk.co.borismorris.dyson.airqualitymonitor.locator.LocatedDevice
import java.time.Duration
import java.time.ZonedDateTime

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(MockitoExtension::class)
internal class MqttMessageSerdeTest {

    @Mock
    lateinit var device: LocatedDevice
    lateinit var mapper: ObjectMapper

    @BeforeEach
    fun setup() {
        mapper = Jackson2ObjectMapperBuilder.json()
            .findModulesViaServiceLoader(true)
            .build<ObjectMapper>()
            .setInjectableValues(Std().addValue(LocatedDevice::class.java, device))
    }

    @Test
    fun `When I receive an environment data message then it successfully deserializes`() {
        val deserialized = this::class.java.getResourceAsStream("environment-data-message.json").use {
            mapper.readValue<MqttResponseMessage>(it)
        }

        assertThat(deserialized).isNotNull
        assertThat(deserialized.device).isSameAs(device)
        assertThat(deserialized.type).isEqualTo(MessageType.ENVIRONMENT_DATA)
        assertThat(deserialized.time).isEqualTo(ZonedDateTime.parse("2019-07-14T07:01:10.123Z"))
        assertThat(deserialized).isInstanceOfSatisfying(MqttEnvironmentDataResponse::class.java) { response ->
            assertThat(response.data).isInstanceOfSatisfying(EnvironmentData::class.java) { data ->
                assertThat(data.temperature).isEqualTo(299.2)
                assertThat(data.humidity).isEqualTo(46)
            }
        }
    }

    @Test
    fun `When I receive an environment data message with read failures then it successfully deserializes`() {
        val deserialized = this::class.java.getResourceAsStream("environment-data-fail-message.json").use {
            mapper.readValue<MqttResponseMessage>(it)
        }

        assertThat(deserialized).isNotNull
        assertThat(deserialized.device).isSameAs(device)
        assertThat(deserialized.type).isEqualTo(MessageType.ENVIRONMENT_DATA)
        assertThat(deserialized.time).isEqualTo(ZonedDateTime.parse("2019-07-14T07:01:10.123Z"))
        assertThat(deserialized).isInstanceOfSatisfying(MqttEnvironmentDataResponse::class.java) { response ->
            assertThat(response.data).isInstanceOfSatisfying(EnvironmentData::class.java) { data ->
                assertThat(data.temperature).isEqualTo(299.2)
                assertThat(data.humidity).isEqualTo(46)
                assertThat(data.pm10).isNull()
            }
        }
    }

    @Test
    fun `When I receive a the current status message then it successfully deserializes`() {
        whenever(device.heatCapable).thenReturn(false)
        mapper.registerDeviceStateModule(device)

        val deserialized = this::class.java.getResourceAsStream("current-status-message.json").use {
            mapper.readValue<MqttResponseMessage>(it)
        }

        assertThat(deserialized).isNotNull
        assertThat(deserialized.device).isSameAs(device)
        assertThat(deserialized.type).isEqualTo(MessageType.DEVICE_CURRENT_STATE)
        assertThat(deserialized.time).isEqualTo(ZonedDateTime.parse("2019-11-20T10:28:32.000Z"))
        assertThat(deserialized).isInstanceOfSatisfying(MqttDeviceStatusResponse::class.java) { response ->
            assertThat(response.data).isInstanceOfSatisfying(CurrentDeviceState::class.java) { data ->
                assertThat(data.fanDirection).isEqualTo(FanDirection.FRONT)
                assertThat(data.fanState).isEqualTo(FanState.OFF)
                assertThat(data.fanSpeed).isEqualTo(FanSpeed(0, true))
                assertThat(data.sleepTimer).isEqualTo(SleepTimer(Duration.ZERO, false))
                assertThat(data.oscillationAngleLow).isEqualTo(135)
                assertThat(data.oscillationAngleHigh).isEqualTo(225)
            }
        }
    }

    @Test
    fun `When I receive a status message then it successfully deserializes`() {
        whenever(device.heatCapable).thenReturn(true)
        mapper.registerDeviceStateModule(device)

        val deserialized = this::class.java.getResourceAsStream("status-change-message.json").use {
            mapper.readValue<MqttResponseMessage>(it)
        }

        assertThat(deserialized).isNotNull
        assertThat(deserialized.device).isSameAs(device)
        assertThat(deserialized.type).isEqualTo(MessageType.DEVICE_STATE_CHANGE)
        assertThat(deserialized.time).isEqualTo(ZonedDateTime.parse("2019-07-28T05:58:05.000Z"))
        assertThat(deserialized).isInstanceOfSatisfying(MqttDeviceStatusChangeResponse::class.java) { response ->
            assertThat(response.data).isInstanceOfSatisfying(HeatCapableDeviceStateChange::class.java) { data ->
                data.fanStatusChange.apply {
                    assertThat(changed()).isFalse
                    assertThat(oldValue).isEqualTo(ToggleState.ON)
                    assertThat(newValue).isEqualTo(ToggleState.ON)
                }
                data.autoModeChange.apply {
                    assertThat(changed()).isTrue
                    assertThat(oldValue).isEqualTo(ToggleState.ON)
                    assertThat(newValue).isEqualTo(ToggleState.OFF)
                }
            }
        }
    }

    @Test
    fun `When I send a command then it is correctly serialized`() {
        val command = MqttRequestMessage(MessageType.REQUEST_ENVIRONMENT_DATA, ZonedDateTime.parse("2019-07-14T07:01:10.123Z"))
        val json = mapper.writeValueAsString(command)
        assertThat(json)
            .isNotNull
            .isEqualTo("""{"msg":"REQUEST-PRODUCT-ENVIRONMENT-CURRENT-SENSOR-DATA","time":"2019-07-14T07:01:10.123Z"}""")
    }
}
