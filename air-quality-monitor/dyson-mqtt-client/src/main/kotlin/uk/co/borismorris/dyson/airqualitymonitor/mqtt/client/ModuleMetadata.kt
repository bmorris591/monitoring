package uk.co.borismorris.dyson.airqualitymonitor.mqtt.client

import org.springframework.modulith.ApplicationModule
import org.springframework.modulith.NamedInterface
import org.springframework.modulith.PackageInfo

@ApplicationModule(type = ApplicationModule.Type.OPEN)
@NamedInterface("mqtt-client")
@PackageInfo
class ModuleMetadata
