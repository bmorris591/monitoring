package uk.co.borismorris.dyson.airqualitymonitor.mqtt.client

import org.springframework.aot.hint.annotation.RegisterReflectionForBinding
import org.springframework.context.annotation.Configuration

@Configuration(proxyBeanMethods = false)
@RegisterReflectionForBinding(
    MqttRequestMessage::class,
    MqttEnvironmentDataResponse::class,
    MqttDeviceStatusResponse::class,
    MqttDeviceStatusChangeResponse::class,
    CurrentHeatCapableDeviceState::class,
    HeatCapableDeviceStateChange::class,
)
class DysonMqttClientAutoConfiguration
