package uk.co.borismorris.dyson.airqualitymonitor.mqtt.client

import uk.co.borismorris.dyson.airqualitymonitor.locator.LocatedDevice

class DysonMqttConnectionError : RuntimeException {
    constructor(device: LocatedDevice, cause: Throwable) : super("Failed to connect to MQTT device=$device", cause)

    constructor(device: LocatedDevice) : super("Failed to connect to MQTT device=$device")
}

class DysonMqttNotConnectedError : RuntimeException {
    constructor(device: LocatedDevice) : super("MQTT channel not connected device=$device")
}
