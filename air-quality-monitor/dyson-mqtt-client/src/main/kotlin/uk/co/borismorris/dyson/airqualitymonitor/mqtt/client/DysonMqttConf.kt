package uk.co.borismorris.dyson.airqualitymonitor.mqtt.client

import java.time.Duration
import kotlin.math.ceil
import kotlin.math.roundToLong

interface DysonMqttConf {
    val pollingInterval: Duration
    val pollingGraceMultiplier: Double
    val retries: Int
}

fun DysonMqttConf.receiveTimeout(): Duration = Duration.ofMillis(ceil(pollingInterval.toMillis() * pollingGraceMultiplier).roundToLong())
