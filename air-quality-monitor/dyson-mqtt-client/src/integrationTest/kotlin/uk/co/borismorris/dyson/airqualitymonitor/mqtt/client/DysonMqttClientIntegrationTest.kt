package uk.co.borismorris.dyson.airqualitymonitor.mqtt.client

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import io.github.davidepianca98.mqtt.broker.Broker
import io.github.davidepianca98.mqtt.broker.interfaces.Authentication
import io.github.davidepianca98.mqtt.broker.interfaces.PacketInterceptor
import io.github.davidepianca98.mqtt.packets.MQTTPacket
import io.github.davidepianca98.mqtt.packets.Qos
import io.github.davidepianca98.mqtt.packets.mqtt.MQTTPublish
import io.micrometer.observation.tck.TestObservationRegistry
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder
import uk.co.borismorris.dyson.airqualitymonitor.device.DeviceCredentials
import uk.co.borismorris.dyson.airqualitymonitor.locator.LocatedDevice
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.MessageType.REQUEST_ENVIRONMENT_DATA
import java.net.InetSocketAddress
import java.net.ServerSocket
import java.time.Duration
import java.time.ZonedDateTime
import java.util.concurrent.BlockingQueue
import java.util.concurrent.CompletableFuture
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.TimeUnit.MINUTES

const val MOCK_FAN_SERIAL = "AA1-UK-ABC01234D"
const val MOCK_FAN_COMMAND_TOPIC = "527/${MOCK_FAN_SERIAL}/command"
const val MOCK_FAN_STATUS_TOPIC = "527/${MOCK_FAN_SERIAL}/status/current"
const val MOCK_FAN_USERNAME = "mock-fan"
const val MOCK_FAN_PASSWORD = "password"
const val AQM_USERNAME = "air-quality-monitor"
const val AQM_PASSWORD = "password"

val aqmCredentials = object : DeviceCredentials {
    override val serial: String
        get() = AQM_USERNAME
    override val password: String
        get() = AQM_PASSWORD
}

@SpringBootTest(classes = [DysonMqttClientIntegrationTestConfiguration::class])
abstract class DysonMqttClientIntegrationTest {
    companion object {

        lateinit var mqttBroker: Broker
        lateinit var mqttBrokerThread: CompletableFuture<*>
        lateinit var messageQueue: BlockingQueue<MQTTPublish>

        @BeforeAll
        @JvmStatic
        fun startMockFan() {
            messageQueue = LinkedBlockingQueue()
            val ss = ServerSocket(0).use { it.localPort }
            mqttBroker = Broker(
                port = ss,
                authentication = object : Authentication {
                    override fun authenticate(
                        clientId: String,
                        username: String?,
                        password: UByteArray?,
                    ) = when (username) {
                        AQM_USERNAME -> password!!.toByteArray().decodeToString() == AQM_PASSWORD
                        MOCK_FAN_USERNAME -> password!!.toByteArray().decodeToString() == MOCK_FAN_PASSWORD
                        else -> false
                    }
                },
                packetInterceptor = object : PacketInterceptor {
                    override fun packetReceived(
                        clientId: String,
                        username: String?,
                        password: UByteArray?,
                        packet: MQTTPacket,
                    ) {
                        if (packet is MQTTPublish && packet.topicName == MOCK_FAN_COMMAND_TOPIC) {
                            messageQueue.add(packet)
                        }
                    }
                },
            )
            mqttBrokerThread = CompletableFuture.runAsync {
                mqttBroker.listen()
            }
        }

        @AfterAll
        @JvmStatic
        fun stopMockFan() {
            mqttBroker.stop()
            mqttBrokerThread.get(1, MINUTES)
        }
    }

    val mockLocatedDevice = object : LocatedDevice {
        override val serial: String
            get() = MOCK_FAN_SERIAL
        override val name: String
            get() = "dyson-device"
        override val credentials: DeviceCredentials
            get() = aqmCredentials
        override val type: Int
            get() = 527
        override val connectionType: String
            get() = "wss"
        override val mqttAddress: InetSocketAddress
            get() = InetSocketAddress.createUnresolved(mqttBroker.host, mqttBroker.port)
    }

    @Autowired
    lateinit var dysonMqttClient: DysonMqttClient

    @BeforeEach
    fun setup() {
        messageQueue.clear()
    }

    @Test
    fun `Integration test environment data poll`() {
        val respond = CompletableFuture.runAsync { respond() }
        val messaages = LinkedBlockingQueue<MqttResponseMessage>()
        dysonMqttClient.connect(mockLocatedDevice, messaages::offer).use { conn ->
            conn.monitorDevice().use {
                it.pollEnvironmentState()
                val response = messaages.poll(1, MINUTES)
                assertThat(response)
                    .isNotNull
                    .isInstanceOfSatisfying(MqttEnvironmentDataResponse::class.java) { envData ->
                        assertThat(envData.device.serial).isEqualTo(MOCK_FAN_SERIAL)
                    }
            }
        }
        assertThat(respond).isCompleted
    }

    private fun respond() {
        waitForPoll()

        val environmentData = MutableEnvironmentData().apply {
            temperature = 700.0
            humidity = 73
        }
        val responseMessage = MqttEnvironmentDataResponse(environmentData).apply {
            time = ZonedDateTime.parse("2020-02-10T19:03:42.486Z")
            type = MessageType.ENVIRONMENT_DATA
        }

        publishMessage(responseMessage)
    }

    private fun waitForPoll() {
        val message = messageQueue.poll(1, MINUTES)!!
        val request = objectMapper.readValue<MqttRequestMessage>(message.payload?.toByteArray()!!)
        assertThat(request.type).isEqualTo(REQUEST_ENVIRONMENT_DATA)
        assertThat(request.time).isBeforeOrEqualTo(ZonedDateTime.now())
    }

    private fun publishMessage(responseMessage: MqttResponseMessage) {
        mqttBroker.publish(
            false,
            MOCK_FAN_STATUS_TOPIC,
            Qos.AT_LEAST_ONCE,
            null,
            objectMapper.writeValueAsBytes(responseMessage).toUByteArray(),
        )
    }
}

private val objectMapper = Jackson2ObjectMapperBuilder.json()
    .findModulesViaServiceLoader(true)
    .modulesToInstall(heatCapableDeviceModule)
    .build<ObjectMapper>()

@Configuration
@EnableAutoConfiguration
class DysonMqttClientIntegrationTestConfiguration {
    @Bean
    fun observationRegistry() = TestObservationRegistry.create()

    @Bean
    fun dysonMqttConf() = object : DysonMqttConf {
        override val pollingInterval: Duration
            get() = Duration.ofSeconds(10)
        override val pollingGraceMultiplier: Double
            get() = 2.0
        override val retries: Int
            get() = 1
    }
}
