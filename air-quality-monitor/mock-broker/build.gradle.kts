plugins {
    monitoring.`application-conventions`
    monitoring.`jib-conventions`
}

group = "uk.co.borismorris.dyson.airqualitymonitor.mqtt"

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")

    implementation(libs.bundles.kmqtt)
    implementation(libs.kmqtt.broker)
}

application {
    mainClass.set("uk.co.borismorris.dyson.airqualitymonitor.mqtt.KmqttBrokerKt")
}
