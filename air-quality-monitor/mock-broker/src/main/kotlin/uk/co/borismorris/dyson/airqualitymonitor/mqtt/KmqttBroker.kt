package uk.co.borismorris.dyson.airqualitymonitor.mqtt

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean

@SpringBootApplication(proxyBeanMethods = false)
@EnableConfigurationProperties(BrokerConfig::class)
class KmqttBroker {

    @Bean
    fun broker(brokerConfig: BrokerConfig) = BrokerApplication(brokerConfig)
}

@Suppress("SpreadOperator")
fun main(args: Array<String>) {
    runApplication<KmqttBroker>(*args)
}
