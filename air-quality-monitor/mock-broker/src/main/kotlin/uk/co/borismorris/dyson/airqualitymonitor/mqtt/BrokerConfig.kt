package uk.co.borismorris.dyson.airqualitymonitor.mqtt

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties(prefix = "broker")
data class BrokerConfig(val port: Int = 1883, val users: Map<String, String>)
