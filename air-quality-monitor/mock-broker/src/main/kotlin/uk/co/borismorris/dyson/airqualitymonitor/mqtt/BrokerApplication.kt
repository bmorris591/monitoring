package uk.co.borismorris.dyson.airqualitymonitor.mqtt

import io.github.davidepianca98.mqtt.broker.Broker
import io.github.davidepianca98.mqtt.broker.interfaces.Authentication
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner

class BrokerApplication(private val brokerConfig: BrokerConfig) : ApplicationRunner {
    override fun run(args: ApplicationArguments?) {
        val mqttBroker = Broker(
            port = brokerConfig.port,
            authentication = object : Authentication {
                override fun authenticate(
                    clientId: String,
                    username: String?,
                    password: UByteArray?,
                ) = password?.let { p -> brokerConfig.users[username]?.let { p.toByteArray().decodeToString() == it } } == true
            },
        )

        mqttBroker.listen()
    }
}
