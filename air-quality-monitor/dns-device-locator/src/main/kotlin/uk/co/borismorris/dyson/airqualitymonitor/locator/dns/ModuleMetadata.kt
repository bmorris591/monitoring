package uk.co.borismorris.dyson.airqualitymonitor.locator.dns

import org.springframework.modulith.ApplicationModule
import org.springframework.modulith.NamedInterface
import org.springframework.modulith.PackageInfo

@ApplicationModule(type = ApplicationModule.Type.OPEN)
@NamedInterface("dns-locator")
@PackageInfo
class ModuleMetadata
