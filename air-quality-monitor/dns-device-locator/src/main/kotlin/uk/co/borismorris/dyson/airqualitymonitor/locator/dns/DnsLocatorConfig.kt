package uk.co.borismorris.dyson.airqualitymonitor.locator.dns

interface DnsLocatorConfig {
    val searchDomain: String
    val mqttPort: Int
}
