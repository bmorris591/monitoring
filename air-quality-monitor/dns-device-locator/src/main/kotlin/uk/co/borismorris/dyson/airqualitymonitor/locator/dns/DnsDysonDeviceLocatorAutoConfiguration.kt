package uk.co.borismorris.dyson.airqualitymonitor.locator.dns

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration(proxyBeanMethods = false)
class DnsDysonDeviceLocatorAutoConfiguration {
    @Bean
    fun dysonDeviceLocator(dnsLocatorConfig: DnsLocatorConfig) = DnsDysonDeviceLocator(dnsLocatorConfig)
}
