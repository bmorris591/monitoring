plugins {
    `java-library`
}

group = "uk.co.borismorris.dyson.airqualitymonitor"

dependencies {
    api(projects.airQualityMonitor.deviceLocator)
    implementation("org.springframework.boot:spring-boot-autoconfigure")
}
