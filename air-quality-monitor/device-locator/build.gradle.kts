plugins {
    `java-library`
}

group = "uk.co.borismorris.dyson.airqualitymonitor"

dependencies {
    api(projects.airQualityMonitor.deviceApi)
}
