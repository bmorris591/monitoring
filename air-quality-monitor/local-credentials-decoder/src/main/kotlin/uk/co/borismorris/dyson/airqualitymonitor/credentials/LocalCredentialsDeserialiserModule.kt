package uk.co.borismorris.dyson.airqualitymonitor.credentials

import com.fasterxml.jackson.databind.module.SimpleModule
import uk.co.borismorris.dyson.airqualitymonitor.device.DeviceCredentials

class LocalCredentialsDeserialiserModule : SimpleModule() {
    init {
        addDeserializer(DeviceCredentials::class.java, LocalCredentialsDeserialiser())
    }
}
