package uk.co.borismorris.dyson.airqualitymonitor.credentials

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.deser.impl.NullsConstantProvider.nuller
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import uk.co.borismorris.dyson.airqualitymonitor.device.DeviceCredentials

class LocalCredentialsDeserialiser : StdDeserializer<DeviceCredentials>(DeviceCredentials::class.java) {
    override fun deserialize(
        p: JsonParser,
        ctxt: DeserializationContext?,
    ): LocalCredentials {
        val mapper = p.codec as ObjectMapper
        return decode(_parseString(p, ctxt, nuller()), mapper)
    }
}
