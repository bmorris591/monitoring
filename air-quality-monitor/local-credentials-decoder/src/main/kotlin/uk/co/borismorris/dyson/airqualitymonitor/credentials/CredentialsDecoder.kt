package uk.co.borismorris.dyson.airqualitymonitor.credentials

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import java.util.Base64
import javax.crypto.Cipher
import javax.crypto.Cipher.DECRYPT_MODE
import javax.crypto.Cipher.ENCRYPT_MODE
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

internal const val CIPHER_MODE = "AES/CBC/PKCS5PADDING"
internal val ivSpec = IvParameterSpec(ByteArray(16) { 0x00 })
internal val keySpec = SecretKeySpec(ByteArray(32) { (it + 1).toByte() }, "AES")

fun decode(
    encoded: String,
    mapper: ObjectMapper,
): LocalCredentials {
    val ciphertext = Base64.getDecoder().decode(encoded)
    val cipher = decryptionCipher()
    val plaintext = cipher.doFinal(ciphertext)
    return mapper.readValue(plaintext)
}

fun encode(
    credentials: LocalCredentials,
    mapper: ObjectMapper,
): String {
    val json = mapper.writeValueAsString(credentials)
    val cipher = encryptionCipher()
    val encrypted = cipher.doFinal(json.toByteArray())
    return Base64.getEncoder().encodeToString(encrypted)
}

private fun encryptionCipher() = initCipher(ENCRYPT_MODE)

private fun decryptionCipher() = initCipher(DECRYPT_MODE)

private fun initCipher(mode: Int) = Cipher.getInstance(CIPHER_MODE).apply { init(mode) }

private fun Cipher.init(mode: Int) = init(mode, keySpec, ivSpec)
