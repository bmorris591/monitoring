package uk.co.borismorris.dyson.airqualitymonitor.credentials

import com.fasterxml.jackson.databind.json.JsonMapper
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class CredentialsDecoderTest {
    private val mapper = JsonMapper.builder().findAndAddModules().build()

    @Test
    fun `I can decrypt encrypted credentials`() {
        val credentials = LocalCredentials("AAA", "passwordpassword")

        val encrypted = encode(credentials, mapper)
        val decrypted = decode(encrypted, mapper)

        assertThat(decrypted).isEqualTo(credentials)
    }
}
