package uk.co.borismorris.dyson.airqualitymonitor.mock

import com.fasterxml.jackson.databind.ObjectMapper
import io.micrometer.observation.ObservationRegistry
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.core.task.AsyncTaskExecutor
import uk.co.borismorris.dyson.airqualitymonitor.mock.conf.MockFanConf
import uk.co.borismorris.dyson.airqualitymonitor.mock.worker.DysonFanRunner

@SpringBootApplication(proxyBeanMethods = false)
class MockDysonFan {
    @ConfigurationProperties(prefix = "dyson.mockfan")
    @Bean
    fun mockFanConfiguration() = MockFanConf()

    @Bean
    fun dysonFanRunner(
        observationRegistry: ObservationRegistry,
        taskExecutor: AsyncTaskExecutor,
        mockFanConf: MockFanConf,
        objectMapper: ObjectMapper,
    ) = DysonFanRunner(observationRegistry, mockFanConf, taskExecutor, objectMapper)
}

@Suppress("SpreadOperator")
fun main(args: Array<String>) {
    runApplication<MockDysonFan>(*args)
}
