package uk.co.borismorris.dyson.airqualitymonitor.mock.conf

class MockFanConf {
    lateinit var host: String
    lateinit var username: String
    lateinit var password: String
    val mqttPort = 1883
    lateinit var incoming: String
    lateinit var outgoing: String

    override fun toString() = "MockFanConf(host='$host', username='$username', password='$password', mqttPort=$mqttPort, incoming='$incoming', outgoing='$outgoing')"
}
