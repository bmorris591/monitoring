package uk.co.borismorris.dyson.airqualitymonitor.mock.worker

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import io.github.davidepianca98.MQTTClient
import io.github.davidepianca98.mqtt.MQTTVersion
import io.github.davidepianca98.mqtt.Subscription
import io.github.davidepianca98.mqtt.packets.Qos
import io.github.davidepianca98.mqtt.packets.mqtt.MQTTPublish
import io.github.davidepianca98.mqtt.packets.mqttv5.ReasonCode.SUCCESS
import io.github.davidepianca98.mqtt.packets.mqttv5.SubscriptionOptions
import io.micrometer.observation.Observation
import io.micrometer.observation.Observation.Event
import io.micrometer.observation.ObservationRegistry
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.core.task.AsyncTaskExecutor
import uk.co.borismorris.dyson.airqualitymonitor.mock.conf.MockFanConf
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.CurrentHeatCapableDeviceState
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.MessageType
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.MqttDeviceStatusResponse
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.MqttRequestMessage
import uk.co.borismorris.dyson.airqualitymonitor.mqtt.client.heatCapableDeviceModule
import uk.co.borismorris.monitoring.logging.KotlinLogging
import java.nio.charset.StandardCharsets
import java.time.ZonedDateTime
import java.util.concurrent.CompletableFuture
import java.util.concurrent.TimeUnit

private val logger = KotlinLogging.logger {}

class DysonFanRunner(
    private val observationRegistry: ObservationRegistry,
    private val mockFanConf: MockFanConf,
    private val executor: AsyncTaskExecutor,
    objectMapper: ObjectMapper,
) : ApplicationRunner,
    AutoCloseable {
    private val objectMapper =
        objectMapper.copy()
            .registerModule(heatCapableDeviceModule)
    private val mqttClient = MQTTClient(
        mqttVersion = MQTTVersion.MQTT3_1_1,
        address = mockFanConf.host,
        port = mockFanConf.mqttPort,
        clientId = "mock-fan",
        userName = mockFanConf.username,
        password = mockFanConf.password.toByteArray(StandardCharsets.UTF_8).toUByteArray(),
        tls = null,
        publishReceived = this::respond,
    )
    private lateinit var promise: CompletableFuture<*>

    override fun run(args: ApplicationArguments?) {
        mqttClient.subscribe(listOf(Subscription(mockFanConf.incoming, SubscriptionOptions(Qos.AT_LEAST_ONCE))))
        promise = executor.submitCompletable {
            while (mqttClient.isRunning()) {
                mqttClient.step()
                Thread.yield()
                TimeUnit.MILLISECONDS.sleep(100)
            }
        }
        promise.whenComplete { _, t ->
            if (t != null) {
                logger.atError().setCause(t).setMessage("Mqtt Client error").log()
            }
        }
    }

    private fun respond(message: MQTTPublish) {
        val observation = Observation.createNotStarted("respond", observationRegistry)
            .highCardinalityKeyValue("topic.incoming", mockFanConf.incoming)
            .highCardinalityKeyValue("topic.outgoing", mockFanConf.outgoing)
        observation.observe {
            observation.event(Event.of("message_received"))
            val request = objectMapper.readValue<MqttRequestMessage>(checkNotNull(message.payload?.toByteArray()))
            logger.atDebug().setMessage("Decoded message").addKeyValue("messsage", request).log()
            observation.event(Event.of("message_decoded"))
            val response = MqttDeviceStatusResponse(CurrentHeatCapableDeviceState()).apply {
                time = ZonedDateTime.now()
                type = MessageType.DEVICE_CURRENT_STATE
            }
            logger.atDebug().setMessage("Sending message").addKeyValue("response", response).log()

            mqttClient.publish(false, Qos.AT_LEAST_ONCE, mockFanConf.outgoing, response.toBytes().toUByteArray())
            observation.event(Event.of("response_sent"))
        }
    }

    private fun Any.toBytes() = objectMapper.writeValueAsBytes(this)

    override fun close() {
        mqttClient.disconnect(SUCCESS)
        promise.cancel(true)
    }
}
