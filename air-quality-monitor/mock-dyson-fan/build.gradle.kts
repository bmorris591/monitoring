plugins {
    monitoring.`application-conventions`
}

group = "uk.co.borismorris.dyson.airqualitymonitor"
val mainClassKt = "uk.co.borismorris.dyson.airqualitymonitor.mock.MockDysonFanKt"

dependencies {
    implementation(projects.airQualityMonitor.dysonMqttClient)
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation(libs.bundles.kmqtt)
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
}

application {
    mainClass.set(mainClassKt)
}
