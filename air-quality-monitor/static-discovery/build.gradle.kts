plugins {
    `java-library`
}

group = "uk.co.borismorris.dyson.airqualitymonitor"

dependencies {
    api(projects.airQualityMonitor.deviceDiscovery)

    implementation(projects.airQualityMonitor.localCredentialsDecoder)
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
}
