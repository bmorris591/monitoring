package uk.co.borismorris.dyson.airqualitymonitor.discovery

import uk.co.borismorris.dyson.airqualitymonitor.device.DeviceMetaData

interface DysonDeviceDiscovery {
    fun discoverDysonDevices(): Sequence<DeviceMetaData>
}
