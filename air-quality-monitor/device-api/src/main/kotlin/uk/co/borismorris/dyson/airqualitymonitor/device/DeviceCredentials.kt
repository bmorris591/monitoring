package uk.co.borismorris.dyson.airqualitymonitor.device

interface DeviceCredentials {
    val serial: String
    val password: String
}
