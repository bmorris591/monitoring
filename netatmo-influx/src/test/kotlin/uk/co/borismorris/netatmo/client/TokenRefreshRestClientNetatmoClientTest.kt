package uk.co.borismorris.netatmo.client

import com.github.tomakehurst.wiremock.client.WireMock.exactly
import com.github.tomakehurst.wiremock.client.WireMock.getAllScenarios
import com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor
import com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor
import com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo
import com.github.tomakehurst.wiremock.client.WireMock.verify
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.oauth2.client.OAuth2AuthorizeRequest
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientManager
import org.springframework.test.context.TestPropertySource
import java.util.function.Consumer

@TestPropertySource(
    properties = [
        "netatmo.credentials.username=test-user-short-token",
        "netatmo.credentials.password=test-user-password-123-short-token",
    ],
)
internal class TokenRefreshRestClientNetatmoClientTest : BaseRestClientNetatmoClientTest() {

    @Autowired
    lateinit var manager: OAuth2AuthorizedClientManager

    @Test
    fun `When a token expires it is refreshed`() {
        // Get a token that's already expired
        val oAuth2AuthorizeRequest = OAuth2AuthorizeRequest
            .withClientRegistrationId("netatmo")
            .principal(SecurityContextHolder.getContext().authentication)
            .build()
        manager.authorize(oAuth2AuthorizeRequest)

        val stationData = client.queryStationData(deviceId)
        assertThat(stationData.status).isEqualTo("ok")

        verify(exactly(2), postRequestedFor(urlPathEqualTo("/token")))
        verify(exactly(1), getRequestedFor(urlPathEqualTo("/api/getstationsdata")))

        assertThat(getAllScenarios()).singleElement().satisfies(
            Consumer { scenario ->
                assertThat(scenario.state).isEqualTo("Finished")
            },
        )
    }
}
