package uk.co.borismorris.netatmo.client

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments.arguments
import org.junit.jupiter.params.provider.MethodSource

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class DeviceIdTest {

    @ParameterizedTest(name = "Binary address {0} becomes {1} when formatted")
    @MethodSource("validHwAddressess")
    fun `Legal MAC address formats correctly`(input: List<Byte>, output: String) {
        val id = DeviceId.from(*input.toByteArray())
        assertThat(id.toString()).isEqualTo(output)
    }

    fun validHwAddressess() = listOf(
        arguments(listOf(0x00, 0x0, 0x0, 0x0, 0x0, 0x0), "00:00:00:00:00:00"),
        arguments(listOf(0x70, 0xEE.toByte(), 0x50, 0x22, 0xA3.toByte(), 0x00), "70:EE:50:22:A3:00"),
    )
}
