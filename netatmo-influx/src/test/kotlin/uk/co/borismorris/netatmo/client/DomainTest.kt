package uk.co.borismorris.netatmo.client

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class DomainTest {

    private val mapper = Jackson2ObjectMapperBuilder.json()
        .findModulesViaServiceLoader(true)
        .modulesToInstall(netatmoApiModule)
        .build<ObjectMapper>()

    @ParameterizedTest
    @ValueSource(
        strings = [
            "/uk/co/borismorris/netatmo/client/wiremock/__files/examplestationsdata.json",
            "/uk/co/borismorris/netatmo/client/wiremock/__files/ninemmstationdata.json",
        ],
    )
    fun `When station data is received it is successfully deserialised`(dataFile: String) {
        val stationData = DomainTest::class.java.getResourceAsStream(dataFile).use {
            mapper.readValue<NetatmoResponse<StationDataBody>>(it)
        }
        assertThat(stationData).isNotNull()
    }

    @Test
    fun `When unoptimized metrics are received they are successfully deserialised`() {
        val metricsData = DomainTest::class.java.getResourceAsStream("/uk/co/borismorris/netatmo/client/wiremock/__files/basestationmeasurements.json").use {
            mapper.readValue<NetatmoResponse<UnoptimizedMetricsBody>>(it)
        }
        assertThat(metricsData).isNotNull()
    }

    @Test
    fun `When optimized metrics are received they are successfully deserialised`() {
        val metricsData = DomainTest::class.java.getResourceAsStream("/uk/co/borismorris/netatmo/client/wiremock/__files/basestationmeasurementsoptimized.json").use {
            mapper.readValue<NetatmoResponse<OptimizedMetricsBody>>(it)
        }
        assertThat(metricsData).isNotNull()
    }

    @Test
    fun `When an error is received it is successfully deserialised`() {
        val error = DomainTest::class.java.getResourceAsStream("/uk/co/borismorris/netatmo/client/wiremock/__files/netatmoerror.json").use {
            mapper.readValue<NetatmoErrorResponse>(it)
        }
        assertThat(error).isNotNull()
    }
}
