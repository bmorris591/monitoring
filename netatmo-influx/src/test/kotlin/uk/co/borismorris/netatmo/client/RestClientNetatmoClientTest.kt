package uk.co.borismorris.netatmo.client

import com.github.tomakehurst.wiremock.client.WireMock.exactly
import com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor
import com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor
import com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo
import com.github.tomakehurst.wiremock.client.WireMock.verify
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.test.context.TestPropertySource
import uk.co.borismorris.netatmo.client.MeasurementType.CO2
import uk.co.borismorris.netatmo.client.MeasurementType.Humidity
import uk.co.borismorris.netatmo.client.MeasurementType.Noise
import uk.co.borismorris.netatmo.client.MeasurementType.Pressure
import uk.co.borismorris.netatmo.client.MeasurementType.Temperature
import java.time.Instant

@TestPropertySource(
    properties = [
        "netatmo.credentials.username=test-user",
        "netatmo.credentials.password=test-user-password-123",
    ],
)
internal class RestClientNetatmoClientTest : BaseRestClientNetatmoClientTest() {
    @Test
    fun `When station data is requested then the user is authenticated`() {
        val stationData = client.queryStationData(deviceId)
        assertThat(stationData.status).isEqualTo("ok")

        verify(exactly(1), postRequestedFor(urlPathEqualTo("/token")))
        verify(exactly(1), getRequestedFor(urlPathEqualTo("/api/getstationsdata")))
    }

    @Test
    fun `When metrics are requested then the user is authenticated`() {
        val device = BaseStation().apply {
            id = deviceId
            availableMeasurements = listOf(Temperature, CO2, Humidity, Noise, Pressure)
        }
        val metrics =
            client.queryDeviceMetrics(device, Instant.ofEpochSecond(1586440184)..Instant.ofEpochSecond(1586524798))

        assertThat(metrics.status).isEqualTo("ok")
        assertThat(metrics.body.metrics().toList()).hasSize(281)

        verify(exactly(1), postRequestedFor(urlPathEqualTo("/token")))
        verify(exactly(1), getRequestedFor(urlPathEqualTo("/api/getmeasure")))
    }

    @Test
    fun `When multiple requests for stationdata are sent the user is authenticated once`() {
        repeat(5) {
            val stationData = client.queryStationData(deviceId)
            assertThat(stationData.status).isEqualTo("ok")
        }

        verify(exactly(1), postRequestedFor(urlPathEqualTo("/token")))
        verify(exactly(5), getRequestedFor(urlPathEqualTo("/api/getstationsdata")))
    }
}
