package uk.co.borismorris.netatmo.client

import org.junit.jupiter.api.io.TempDir
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.wiremock.spring.ConfigureWireMock
import org.wiremock.spring.EnableWireMock
import java.nio.file.Files
import java.nio.file.Path
import kotlin.io.path.absolutePathString

@SpringBootTest(
    properties = [
        "spring.security.oauth2.client.registration.netatmo.client-id=test-client-id",
        "spring.security.oauth2.client.registration.netatmo.client-secret=test-client-secret",
        "spring.security.oauth2.client.registration.netatmo.scope=test-scope-1,test-scope-2",
        "scheduling.enabled=false",
        "spring.http.client.ssl.bundle=wiremock",
        "netatmo.url=\${wiremock.baseUrl}/api",
        "spring.security.oauth2.client.provider.netatmo.token-uri=\${wiremock.baseUrl}/token",
        "test-certs.wiremock.enabled=true",
    ],
)
@EnableWireMock(
    ConfigureWireMock(
        filesUnderClasspath = ["uk/co/borismorris/netatmo/client/wiremock"],
        httpsPort = 0,
        sslBundle = "wiremock",
    ),
)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@ActiveProfiles("test")
internal abstract class BaseRestClientNetatmoClientTest {
    companion object {
        @JvmStatic
        @TempDir
        lateinit var tempDir: Path

        @JvmStatic
        @DynamicPropertySource
        fun registerNetatmoProperties(registry: DynamicPropertyRegistry) {
            registry.add("netatmo.cache-path") {
                Files.createTempDirectory(tempDir, "token-refresh").resolve("oauth_client_registry.json")
                    .absolutePathString()
            }
        }
    }

    val deviceId = DeviceId.from(0x70, 0xEE.toByte(), 0x50, 0x22, 0xA3.toByte(), 0x00)

    @Autowired
    lateinit var client: RestClientNetatmoClient
}
