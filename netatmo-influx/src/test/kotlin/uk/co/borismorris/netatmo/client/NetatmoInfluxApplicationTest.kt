package uk.co.borismorris.netatmo.client

import org.springframework.modulith.core.ApplicationModules
import org.springframework.modulith.docs.Documenter
import uk.co.borismorris.netatmo.NetatmoInfluxApplication
import kotlin.test.Test

class NetatmoInfluxApplicationTest {

    @Test
    fun writeDocumentationSnippets() {
        var modules = ApplicationModules.of(NetatmoInfluxApplication::class.java).verify()
        Documenter(modules)
            .writeModulesAsPlantUml()
            .writeIndividualModulesAsPlantUml()
    }
}
