package uk.co.borismorris.netatmo.worker

import com.influxdb.client.InfluxDBClientOptions
import com.influxdb.client.domain.WritePrecision
import com.influxdb.client.write.Point
import com.influxdb.client.write.WriteParameters
import com.influxdb.query.FluxRecord
import com.influxdb.query.dsl.Flux
import com.influxdb.query.dsl.functions.restriction.Restrictions.measurement
import com.influxdb.query.dsl.functions.restriction.Restrictions.tag
import io.micrometer.context.ContextSnapshotFactory
import io.micrometer.observation.Observation
import io.micrometer.observation.ObservationRegistry
import org.springframework.scheduling.annotation.Scheduled
import uk.co.borismorris.monitoring.influxclient.InfluxClient
import uk.co.borismorris.monitoring.logging.KotlinLogging
import uk.co.borismorris.netatmo.client.BaseStation
import uk.co.borismorris.netatmo.client.DataObservation
import uk.co.borismorris.netatmo.client.DeviceId
import uk.co.borismorris.netatmo.client.NetatmoClient
import uk.co.borismorris.netatmo.client.NetatmoDevice
import uk.co.borismorris.netatmo.client.StationDataBody
import uk.co.borismorris.netatmo.influx.NetatmoDeviceConverter.toPoints
import uk.co.borismorris.netatmo.influx.NetatmoObservationConverter.toPoints
import java.time.Instant
import java.time.Instant.EPOCH
import java.time.Instant.now
import java.util.concurrent.StructuredTaskScope
import java.util.concurrent.StructuredTaskScope.Subtask.State.SUCCESS
import java.util.concurrent.atomic.AtomicReference
import java.util.function.Supplier

private val logger = KotlinLogging.logger {}

class GatherNetatmoData(
    private val observationRegistry: ObservationRegistry,
    private val netatmoClient: NetatmoClient,
    private val influxConfig: InfluxDBClientOptions,
    private val influxClient: InfluxClient,
) {

    private val stationData = AtomicReference<StationDataBody>()

    @Scheduled(fixedRateString = "\${netatmo.station-polling-interval}", initialDelay = 0L)
    fun updateStationData() {
        Observation.createNotStarted("update-state-data", observationRegistry)
            .observe {
                val stationData = netatmoClient.queryStationData().body
                logger.atInfo().setMessage("Got station data").addKeyValue("stationData") { stationData.toString() }
                    .log()
                this.stationData.set(stationData)
                stationData.recordDeviceStatuses()
            }
    }

    @Scheduled(
        fixedRateString = "\${netatmo.device-polling-interval}",
        initialDelayString = "\${netatmo.device-polling-interval}",
    )
    fun updateDeviceData() {
        Observation.createNotStarted("update-device-data", observationRegistry)
            .observe {
                this.stationData.get()?.let { updateDeviceData(it) }
            }
    }

    private fun updateDeviceData(stationData: StationDataBody) {
        val factory = Thread.ofVirtual().factory()
        StructuredTaskScope<List<Point>>("updateDeviceData") { r ->
            factory.newThread(
                ContextSnapshotFactory.builder().build().captureAll().wrap(r),
            )
        }.use { scope ->
            val tasks = stationData.allDevices().map {
                scope.fork {
                    getDeviceData(it).toList()
                }
            }.toList()

            scope.join()

            val (succeeded, failed) = tasks.partition { it.state() == SUCCESS }

            failed.forEach {
                @Suppress("ThrowingExceptionsWithoutMessageOrCause")
                logger.atError().setCause(it.exception()).setMessage("Task failed").log()
            }

            val points = succeeded.asSequence()
                .flatMap { it.get() }
                .onEach {
                    logger.atTrace().setMessage("Got point").addKeyValue("lineProtocol") { it.toLineProtocol() }.log()
                }
                .toList()
            influxClient.write(points)
        }
    }

    private fun <T : DataObservation> getDeviceData(device: NetatmoDevice<T>): Sequence<Point> = Observation.createNotStarted("get-device-data", observationRegistry)
        .highCardinalityKeyValue("device.id", device.id.toString())
        .highCardinalityKeyValue("device.type", device.type.toString())
        .highCardinalityKeyValue("device.name", device.moduleName)
        .observe(
            Supplier {
                val deviceMetrics = netatmoClient.queryDeviceMetrics(device, getMostRecentMeter(checkNotNull(device.id))..now())
                logger.atInfo().setMessage("Got device metrics data")
                    .addKeyValue("deviceMetrics") { deviceMetrics.toString() }
                    .log()
                deviceMetrics.body.metrics()
                    .map { NetatmoDeviceObservation(device, it) }
                    .onEach {
                        logger.atInfo().setMessage("Get device metric")
                            .addKeyValue("observation") { it.observation.toString() }
                            .log()
                    }
                    .flatMap { it.toPoints() }
            },
        ) ?: error("Failed to get points for device $device")

    private fun StationDataBody.recordDeviceStatuses() = allDevices().flatMap { it.toPoints() }
        .onEach { logger.atTrace().setMessage("Got point").addKeyValue("lineProtocol") { it.toLineProtocol() }.log() }
        .toList()
        .also {
            influxClient.write(it)
        }

    private fun StationDataBody.allDevices() = devices.asSequence()
        .flatMap { it.allDevices() }

    private fun BaseStation.allDevices() = (sequenceOf(this) + modules.asSequence())

    private fun getMostRecentMeter(id: DeviceId) = influxClient.query(
        Flux.from(checkNotNull(influxConfig.bucket)).range(EPOCH)
            .filter(measurement().equal("weather-observation"))
            .filter(tag("device-id").equal(id.toString()))
            .last().toString(),
    )
        .asSequence()
        .flatMap { it.records.asSequence() }
        .getMostRecentMeter()
        .also {
            logger.atDebug().setMessage("Most recent time stamps")
                .addKeyValue("device", id)
                .addKeyValue("timestamp", it)
                .log()
        }

    private fun Sequence<FluxRecord>.getMostRecentMeter(): Instant {
        logger.atDebug().setMessage("Got query results").addKeyValue("queryResults", this).log()
        return mapNotNull { it.time }.maxOrNull() ?: EPOCH
    }

    private fun InfluxClient.write(measurements: List<Point>) = writePoints(measurements, WriteParameters(WritePrecision.MS, null))
}

data class NetatmoDeviceObservation<T : DataObservation>(val device: NetatmoDevice<T>, val observation: T)
