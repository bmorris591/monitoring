package uk.co.borismorris.netatmo.influx

import com.influxdb.client.domain.WritePrecision
import com.influxdb.client.write.Point
import uk.co.borismorris.netatmo.client.AdditionalDevice
import uk.co.borismorris.netatmo.client.AtmosphericPressureData
import uk.co.borismorris.netatmo.client.BaseStation
import uk.co.borismorris.netatmo.client.Co2Data
import uk.co.borismorris.netatmo.client.DataObservation
import uk.co.borismorris.netatmo.client.HumidityData
import uk.co.borismorris.netatmo.client.NetatmoDevice
import uk.co.borismorris.netatmo.client.NoiseData
import uk.co.borismorris.netatmo.client.RainData
import uk.co.borismorris.netatmo.client.TemperatureData
import uk.co.borismorris.netatmo.client.WindData
import uk.co.borismorris.netatmo.worker.NetatmoDeviceObservation
import java.time.Instant

object NetatmoObservationConverter {

    fun NetatmoDeviceObservation<*>.toPoints() = sequenceOf(recordWeatherObservation()).filter { it.hasFields() }

    private fun NetatmoDeviceObservation<*>.recordWeatherObservation() = measure("weather-observation") {
        fun TemperatureData.recordTemperatureObservation() {
            addField("temperature", temperature)
        }

        fun HumidityData.recordHumidityObservation() {
            addField("humidity", humidity)
        }

        fun Co2Data.recordCo2DObservation() {
            addField("CO2", co2ppm)
        }

        fun NoiseData.recordNoiseObservation() {
            addField("noise", noiseDb)
        }

        fun AtmosphericPressureData.recordAtmosphericPressureObservation() {
            addField("pressure", pressure)
        }

        fun RainData.recordRainObservation() {
            addField("rain", rainMm)
        }

        fun WindData.recordWindObservation() {
            addField("wind-strength", windGustStrength)
            addField("wind-angle", windAngle)
            addField("wind-gust-strength", windGustStrength)
            addField("wind-gust-angle", windGustAngle)
        }

        if (observation is TemperatureData) {
            observation.recordTemperatureObservation()
        }
        if (observation is HumidityData) {
            observation.recordHumidityObservation()
        }
        if (observation is Co2Data) {
            observation.recordCo2DObservation()
        }
        if (observation is NoiseData) {
            observation.recordNoiseObservation()
        }
        if (observation is AtmosphericPressureData) {
            observation.recordAtmosphericPressureObservation()
        }
        if (observation is RainData) {
            observation.recordRainObservation()
        }
        if (observation is WindData) {
            observation.recordWindObservation()
        }
    }

    private fun <T : DataObservation> NetatmoDeviceObservation<T>.measure(
        measurement: String,
        measurementBuilder: Point.() -> Unit,
    ) = device.measure(measurement, observation.observationDate) {
        measurementBuilder(this)
    }
}

object NetatmoDeviceConverter {

    fun NetatmoDevice<*>.toPoints() = sequenceOf(recordDeviceStatus()).filter { it.hasFields() }

    private fun NetatmoDevice<*>.recordDeviceStatus() = measure("device-status", Instant.now()) {
        fun AdditionalDevice<*>.recordDeviceStatus() {
            addField("battery-percent", batteryPercent)
            addField("last-message", lastMessage.toString())
            addField("last-seen", lastSeen.toString())
            addField("rf-state", rfStatus)
            addField("battery-voltage", batteryVoltage)
        }

        fun BaseStation.recordBaseStationStatus() {
            addField("date-setup", createdAt.toString())
            addField("last-status-store", lastStatusStoreAt.toString())
            addField("wifi-status", wifiStatus)
            addField("read-only", readOnly.toString())
            addField("location-altitude", location.altitude)
            addField("location-timezone", location.timeZone.toString())
            with(location.location) {
                addField("location-latitude", latitude)
                addField("location-longitude", longitude)
            }
        }

        addField("last-setup", lastSetupAt.toString())
        addField("firmware-version", firmwareVersion)
        addField("last-upgrade", lastUpgradeDate.toString())
        addField("reachable", reachable.toString())

        when (this@recordDeviceStatus) {
            is BaseStation -> recordBaseStationStatus()
            is AdditionalDevice<*> -> recordDeviceStatus()
        }
    }
}

internal fun <T : DataObservation> NetatmoDevice<T>.measure(
    measurement: String,
    date: Instant,
    measurementBuilder: Point.() -> Unit,
) = Point(measurement).apply {
    time(date, WritePrecision.MS)
    addTag("device-id", id.toString())
    addTag("device-type", type.toString())
    addTag("device-name", moduleName)
    measurementBuilder(this)
}
