package uk.co.borismorris.netatmo.config.props

import uk.co.borismorris.monitoring.oauth2.ClientCredentials
import java.net.URI

class NetatmoProps {
    lateinit var url: URI
    val credentials = NetamoCredentials()

    override fun toString() = "NetamoProps(url=$url,, credentials=$credentials)"
}

class NetamoCredentials : ClientCredentials {
    override lateinit var username: String
    override lateinit var password: String

    override fun toString() = "NetamoCredentials(username='$username', password='XXXX')"
}
