package uk.co.borismorris.netatmo

import com.influxdb.client.InfluxDBClientOptions
import io.micrometer.observation.ObservationRegistry
import org.springframework.beans.factory.ListableBeanFactory
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.oauth2.client.web.client.OAuth2ClientHttpRequestInterceptor
import org.springframework.web.client.RestClient
import uk.co.borismorris.monitoring.influxclient.InfluxClient
import uk.co.borismorris.monitoring.influxclient.InfluxDB2Properties
import uk.co.borismorris.monitoring.observability.logging.LogConfigOnStartup
import uk.co.borismorris.netatmo.client.NetatmoClient
import uk.co.borismorris.netatmo.client.RestClientNetatmoClient
import uk.co.borismorris.netatmo.client.netatmoApiModule
import uk.co.borismorris.netatmo.config.props.NetatmoProps
import uk.co.borismorris.netatmo.worker.GatherNetatmoData

@Suppress("TooManyFunctions", "LongParameterList")
@SpringBootApplication(proxyBeanMethods = false)
class NetatmoInfluxApplication {
    companion object {
        init {
            SecurityContextHolder.setStrategyName(SecurityContextHolder.MODE_GLOBAL)
        }
    }

    @ConfigurationProperties(prefix = "netatmo")
    @Bean
    fun netatmoProps() = NetatmoProps()

    @Bean
    fun jacksonCustomiser() = Jackson2ObjectMapperBuilderCustomizer {
        it.findModulesViaServiceLoader(true)
            .modulesToInstall(netatmoApiModule)
    }

    @Bean
    fun netatmoClient(
        restClientBuilder: RestClient.Builder,
        netatmoProps: NetatmoProps,
        authFilter: OAuth2ClientHttpRequestInterceptor,
    ) = RestClientNetatmoClient(
        restClientBuilder.requestInterceptor(authFilter),
        netatmoProps,
    )

    @Bean
    fun gatherNetatmoData(
        observationRegistry: ObservationRegistry,
        netatmoClient: NetatmoClient,
        influxConfig: InfluxDBClientOptions,
        influxClient: InfluxClient,
    ) = GatherNetatmoData(observationRegistry, netatmoClient, influxConfig, influxClient)

    @Bean
    fun logConfigOnStartup(context: ListableBeanFactory) = LogConfigOnStartup(context, NetatmoProps::class, InfluxDB2Properties::class)

    @Configuration(proxyBeanMethods = false)
    @ConditionalOnProperty(value = ["scheduling.enabled"], havingValue = "true", matchIfMissing = true)
    @EnableScheduling
    class Scheduling
}

@Suppress("SpreadOperator")
fun main(args: Array<String>) {
    runApplication<NetatmoInfluxApplication>(*args)
}
