package uk.co.borismorris.netatmo.client

import inet.ipaddr.MACAddressString
import inet.ipaddr.mac.MACAddress
import java.time.Instant

interface NetatmoClient {

    fun queryStationData(deviceId: DeviceId? = null): NetatmoResponse<StationDataBody>

    fun <T : DataObservation> queryDeviceMetrics(
        device: NetatmoDevice<T>,
        range: ClosedRange<Instant>,
    ): NetatmoResponse<NetatmoDeviceMetrics<T>>
}

data class DeviceId(private val hardwareAddress: MACAddress) {
    companion object {
        fun from(vararg hardwareAddress: Byte): DeviceId {
            assert(hardwareAddress.size == 6) { "MAC address must 6 bytes." }
            return DeviceId(MACAddress(hardwareAddress))
        }

        fun from(hardwareAddress: String) = DeviceId(MACAddressString(hardwareAddress).toAddress())
    }
    override fun toString(): String = hardwareAddress.toColonDelimitedString().uppercase()
}
