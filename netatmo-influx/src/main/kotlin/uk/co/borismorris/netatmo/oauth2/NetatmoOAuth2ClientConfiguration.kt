@file:Suppress("DEPRECATION")

package uk.co.borismorris.netatmo.oauth2

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.convert.converter.Converter
import org.springframework.security.oauth2.client.OAuth2AuthorizationFailureHandler
import org.springframework.security.oauth2.core.OAuth2AccessToken.TokenType.BEARER
import org.springframework.security.oauth2.core.endpoint.DefaultMapOAuth2AccessTokenResponseConverter
import org.springframework.security.oauth2.core.endpoint.OAuth2AccessTokenResponse
import org.springframework.security.oauth2.core.http.converter.OAuth2AccessTokenResponseHttpMessageConverter
import uk.co.borismorris.monitoring.oauth2.AuthorizedClientServiceOAuth2AuthorizedClientManagerCustomizer
import uk.co.borismorris.monitoring.oauth2.OauthCachePath
import uk.co.borismorris.netatmo.config.props.NetatmoProps
import java.nio.file.Path
import java.util.*

@Configuration(proxyBeanMethods = false)
class NetatmoOAuth2ClientConfiguration {
    @Bean
    @OauthCachePath
    fun cachePath(@Value("\${netatmo.cache-path}") path: Path) = path

    @Bean
    fun clientCredentials(netatmoProps: NetatmoProps) = netatmoProps.credentials

    @Bean
    fun oAuth2AccessTokenResponseHttpMessageConverter() = OAuth2AccessTokenResponseHttpMessageConverter().also {
        it.setAccessTokenResponseConverter(accessTokenConverter)
    }

    @Bean
    fun noopFailureHandlerClientManagerCustomizer() = AuthorizedClientServiceOAuth2AuthorizedClientManagerCustomizer {
        it.setAuthorizationFailureHandler(OAuth2AuthorizationFailureHandler { _, _, _ -> })
    }
}

/**
 * Netatmo returns a response that
 * - is missing the token_type property
 * - has the scope property as a JSON list not a space delimited String
 */
private val accessTokenConverter = object : Converter<Map<String, Any>, OAuth2AccessTokenResponse> {
    private val defaultAccessTokenConverter = DefaultMapOAuth2AccessTokenResponseConverter()

    override fun convert(params: Map<String, Any>): OAuth2AccessTokenResponse? = defaultAccessTokenConverter.convert(
        buildMap {
            putAll(params)
            putIfAbsent("token_type", BEARER.value)
            computeIfPresent("scope") { _, value ->
                if (value is Iterable<*>) {
                    value.joinToString(separator = " ", transform = Objects::toString)
                } else {
                    value
                }
            }
        },
    )
}
