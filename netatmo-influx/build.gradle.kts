plugins {
    monitoring.`application-conventions`
    monitoring.`jib-conventions`
}

group = "uk.co.borismorris.netatmo"

dependencies {
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation(projects.springOauth2)
    implementation(projects.restclientInfluxClient)
    implementation(libs.ipaddresss)
    implementation("org.springframework.modulith:spring-modulith-api")

    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.springframework.modulith:spring-modulith-starter-test")
    testImplementation(projects.wiremockSpringBoot)
    testImplementation(projects.testCerts.springTestCerts)

    integrationTestImplementation(projects.restclientInfluxClientTestcontainers)
}

application {
    mainClass.set("uk.co.borismorris.netatmo.NetatmoInfluxApplicationKt")
}
