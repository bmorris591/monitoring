#!/usr/bin/env -S bash -e

process_project() {
    local project=$1
    local path="${project##*/}"
    local name="${path%%.*}"
    echo "Processing $project $path $name"
    git remote add -f $name $project
    git subtree add --prefix $name $name master
}

main() {
    local project_list="./merged-projects.txt"
    cat $project_list | while read project; do
        process_project $project
    done
}

main "$@"