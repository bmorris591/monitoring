package uk.co.borismorris.monitoring.testcerts.extension

import org.junit.jupiter.api.extension.AfterAllCallback
import org.junit.jupiter.api.extension.BeforeAllCallback
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.api.extension.ParameterContext
import org.junit.jupiter.api.extension.ParameterResolver
import uk.co.borismorris.monitoring.testcerts.KeyStoreFile
import uk.co.borismorris.monitoring.testcerts.TestCertsProperties
import uk.co.borismorris.monitoring.testcerts.createCertificate
import java.net.InetAddress
import java.nio.file.Files
import java.nio.file.Path
import java.security.KeyStore
import kotlin.io.path.createDirectories
import kotlin.io.path.deleteRecursively

@Suppress("LongParameterList")
class TestCertsExtension(
    private val rootCn: String,
    private val serverCn: String,
    private val domains: Set<String>,
    private val addresses: Set<InetAddress>,
    keyStoreFile: KeyStoreFile,
    trustStoreFile: KeyStoreFile,
) : BeforeAllCallback,
    AfterAllCallback,
    ParameterResolver {

    companion object {
        fun builder() = Builder()
        fun create() = builder().build()
    }

    private val certificatesDirectory: Path = Files.createTempDirectory("test-certs")
    val properties = TestCertsProperties(
        certificatesDirectory,
        keyStoreFile,
        trustStoreFile,
    )

    override fun beforeAll(extensionContext: ExtensionContext) {
        certificatesDirectory.createDirectories()

        val root = generateCA()
        val server = root.generateCert()
        properties.initKeyStores(server)
    }

    private fun generateCA() = createCertificate(rootCn, emptySet(), emptySet(), null, true)

    private fun KeyStore.PrivateKeyEntry.generateCert() = createCertificate(serverCn, domains, addresses, this, false)

    override fun afterAll(extensionContext: ExtensionContext) {
        certificatesDirectory.deleteRecursively()
    }

    override fun supportsParameter(parameterContext: ParameterContext, extensionContext: ExtensionContext) = parameterContext.parameter.type == TestCertsProperties::class.java

    override fun resolveParameter(parameterContext: ParameterContext, extensionContext: ExtensionContext): Any? {
        if (!supportsParameter(parameterContext, extensionContext)) {
            return null
        }
        return properties
    }

    class Builder internal constructor() {
        var rootCn = "rootCA"
        var serverCn = "localhost"

        @Suppress("DoubleMutabilityForCollection")
        var domains = mutableSetOf("localhost")

        @Suppress("DoubleMutabilityForCollection")
        var addresses = InetAddress.getAllByName("localhost").toMutableSet()
        var keyStoreName = "localhost.p12"
        var keyStorePassword = "password"
        var keyStoreFormat: String = KeyStore.getDefaultType()
        var trustStoreName = "truststore.p12"
        var trustStorePassword = "password"
        var truststoreFormat: String = KeyStore.getDefaultType()

        fun build() = TestCertsExtension(
            rootCn,
            serverCn,
            domains,
            addresses,
            KeyStoreFile(keyStoreName, keyStorePassword, keyStoreFormat),
            KeyStoreFile(trustStoreName, trustStorePassword, truststoreFormat),
        )
    }
}
