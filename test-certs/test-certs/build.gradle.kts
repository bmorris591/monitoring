plugins {
    `java-library`
}

group = "uk.co.borismorris.testcerts"

dependencies {
    api("org.slf4j:slf4j-api")
    api(libs.bouncycastle)
}
