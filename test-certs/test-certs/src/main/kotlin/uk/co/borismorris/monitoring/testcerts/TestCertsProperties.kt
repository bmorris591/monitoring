package uk.co.borismorris.monitoring.testcerts

import java.nio.file.Path
import java.security.KeyStore

data class TestCertsProperties(
    val certificatesDirectory: Path,
    val keyStoreFile: KeyStoreFile,
    val trustStoreFile: KeyStoreFile,
) {

    lateinit var trustStore: KeyStore
        private set
    lateinit var keyStore: KeyStore
        private set

    val trustStoreLocation: Path by lazy { certificatesDirectory.resolve(trustStoreFile.fileName) }
    val keyStoreLocation: Path by lazy { certificatesDirectory.resolve(keyStoreFile.fileName) }

    fun initKeyStores(keyPair: KeyStore.PrivateKeyEntry) {
        keyStore = keyStoreFile.createKeyStore(keyPair)
        keyStore.write(keyStoreLocation, keyStoreFile.password)
        trustStore = trustStoreFile.createTrustStore(keyPair.certificateChain.last())
        trustStore.write(trustStoreLocation, trustStoreFile.password)
    }
}
