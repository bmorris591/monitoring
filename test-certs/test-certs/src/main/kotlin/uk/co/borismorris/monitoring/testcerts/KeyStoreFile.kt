package uk.co.borismorris.monitoring.testcerts

import java.security.KeyStore
import java.security.cert.Certificate

data class KeyStoreFile(
    val fileName: String,
    val password: String,
    val format: String,
) {
    internal fun createKeyStore(keyPair: KeyStore.PrivateKeyEntry) = createKeyStore(format, password, keyPair)
    internal fun createTrustStore(vararg roots: Certificate) = createTrustStore(format, password, *roots)
}
