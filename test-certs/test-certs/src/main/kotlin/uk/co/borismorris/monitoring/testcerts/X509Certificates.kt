package uk.co.borismorris.monitoring.testcerts

import org.bouncycastle.asn1.x500.X500Name
import org.bouncycastle.asn1.x500.X500NameBuilder
import org.bouncycastle.asn1.x500.style.BCStyle
import org.bouncycastle.asn1.x509.BasicConstraints
import org.bouncycastle.asn1.x509.Extension.basicConstraints
import org.bouncycastle.asn1.x509.Extension.subjectAlternativeName
import org.bouncycastle.asn1.x509.GeneralName
import org.bouncycastle.asn1.x509.GeneralNames
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder
import java.math.BigInteger
import java.net.InetAddress
import java.nio.file.Path
import java.security.KeyPairGenerator
import java.security.KeyStore
import java.security.KeyStore.PrivateKeyEntry
import java.security.PrivateKey
import java.security.cert.Certificate
import java.security.cert.X509Certificate
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.*
import kotlin.io.path.outputStream

const val EC = "EC"
const val SHA256_WITH_ECDSA = "SHA256withECDSA"

fun createCertificate(
    cnName: String,
    domains: Set<String>,
    addresses: Set<InetAddress>,
    issuer: PrivateKeyEntry?,
    isCA: Boolean,
): PrivateKeyEntry {
    val certKeyPair = generateKeypair()
    val name = X500NameBuilder(BCStyle.INSTANCE)
        .addRDN(BCStyle.CN, cnName)
        .build()

    val serialNumber = BigInteger.valueOf(System.currentTimeMillis())
    val validFrom = Instant.now()
    val validUntil = validFrom.plus(1, ChronoUnit.HOURS)
    val (issuerName, issuerKey) = issuer?.issuerDetails() ?: (name to certKeyPair.private)

    val cert: Certificate = JcaX509v3CertificateBuilder(
        issuerName,
        serialNumber,
        Date.from(validFrom),
        Date.from(validUntil),
        name,
        certKeyPair.public,
    ).run {
        if (isCA) {
            setCa()
        }
        addSans(domains, addresses)
        buildAndSign(issuerKey)
    }

    return PrivateKeyEntry(
        certKeyPair.private,
        arrayOf(cert) + issuer?.certificateChain.orEmpty(),
    )
}

private fun PrivateKeyEntry.issuerDetails(): Pair<X500Name, PrivateKey> = X500Name(certificate.subjectX500PrincipalName()) to privateKey

private fun generateKeypair() = KeyPairGenerator.getInstance(EC).generateKeyPair()

private fun JcaX509v3CertificateBuilder.buildAndSign(issuerKey: PrivateKey) = JcaContentSignerBuilder(SHA256_WITH_ECDSA).build(issuerKey).let {
    JcaX509CertificateConverter().getCertificate(build(it))
}

private fun JcaX509v3CertificateBuilder.setCa() {
    addExtension(basicConstraints, true, BasicConstraints(true))
}

private fun JcaX509v3CertificateBuilder.addSans(domains: Iterable<String>, addresses: Iterable<InetAddress>) {
    val dnsNames = domains.asSequence()
        .map { GeneralName(GeneralName.dNSName, it) }
    val ipAddresses = addresses.asSequence()
        .map { GeneralName(GeneralName.iPAddress, it.hostAddress) }
    val names = (dnsNames + ipAddresses).toList().toTypedArray()
    if (names.isNotEmpty()) {
        addExtension(subjectAlternativeName, false, GeneralNames(names))
    }
}

fun createKeyStore(
    format: String,
    password: String,
    keypair: PrivateKeyEntry,
): KeyStore = KeyStore.getInstance(format).apply {
    load(null, password.toCharArray())
    setEntry(
        keypair.subjectX500PrincipalName(),
        keypair,
        KeyStore.PasswordProtection(password.toCharArray()),
    )
}

fun createTrustStore(
    format: String,
    password: String,
    vararg roots: Certificate,
): KeyStore = KeyStore.getInstance(format).apply {
    load(null, password.toCharArray())
    roots.forEach {
        setCertificateEntry(
            it.subjectX500PrincipalName(),
            it,
        )
    }
}

private fun PrivateKeyEntry.subjectX500PrincipalName() = certificate.subjectX500PrincipalName()

private fun Certificate.subjectX500PrincipalName() = (this as? X509Certificate)?.subjectX500Principal?.name

fun KeyStore.write(destination: Path, password: String) {
    destination.outputStream().use {
        store(it, password.toCharArray())
    }
}
