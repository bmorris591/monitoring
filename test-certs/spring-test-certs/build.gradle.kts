plugins {
    `java-library`
}

group = "uk.co.borismorris.testcerts"

dependencies {
    api("org.slf4j:slf4j-api")
    implementation(projects.testCerts.testCerts)
    implementation("org.springframework.boot:spring-boot-starter-test")
}
