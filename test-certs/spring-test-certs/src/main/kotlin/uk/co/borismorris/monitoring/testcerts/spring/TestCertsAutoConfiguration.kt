package uk.co.borismorris.monitoring.testcerts.spring

import org.springframework.boot.autoconfigure.AutoConfigureBefore
import org.springframework.boot.autoconfigure.ssl.SslAutoConfiguration
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Lazy

@TestConfiguration
@EnableConfigurationProperties(TestCertsProperties::class)
@AutoConfigureBefore(SslAutoConfiguration::class)
class TestCertsAutoConfiguration {

    @Bean
    @Lazy(false)
    fun testCertGenerator(properties: TestCertsProperties): TestCertGenerator = TestCertGenerator(properties)
}
