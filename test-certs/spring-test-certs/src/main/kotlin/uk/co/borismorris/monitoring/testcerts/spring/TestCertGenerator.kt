package uk.co.borismorris.monitoring.testcerts.spring

import org.springframework.boot.autoconfigure.ssl.JksSslBundleProperties.Store
import org.springframework.boot.autoconfigure.ssl.SslBundleRegistrar
import org.springframework.boot.ssl.SslBundle
import org.springframework.boot.ssl.SslBundleKey
import org.springframework.boot.ssl.SslBundleRegistry
import org.springframework.boot.ssl.SslManagerBundle
import org.springframework.boot.ssl.SslOptions
import org.springframework.boot.ssl.SslStoreBundle
import uk.co.borismorris.monitoring.testcerts.createCertificate
import java.security.KeyStore
import java.security.cert.Certificate

class TestCertGenerator(private val properties: TestCertsProperties) : SslBundleRegistrar {

    override fun registerBundles(registry: SslBundleRegistry) {
        properties.testCerts.forEach { (name, cert) ->
            if (cert.enabled) registry.registerBundle(name, cert.toBundle())
        }
    }

    private fun TestCertProperties.toBundle(): SslBundle {
        val root = generateCA()
        val server = generateCert(root)
        val storeBundles = initKeyStores(server)
        val key = key?.run { SslBundleKey.of(password, alias) } ?: SslBundleKey.NONE
        val options = options?.run { SslOptions.of(ciphers, enabledProtocols) } ?: SslOptions.NONE
        val managers = SslManagerBundle.from(storeBundles, key)
        return SslBundle.of(storeBundles, key, options, protocol, managers)
    }

    private fun TestCertProperties.generateCA() = createCertificate(rootCn, emptySet(), emptySet(), null, true)

    private fun TestCertProperties.generateCert(root: KeyStore.PrivateKeyEntry) = createCertificate(serverCn, domains, addresses, root, false)

    fun TestCertProperties.initKeyStores(keyPair: KeyStore.PrivateKeyEntry): SslStoreBundle {
        val jksKeyStore = keystore.createKeyStore(keyPair)
        val jksTrustStore = truststore.createTrustStore(keyPair.certificateChain.last())
        return SslStoreBundle.of(jksKeyStore, keystore.password, jksTrustStore)
    }

    private fun Store.createKeyStore(keyPair: KeyStore.PrivateKeyEntry) = uk.co.borismorris.monitoring.testcerts.createKeyStore(type, password, keyPair)

    private fun Store.createTrustStore(vararg roots: Certificate) = uk.co.borismorris.monitoring.testcerts.createTrustStore(type, password, *roots)
}
