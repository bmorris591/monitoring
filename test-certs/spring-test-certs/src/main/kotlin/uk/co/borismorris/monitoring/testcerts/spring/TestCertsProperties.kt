package uk.co.borismorris.monitoring.testcerts.spring

import org.springframework.boot.autoconfigure.ssl.JksSslBundleProperties
import org.springframework.boot.context.properties.ConfigurationProperties
import java.net.InetAddress
import java.security.KeyStore

@ConfigurationProperties
data class TestCertsProperties(val testCerts: Map<String, TestCertProperties> = mapOf())

class TestCertProperties : JksSslBundleProperties() {
    var enabled: Boolean = true
    var rootCn: String = "rootCA"
    var serverCn: String = "localhost"
    var domains: Set<String> = setOf("localhost")
    var addresses = InetAddress.getAllByName("localhost").toSet()

    init {
        keystore.password = "password"
        keystore.location = "localhost.p12"
        keystore.type = KeyStore.getDefaultType()

        truststore.password = "password"
        truststore.location = "localhost.p12"
        truststore.type = KeyStore.getDefaultType()
    }
}
