import de.undercouch.gradle.tasks.download.Download
import io.gitlab.arturbosch.detekt.CONFIGURATION_DETEKT
import io.gitlab.arturbosch.detekt.Detekt
import io.gitlab.arturbosch.detekt.getSupportedKotlinVersion
import org.gradle.api.tasks.testing.logging.TestExceptionFormat.FULL
import org.jetbrains.kotlin.gradle.dsl.JvmTarget
import org.springframework.boot.gradle.plugin.SpringBootPlugin

plugins {
    java
    jacoco
    `project-report`
    `test-report-aggregation`
    alias(libs.plugins.kpublish)
    alias(libs.plugins.kotlin.jvm)
    alias(libs.plugins.kotlin.spring)
    alias(libs.plugins.catalog.update)
    alias(libs.plugins.download)
    alias(libs.plugins.spotless)
    alias(libs.plugins.detekt)
}

group = "uk.co.borismorris.monitoring"

val downloadDetektConfig by tasks.registering(Download::class) {
    src("https://gitlab.com/bmorris591/detekt/-/raw/main/detekt.yaml?inline=false")
    val detektConfig = layout.buildDirectory.file("detket.config")
    dest(detektConfig)
    overwrite(false)
}

allprojects {
    apply(plugin = "java")
    apply(plugin = "org.hildan.kotlin-publish")
    apply(plugin = "org.jetbrains.kotlin.jvm")
    apply(plugin = "org.jetbrains.kotlin.plugin.spring")
    apply(plugin = "com.diffplug.spotless")
    apply(plugin = "io.gitlab.arturbosch.detekt")
    apply(plugin = "jacoco")

    group = rootProject.group

    java {
        sourceCompatibility = JavaVersion.VERSION_22
        targetCompatibility = JavaVersion.VERSION_22

        withJavadocJar()
        withSourcesJar()
    }

    tasks.withType<Javadoc> {
        isFailOnError = false
        // -quiet
        options.quiet()
        // Different builds of openjdk have different doclint options; openjdk:11 has no linting by default
        // Some trickery required to pass -X arg to gradle:
        // https://discuss.gradle.org/t/how-to-send-x-option-to-javadoc/23384/4
        (options as? CoreJavadocOptions)?.addBooleanOption("Xdoclint:none", true)
    }

    allOpen {
        annotations("io.micrometer.observation.annotation.Observed")
    }

    kotlin {
        compilerOptions {
            jvmTarget.set(JvmTarget.JVM_22)
            freeCompilerArgs.addAll(
                "-Xjsr305=strict",
                "-Xannotation-default-target=param-property",
            )
        }
        sourceSets.all {
            languageSettings {
                languageVersion = "2.1"
                progressiveMode = true
                optIn("kotlin.RequiresOptIn")
                optIn("kotlin.ExperimentalUnsignedTypes")
                optIn("kotlin.ExperimentalStdlibApi")
                optIn("kotlin.time.ExperimentalTime")
                optIn("kotlin.io.path.ExperimentalPathApi")
            }
        }
    }

    val mockitoAgent by configurations.creating

    testing {
        suites {
            val test by getting(JvmTestSuite::class)
            val integrationTest by registering(JvmTestSuite::class) {
                sources {
                    compileClasspath += test.sources.output
                    runtimeClasspath += test.sources.output
                }

                dependencies {
                    implementation(project())
                }

                targets {
                    all {
                        testTask.configure {
                            systemProperty("cucumber.junit-platform.naming-strategy", "long")
                            shouldRunAfter(test)
                        }
                    }
                }
            }

            withType<JvmTestSuite> {
                useJUnitJupiter()
                targets {
                    all {
                        testTask.configure {
                            minHeapSize = "1G"
                            maxHeapSize = "4G"
                            jvmArgs(
                                "-XX:+UseZGC",
                                "-XX:+EnableDynamicAgentLoading",
                                "-javaagent:${mockitoAgent.asPath}",
                            )
                            testLogging {
                                exceptionFormat = FULL
                                showStandardStreams = true
                                events("skipped", "failed")
                            }
                        }
                    }
                }
            }
        }
    }

    val integrationTestImplementation by configurations.getting {
        extendsFrom(configurations.testImplementation.get())
    }
    val integrationTestRuntimeOnly by configurations.getting {
        extendsFrom(configurations.testRuntimeOnly.get())
    }

    configurations.matching { it.name == CONFIGURATION_DETEKT }.all {
        resolutionStrategy.eachDependency {
            if (requested.group == "org.jetbrains.kotlin") {
                useVersion(getSupportedKotlinVersion())
            }
        }
    }

    dependencies {
        implementation(platform(SpringBootPlugin.BOM_COORDINATES))
        implementation(platform(rootProject.libs.spring.cloud))
        implementation(platform(rootProject.libs.spring.modulith))

        implementation(kotlin("stdlib-jdk8"))
        implementation(kotlin("reflect"))

        implementation("io.micrometer:micrometer-core")
        implementation("io.micrometer:micrometer-tracing")

        implementation("org.springframework.modulith:spring-modulith-api")

        testImplementation("org.junit.jupiter:junit-jupiter-api")
        testImplementation("org.junit.jupiter:junit-jupiter-params")
        testImplementation("org.assertj:assertj-core")
        testImplementation("org.awaitility:awaitility")
        testImplementation("org.mockito:mockito-core")
        testImplementation("org.mockito:mockito-junit-jupiter")
        testImplementation(rootProject.libs.mockito.kotlin)
        testImplementation(kotlin("test-junit5"))

        testRuntimeOnly("ch.qos.logback:logback-classic")

        integrationTestImplementation(platform(rootProject.libs.cucumber))
        integrationTestImplementation(rootProject.libs.junit.suite.api)

        integrationTestRuntimeOnly(rootProject.libs.junit.suite)
        integrationTestRuntimeOnly("io.cucumber:cucumber-junit-platform-engine")

        mockitoAgent(platform(SpringBootPlugin.BOM_COORDINATES))
        mockitoAgent("org.mockito:mockito-core") {
            isTransitive = false
        }
    }

    tasks.jacocoTestReport {
        reports {
            xml.required = false
            csv.required = false
            html.required = true
        }
    }

    spotless {
        java {
            palantirJavaFormat()
            removeUnusedImports()
            cleanthat()
                .addMutator("SafeButNotConsensual")
                .addMutator("SafeButControversial")
                .addMutator("LocalVariableTypeInference")
                .excludeMutator("AvoidInlineConditionals")
                .excludeMutator("LiteralsFirstInComparisons")
                .excludeMutator("UseUnderscoresInNumericLiterals")
                .sourceCompatibility(java.sourceCompatibility.toString())
            targetExclude("**/build/generated/**")
        }
        kotlin {
            ktlint().editorConfigOverride(
                mapOf(
                    "ktlint_standard_no-wildcard-imports" to "disabled",
                ),
            )
            targetExclude("**/build/generated/**")
        }
        kotlinGradle {
            ktlint()
        }
        gherkin {
            target("src/**/*.feature")
            gherkinUtils()
        }
    }

    detekt {
        buildUponDefaultConfig = true
        allRules = true
        config.setFrom(downloadDetektConfig)
    }

    tasks["check"].dependsOn("detektMain")
    tasks["build"].dependsOn("integrationTest")

    tasks.withType<Detekt> {
        jvmTarget = "21"
        exclude {
            it.file.absolutePath.contains("/build/generated/")
        }
    }

    publishing {
        repositories {
            maven {
                name = "gitlab-ci"
                url = uri("https://gitlab.com/api/v4/projects/53458593/packages/maven")
                credentials(HttpHeaderCredentials::class) {
                    name = "Job-Token"
                    value = System.getenv("CI_JOB_TOKEN")
                }
                authentication {
                    create<HttpHeaderAuthentication>("token-header")
                }
            }
        }
    }

    tasks.check {
        dependsOn("integrationTest")
        finalizedBy(tasks.jacocoTestReport, tasks.jacocoTestCoverageVerification)
    }
    tasks.jacocoTestReport {
        dependsOn(tasks.check, "integrationTest")
    }
    tasks.jacocoTestCoverageVerification {
        dependsOn(tasks.check, "integrationTest")
    }
}

subprojects {
    val logging = rootProject.projects.logging
    if (name != logging.name) {
        dependencies {
            implementation(logging)
        }
    }
}

dependencies {
    subprojects.forEach {
        testReportAggregation(it)
    }
}

tasks {
    test {
        finalizedBy("testAggregateTestReport")
    }

    check {
        dependsOn("testAggregateTestReport", "integrationTestAggregateTestReport")
    }

    htmlDependencyReport {
        projects = project.allprojects
    }
}
