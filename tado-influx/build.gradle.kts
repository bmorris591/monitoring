plugins {
    monitoring.`application-conventions`
    monitoring.`jib-conventions`

    alias(libs.plugins.openapi.generator)
}

group = "uk.co.borismorris.tado"

dependencies {
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-cache")
    implementation("org.springframework.modulith:spring-modulith-api")
    implementation(projects.springOauth2)
    implementation("com.github.ben-manes.caffeine:caffeine")

    implementation(projects.restclientInfluxClient)

    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.springframework.modulith:spring-modulith-starter-test")
    testImplementation(projects.wiremockSpringBoot)
    testImplementation(projects.testCerts.springTestCerts)

    integrationTestImplementation(projects.restclientInfluxClientTestcontainers)
}

kotlin {
    sourceSets {
        main {
            kotlin.srcDir(tasks["openApiGenerate"])
        }
    }
}

tasks["sourcesJar"].dependsOn("compileJava")

openApiGenerate {
    validateSpec.set(true)
    generatorName.set("kotlin")
    library.set("jvm-spring-restclient")
    outputDir.set(layout.buildDirectory.dir("generated").map { it.asFile.absolutePath })
    inputSpec.set(file("src/main/swagger/com/tado/api_v2.yaml").toString())
    packageName.set("com.tado")
    apiPackage.set("com.tado.api")
    modelPackage.set("com.tado.model")
    generateApiTests.set(false)
    generateModelTests.set(false)
    configOptions.set(
        mapOf(
            "dateLibrary" to "java8",
            "sourceFolder" to "tado",
            "serializationLibrary" to "jackson",
            "useSpringBoot3" to "true",
        ),
    )
    typeMappings.set(
        mapOf(
            "float" to "BigDecimal",
            "double" to "BigDecimal",
        ),
    )
    importMappings.set(
        mapOf(
            "BigDecimal" to "java.math.BigDecimal",
        ),
    )
}

application {
    mainClass.set("uk.co.borismorris.tado.TadoInfluxApplicationKt")
}
