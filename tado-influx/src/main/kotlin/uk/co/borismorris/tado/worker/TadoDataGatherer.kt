@file:Suppress("UnsafeCallOnNullableType")

package uk.co.borismorris.tado.worker

import com.influxdb.client.domain.WritePrecision
import com.influxdb.client.write.Point
import com.influxdb.client.write.WriteParameters
import com.tado.model.HeatingZoneSetting
import com.tado.model.HomeState
import com.tado.model.MobileDevice
import com.tado.model.PercentageDataPoint
import com.tado.model.TemperatureDataPoint
import com.tado.model.ZoneState
import io.micrometer.context.ContextSnapshotFactory
import io.micrometer.observation.Observation
import io.micrometer.observation.ObservationRegistry
import org.springframework.scheduling.annotation.Scheduled
import uk.co.borismorris.monitoring.influxclient.InfluxClient
import uk.co.borismorris.monitoring.logging.KotlinLogging
import uk.co.borismorris.tado.api.TadoApi
import java.time.Instant
import java.util.concurrent.StructuredTaskScope
import java.util.concurrent.atomic.AtomicReference
import java.util.function.Supplier

private val logger = KotlinLogging.logger {}

class TadoDataGatherer(
    private val tadoApi: TadoApi,
    private val influxClient: InfluxClient,
    private val observationRegistry: ObservationRegistry,
) {
    private val homes = AtomicReference<List<TadoApi.User.Home>>()

    @Scheduled(initialDelay = 0, fixedRateString = "\${tado.user-polling-interval}")
    fun refreshHomes() {
        tadoApi.user()?.let { user ->
            logger.atInfo().setMessage("Monitoring devices for user").addKeyValue("user", user).log()
            val homes = user.homes()
            logger.atInfo().setMessage("Monitoring devices for homes").addKeyValue("homes", homes).log()
            this.homes.set(homes)
        } ?: logger.info("User not present, assume unchanged")
    }

    @Scheduled(
        initialDelayString = "\${tado.device-polling-interval}",
        fixedRateString = "\${tado.device-polling-interval}",
    )
    fun pollDevices() {
        homes.get()?.let {
            it.forEach { home -> home.pollDevices() }
        }
    }

    private fun TadoApi.User.Home.pollDevices() {
        logger.atInfo().setMessage("Polling Tado for new data for Home").addKeyValue("home", home).log()
        Observation.createNotStarted("poll-home-devices", observationRegistry)
            .lowCardinalityKeyValue("home.id", id.toString())
            .observe {
                influxClient.writePoints(toPoints(), WriteParameters(WritePrecision.MS, null))
            }
    }

    private fun TadoApi.User.Home.toPoints() = convert(Instant.now())

    private fun TadoApi.User.Home.convert(now: Instant): List<Point> {
        fun MobileDevice.convertDevice() = Point("device-state").apply {
            time(now.toEpochMilli(), WritePrecision.MS)
            addTag("home-id", this@convert.id.toString())
            addTag("device-id", id.toString())
            addTag("name", name)
            location?.apply {
                addField("at-home", atHome?.toString())
                addField("stale", stale?.toString())
            }
            settings?.geoTrackingEnabled?.let {
                addField("geo-tracking-enabled", it.toString())
            }
        }

        val factory = Thread.ofVirtual().factory()
        return StructuredTaskScope<List<Point>>("updateDeviceData") { r ->
            factory.newThread(
                ContextSnapshotFactory.builder().build().captureAll().wrap(r),
            )
        }.use { scope ->
            val tasks = buildList {
                add(scope.fork { convertHome(now) })
                add(scope.fork { devices().map { it.convertDevice() } })
                addAll(convertZones(scope, now))
            }

            scope.join()

            val (succeeded, failed) = tasks.partition { it.state() == StructuredTaskScope.Subtask.State.SUCCESS }

            failed.forEach {
                @Suppress("ThrowingExceptionsWithoutMessageOrCause")
                logger.atError().setCause(it.exception()).setMessage("Task failed").log()
            }

            succeeded.asSequence()
                .flatMap { it.get() }
                .onEach {
                    logger.atTrace().setMessage("Got point").addKeyValue("lineProtocol") { it.toLineProtocol() }.log()
                }
                .toList()
        }
    }

    private fun TadoApi.User.Home.convertZones(
        scope: StructuredTaskScope<List<Point>>,
        now: Instant,
    ): List<StructuredTaskScope.Subtask<List<Point>>> = Observation.createNotStarted("convert-zones", observationRegistry).observe(
        Supplier {
            zones().map { scope.fork { it.convertZone(now) } }.toList()
        },
    ).orEmpty()

    private fun TadoApi.User.Home.convertHome(now: Instant): List<Point> {
        fun HomeState.measurement() = Point("home-state").apply {
            time(now.toEpochMilli(), WritePrecision.MS)
            addTag("home-id", id.toString())
            addTag("name", name)
            addField("presence", presence?.value)
        }

        val observation = Observation.createNotStarted("convert-home", observationRegistry)
        return observation.observe(
            Supplier {
                state()?.let {
                    observation.lowCardinalityKeyValue("home.name", it.name ?: "Unknown")
                    listOf(it.measurement())
                }.orEmpty()
            },
        ).orEmpty()
    }

    @Suppress("CyclomaticComplexMethod", "NestedBlockDepth")
    private fun TadoApi.User.Home.Zone.convertZone(now: Instant): List<Point> {
        fun ZoneState.zoneMeasurement() = Point("zone-state").apply {
            time(now.toEpochMilli(), WritePrecision.MS)
            addTag("home-id", home.id.toString())
            addTag("zone-id", id.toString())
            addTag("name", name)
            addTag("type", type?.value)

            addField("zone-mode", tadoMode?.value)
            addField("window-open", openWindowDetected?.toString())

            link?.apply {
                addField("link-state", state?.value)
                reason?.apply {
                    addField("link-reason-code", code)
                    addField("link-reason-title", title)
                }
            }

            openWindow?.apply {
                addField("open-window-total-duration", durationInSeconds)
                addField("open-window-remaining-duration", remainingTimeInSeconds)
            }

            preparation?.apply {
                addField("preparation-tado-mode", tadoMode?.value)
                when (val setting = setting) {
                    is HeatingZoneSetting -> {
                        addField("preparation-heating-state", setting.power?.value)
                        setting.temperature?.celsius?.let {
                            addField("preparation-target-temperature", it)
                        }
                    }
                }
            }

            when (val setting = setting) {
                is HeatingZoneSetting -> with(setting) {
                    addField("heating-state", power?.value)
                    temperature?.apply {
                        addField("target-temperature", celsius)
                    }
                }
            }
        }

        fun PercentageDataPoint.measurement(measurement: String) = Point(measurement).apply {
            time(timestamp!!.toInstant().toEpochMilli(), WritePrecision.MS)
            addTag("home-id", home.id.toString())
            addTag("zone-id", id.toString())

            addField("value", percentage)
        }

        fun TemperatureDataPoint.measurement(measurement: String) = Point(measurement).apply {
            time(timestamp!!.toInstant().toEpochMilli(), WritePrecision.MS)
            addTag("home-id", home.id.toString())
            addTag("zone-id", id.toString())

            addField("value", celsius)
        }

        val observation = Observation.createNotStarted("convert-zone", observationRegistry)
            .lowCardinalityKeyValue("zone.id", id.toString())
            .lowCardinalityKeyValue("zone.name", name ?: "Unknown")
            .lowCardinalityKeyValue("zone.type", type?.value ?: "Unknown")
        return observation.observe(
            Supplier {
                val state = state()
                buildList {
                    state?.zoneMeasurement()?.let { add(it) }
                    state?.activityDataPoints?.heatingPower?.measurement("zone-heating-power")?.let { add(it) }
                    state?.sensorDataPoints?.apply {
                        insideTemperature?.measurement("zone-temperature")?.let { add(it) }
                        humidity?.measurement("zone-humidity")?.let { add(it) }
                    }
                }
            },
        ).orEmpty()
    }
}
