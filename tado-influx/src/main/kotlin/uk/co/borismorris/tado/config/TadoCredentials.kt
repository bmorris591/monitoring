package uk.co.borismorris.tado.config

import org.springframework.boot.context.properties.ConfigurationProperties
import uk.co.borismorris.monitoring.oauth2.ClientCredentials

@ConfigurationProperties(prefix = "tado.credentials")
class TadoCredentials : ClientCredentials {
    override lateinit var username: String
    override lateinit var password: String

    override fun toString() = "TadoCredentials(username='$username', password='XXXX')"
}
