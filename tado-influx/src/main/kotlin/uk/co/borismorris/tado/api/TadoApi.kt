@file:Suppress("UNCHECKED_CAST")

package uk.co.borismorris.tado.api

import com.tado.api.HomeApi
import com.tado.model.ControlDevice
import com.tado.model.GenericZoneCapabilities
import com.tado.model.HomeInfo
import com.tado.model.HomeState
import com.tado.model.MobileDevice
import com.tado.model.Overlay
import com.tado.model.OverlayTemplate
import com.tado.model.TadoSystemType
import com.tado.model.UserHomesInner
import com.tado.model.ZoneState
import org.springframework.http.ResponseEntity
import com.tado.model.User as TadoUser
import com.tado.model.Zone as TadoZone

class TadoApi(private val homeApi: HomeApi) {

    fun user() = (homeApi.showUserWithHttpInfo() as ResponseEntity<TadoUser?>).body?.asUser()

    private fun TadoUser.asUser() = User(this)

    @Suppress("UnsafeCallOnNullableType")
    inner class User(val user: TadoUser) {
        val name: String?
            get() = user.name

        val id: String
            get() = user.id!!

        val email: String?
            get() = user.email

        val username: String?
            get() = user.username

        val locale: String?
            get() = user.locale

        fun homes() = user.homes!!.map { it.asHome() }

        override fun toString() = "User(user=$user)"

        private fun UserHomesInner.asHome() = Home(this)

        inner class Home(val home: UserHomesInner) {
            val id: Long
                get() = home.id!!.toLong()

            val user: User
                get() = this@User

            fun info(): HomeInfo? = (homeApi.showHomeWithHttpInfo(id) as ResponseEntity<HomeInfo?>).body
            fun state(): HomeState? = (homeApi.homeStateWithHttpInfo(id) as ResponseEntity<HomeState?>).body

            fun devices(): List<MobileDevice> = (homeApi.listMobileDevicesWithHttpInfo(id) as ResponseEntity<List<MobileDevice>?>).body.orEmpty()

            fun zones(): Sequence<Zone> = (homeApi.listZonesWithHttpInfo(id) as ResponseEntity<List<TadoZone>?>).body?.asSequence().orEmpty()
                .map { it.asZone() }

            private fun TadoZone.asZone(): Zone = Zone(this)

            override fun toString() = "Home(home=$home)"

            inner class Zone(val zone: TadoZone) {
                val id: Long
                    get() = zone.id!!.toLong()

                val name: String?
                    get() = zone.name

                val type: TadoSystemType?
                    get() = zone.type

                val devices: List<ControlDevice>
                    get() = zone.devices!!

                val home: Home
                    get() = this@Home

                fun capabilities(): GenericZoneCapabilities? = (
                    homeApi.showZoneCapabilitiesWithHttpInfo(
                        this@Home.id,
                        this.id,
                    ) as ResponseEntity<GenericZoneCapabilities?>
                    ).body

                fun defaultOverlay(): OverlayTemplate? = (
                    homeApi.showZoneDefaultOverlayWithHttpInfo(
                        this@Home.id,
                        this.id,
                    ) as ResponseEntity<OverlayTemplate?>
                    ).body

                fun overlay(): Overlay? = (homeApi.showZoneOverlayWithHttpInfo(this@Home.id, this.id) as ResponseEntity<Overlay?>).body

                fun state(): ZoneState? = (homeApi.showZoneStateWithHttpInfo(this@Home.id, this.id) as ResponseEntity<ZoneState?>).body

                override fun toString() = "Zone(zone=$zone)"
            }
        }
    }
}
