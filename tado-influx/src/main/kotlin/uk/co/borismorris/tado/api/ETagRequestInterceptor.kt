package uk.co.borismorris.tado.api

import org.springframework.cache.Cache
import org.springframework.cache.get
import org.springframework.http.HttpRequest
import org.springframework.http.client.ClientHttpRequestExecution
import org.springframework.http.client.ClientHttpRequestInterceptor
import org.springframework.http.client.ClientHttpResponse

class ETagRequestInterceptor(private val cache: Cache) : ClientHttpRequestInterceptor {

    override fun intercept(
        request: HttpRequest,
        body: ByteArray,
        execution: ClientHttpRequestExecution,
    ): ClientHttpResponse {
        cache.get<String>(request.uri)?.also {
            request.headers.ifNoneMatch = listOf(it)
        }
        val response = execution.execute(request, body)
        response.headers.eTag?.let {
            if (cache.get<String>(request.uri) != it) {
                cache.put(request.uri, it)
            }
        }
        return response
    }
}
