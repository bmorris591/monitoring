package uk.co.borismorris.tado

import com.github.benmanes.caffeine.cache.Caffeine
import com.github.benmanes.caffeine.cache.Caffeine.from
import com.github.benmanes.caffeine.cache.Caffeine.newBuilder
import com.tado.api.HomeApi
import io.micrometer.observation.ObservationRegistry
import org.springframework.beans.factory.ListableBeanFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.cache.CacheProperties
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import org.springframework.cache.CacheManager
import org.springframework.cache.annotation.EnableCaching
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.task.TaskExecutor
import org.springframework.scheduling.TaskScheduler
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.oauth2.client.web.client.OAuth2ClientHttpRequestInterceptor
import org.springframework.web.client.RestClient
import uk.co.borismorris.monitoring.influxclient.InfluxClient
import uk.co.borismorris.monitoring.influxclient.InfluxDB2Properties
import uk.co.borismorris.monitoring.logging.KotlinLogging
import uk.co.borismorris.monitoring.oauth2.OauthCachePath
import uk.co.borismorris.monitoring.observability.logging.LogConfigOnStartup
import uk.co.borismorris.tado.api.ETagRequestInterceptor
import uk.co.borismorris.tado.api.TadoApi
import uk.co.borismorris.tado.config.TadoCredentials
import uk.co.borismorris.tado.worker.TadoDataGatherer
import java.nio.file.Path
import java.time.Instant

private val logger = KotlinLogging.logger {}

@SpringBootApplication(proxyBeanMethods = false)
@EnableConfigurationProperties(TadoCredentials::class)
@EnableCaching
@Suppress("TooManyFunctions")
class TadoInfluxApplication {
    companion object {
        init {
            SecurityContextHolder.setStrategyName(SecurityContextHolder.MODE_GLOBAL)
        }
    }

    @Bean
    fun caffeine(
        props: CacheProperties,
        taskScheduler: TaskScheduler,
        taskExecutor: TaskExecutor,
    ): Caffeine<Any, Any> = (props.caffeine?.spec?.let { from(it) } ?: newBuilder())
        .recordStats()
        .executor(taskExecutor)
        .scheduler { executor, runnable, delay, timeUnit ->
            taskScheduler.schedule(
                { executor.execute(runnable) },
                Instant.now().plusMillis(timeUnit.toMillis(delay)),
            )
        }.removalListener { key, value, cause ->
            logger.atInfo().setMessage("Removed etag")
                .addKeyValue("key", key)
                .addKeyValue("value", value)
                .addKeyValue("reason", cause)
                .log()
        }

    @Bean
    fun homeApi(
        restClientBuilder: RestClient.Builder,
        @Value("\${tado.base-url}") baseUrl: String,
        authFilter: OAuth2ClientHttpRequestInterceptor,
        cacheManager: CacheManager,
    ) = HomeApi(
        restClientBuilder
            .baseUrl(baseUrl)
            .requestInterceptor(authFilter)
            .requestInterceptor(ETagRequestInterceptor(cacheManager.getCache("etags")!!))
            .build(),
    )

    @Bean
    fun tadoApi(homeApi: HomeApi) = TadoApi(homeApi)

    @Bean
    @OauthCachePath
    fun cachePath(@Value("\${tado.cache-path}") path: Path) = path

    @Bean
    fun tadoDataGatherer(tadoApi: TadoApi, influxClient: InfluxClient, observationRegistry: ObservationRegistry) = TadoDataGatherer(tadoApi, influxClient, observationRegistry)

    @Bean
    fun logConfigOnStartup(context: ListableBeanFactory) = LogConfigOnStartup(context, TadoCredentials::class, InfluxDB2Properties::class)

    @Configuration(proxyBeanMethods = false)
    @ConditionalOnProperty(value = ["scheduling.enabled"], havingValue = "true", matchIfMissing = true)
    @EnableScheduling
    class Scheduling
}

@Suppress("SpreadOperator")
fun main(args: Array<String>) {
    runApplication<TadoInfluxApplication>(*args)
}
