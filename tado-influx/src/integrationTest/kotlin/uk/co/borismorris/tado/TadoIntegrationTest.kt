package uk.co.borismorris.tado

import com.influxdb.query.dsl.Flux
import org.assertj.core.api.Assertions.assertThat
import org.awaitility.Awaitility.await
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.testcontainers.service.connection.ServiceConnection
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.testcontainers.containers.InfluxDBContainer
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers
import org.testcontainers.utility.DockerImageName
import org.wiremock.spring.ConfigureWireMock
import org.wiremock.spring.DYNAMIC_PORT
import org.wiremock.spring.EnableWireMock
import org.wiremock.spring.PORT_DISABLED
import uk.co.borismorris.monitoring.influxclient.InfluxClient
import uk.co.borismorris.tado.worker.TadoDataGatherer
import java.nio.file.Files
import java.nio.file.Path
import java.time.Duration
import java.time.temporal.ChronoUnit
import kotlin.io.path.absolutePathString

@SpringBootTest(
    properties = [
        "tado.credentials.username=test-user",
        "tado.credentials.password=test-user-password-123",
        "tado.device-polling-interval=PT10S",
        "tado.base-url=\${wiremock.baseUrl}/api",
        "spring.security.oauth2.client.provider.tado.token-uri=\${wiremock.baseUrl}/token",
        "spring.http.client.ssl.bundle=wiremock",
        "test-certs.wiremock.enabled=true",
    ],
)
@EnableWireMock(
    ConfigureWireMock(
        port = PORT_DISABLED,
        httpsPort = DYNAMIC_PORT,
        sslBundle = "wiremock",
        filesUnderClasspath = ["uk/co/borismorris/tado"],
        templatingEnabled = true,
        globalTemplatingEnabled = true,
    ),
)
@ActiveProfiles("int-test")
@Testcontainers
class TadoIntegrationTest {

    companion object {
        val influxImage = DockerImageName.parse("influxdb")

        @Container
        @ServiceConnection
        @JvmStatic
        val influxdb = InfluxDBContainer(influxImage)
            .withUsername("admin")
            .withPassword("password")
            .withOrganization("default")
            .withBucket("tado")
            .withAdminToken("abc123")
            .withEnv("INFLUXD_LOG_LEVEL", "debug")

        @JvmStatic
        @TempDir
        lateinit var tempDir: Path

        @JvmStatic
        @DynamicPropertySource
        fun registerNetatmoProperties(registry: DynamicPropertyRegistry) {
            registry.add("tado.cache-path") {
                Files.createTempDirectory(tempDir, "token-refresh").resolve("oauth_client_registry.json")
                    .absolutePathString()
            }
        }
    }

    @Autowired
    lateinit var dataGather: TadoDataGatherer

    @Autowired
    lateinit var influxClient: InfluxClient

    @Test
    fun `I can start the application`() {
        val flux = Flux.from("tado").range(-10, ChronoUnit.YEARS).toString()
        await()
            .atMost(Duration.ofMinutes(5))
            .pollInterval(Duration.ofSeconds(5))
            .untilAsserted {
                val results = influxClient.query(flux).asSequence().flatMap { it.records }.toList()
                assertThat(results).hasSize(55)
            }
        val labels = """
            import "influxdata/influxdb/schema"
            schema.tagValues(bucket: "tado", tag: "name")
        """.trimIndent()
        val results = influxClient.query(labels).asSequence()
            .flatMap { it.records }
            .map { it.value.toString() }
            .toList()
        assertThat(results)
            .hasSize(10)
            .containsExactlyInAnyOrder(
                "Device One",
                "Device 2 no Location",
                "Disabled Device",
                "Zone 1",
                "Zone 2",
                "Zone 3",
                "Zone 4",
                "Zone 5",
                "Zone 7",
                "Zone 8",
            )
    }
}
