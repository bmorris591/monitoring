package uk.co.borismorris.tado.api

import com.github.tomakehurst.wiremock.client.WireMock.exactly
import com.github.tomakehurst.wiremock.client.WireMock.getAllScenarios
import com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor
import com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor
import com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo
import com.github.tomakehurst.wiremock.client.WireMock.verify
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.scheduling.TaskScheduler
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.oauth2.client.OAuth2AuthorizeRequest
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientManager
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.annotation.DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.springframework.test.context.bean.override.mockito.MockitoBean
import org.wiremock.spring.ConfigureWireMock
import org.wiremock.spring.DYNAMIC_PORT
import org.wiremock.spring.EnableWireMock
import org.wiremock.spring.PORT_DISABLED
import java.nio.file.Files
import java.nio.file.Path
import java.util.function.Consumer
import kotlin.io.path.absolutePathString

@SpringBootTest(
    properties = [
        "tado.credentials.username=test-user-short-token",
        "tado.credentials.password=test-user-password-123-short-token",
        "scheduling.enabled=false",
        "spring.http.client.ssl.bundle=wiremock",
        "tado.base-url=\${wiremock.baseUrl}/api",
        "spring.security.oauth2.client.provider.tado.token-uri=\${wiremock.baseUrl}/token",
        "test-certs.wiremock.enabled=true",
    ],
)
@EnableWireMock(
    ConfigureWireMock(
        filesUnderClasspath = ["uk/co/borismorris/tado/api/wiremock"],
        port = PORT_DISABLED,
        httpsPort = DYNAMIC_PORT,
        sslBundle = "wiremock",
    ),
)
@DirtiesContext(classMode = BEFORE_EACH_TEST_METHOD)
@ActiveProfiles("test")
internal class TokenRefreshTadoApiTest {
    companion object {
        @JvmStatic
        @TempDir
        lateinit var tempDir: Path

        @JvmStatic
        @DynamicPropertySource
        fun registerNetatmoProperties(registry: DynamicPropertyRegistry) {
            registry.add("tado.cache-path") {
                Files.createTempDirectory(tempDir, "token-refresh").resolve("oauth_client_registry.json")
                    .absolutePathString()
            }
        }
    }

    @MockitoBean
    lateinit var scheduler: TaskScheduler

    @Autowired
    lateinit var api: TadoApi

    @Autowired
    lateinit var manager: OAuth2AuthorizedClientManager

    @Test
    fun `When a token expires it is refreshed`() {
        // Get a token that's already expired
        val oAuth2AuthorizeRequest = OAuth2AuthorizeRequest
            .withClientRegistrationId("tado")
            .principal(SecurityContextHolder.getContext().authentication)
            .build()
        manager.authorize(oAuth2AuthorizeRequest)

        val user = api.user()!!
        assertThat(user.username).isEqualTo("username")

        verify(exactly(2), postRequestedFor(urlPathEqualTo("/token")))
        verify(exactly(1), getRequestedFor(urlPathEqualTo("/api/me")))

        assertThat(getAllScenarios()).singleElement().satisfies(
            Consumer { scenario ->
                assertThat(scenario.state).isEqualTo("Finished")
            },
        )
    }
}
