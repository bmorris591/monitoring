package uk.co.borismorris.tado

import org.junit.jupiter.api.Test
import org.springframework.modulith.core.ApplicationModules
import org.springframework.modulith.docs.Documenter

class TadoInfluxApplicationTest {
    @Test
    fun writeDocumentationSnippets() {
        var modules = ApplicationModules.of(TadoInfluxApplication::class.java).verify()
        Documenter(modules)
            .writeModulesAsPlantUml()
            .writeIndividualModulesAsPlantUml()
    }
}
