package org.wiremock.spring.internal

import com.github.tomakehurst.wiremock.WireMockServer
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.core.env.PropertySource
import org.springframework.test.util.ReflectionTestUtils

class WireMockPropertySource(context: ConfigurableApplicationContext) : PropertySource<Any>("wiremock") {

    val wiremockServers: WireMockServers by lazy { context.getBean(WireMockServers::class.java) }

    @Suppress("ReturnCount")
    override fun getProperty(name: String): Any? {
        if (!name.startsWith("wiremock.")) {
            return null
        }
        val nameParts = name.split(".", limit = 3)

        val wireMockServer = if (nameParts.size == 2) {
            wiremockServers.defaultServer()
        } else {
            wiremockServers[nameParts[1]]
        } ?: return null

        return ReflectionTestUtils.invokeMethod(wireMockServer, WireMockServer::class.java, nameParts.last())
    }
}
