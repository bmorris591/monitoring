package org.wiremock.spring.internal

import org.springframework.test.context.ContextConfigurationAttributes
import org.springframework.test.context.ContextCustomizer
import org.springframework.test.context.ContextCustomizerFactory
import org.springframework.test.context.TestContextAnnotationUtils
import org.wiremock.spring.ConfigureWireMock
import org.wiremock.spring.DEFAULT_CONFIGURE_WIREMOCK
import org.wiremock.spring.EnableWireMock

class WireMockContextCustomizerFactory : ContextCustomizerFactory {

    override fun createContextCustomizer(
        testClass: Class<*>,
        configAttributes: List<ContextConfigurationAttributes?>,
    ): ContextCustomizer? {
        // scan class and all enclosing classes if the test class is @Nested
        val annotations = parse(testClass)
        return if (annotations.isEmpty()) {
            null
        } else {
            annotations.validate()
            WireMockContextCustomizer(annotations)
        }
    }

    fun parse(clazz: Class<*>) = TestContextAnnotationUtils.findMergedAnnotation(clazz, EnableWireMock::class.java)?.let {
        getConfigureWireMocksOrDefault(it.value.asList())
    }.orEmpty()

    fun List<ConfigureWireMock>.validate() {
        sanityCheckDuplicateNames()
        sanityCheckHttpOrHttpsMustBeEnabled()
        sanityCheckHttpAndHttpsMustUseDifferentPorts()
        sanityCheckUniquePorts()
    }

    fun List<ConfigureWireMock>.sanityCheckDuplicateNames() {
        val duplicateNames = this
            .map { it.name }
            .duplicates()
        check(duplicateNames.isEmpty()) {
            "Names of mocks must be unique, found duplicates of: $duplicateNames"
        }
    }

    fun List<ConfigureWireMock>.sanityCheckHttpOrHttpsMustBeEnabled() {
        for (configureWireMock in this) {
            check(!(configureWireMock.port == -1 && configureWireMock.httpsPort == -1)) {
                "ConfigureWireMock ${configureWireMock.name} has both HTTP and HTTPS disabled. It is an invalid configuration."
            }
        }
    }

    fun List<ConfigureWireMock>.sanityCheckUniquePorts() {
        val duplicatePorts = this
            .flatMap { sequenceOf(it.port, it.httpsPort) }
            .filter { it > 0 }
            .duplicates()
        check(duplicatePorts.isEmpty()) {
            "Some statically configured ports are being used more than once: $duplicatePorts"
        }
    }

    private fun <T> List<T>.duplicates(): List<T> = groupingBy { it }.eachCount().filter { it.value > 1 }.map { it.key }

    fun List<ConfigureWireMock>.sanityCheckHttpAndHttpsMustUseDifferentPorts() {
        for (configureWireMock in this) {
            check(!(configureWireMock.port > 0 && configureWireMock.port == configureWireMock.httpsPort)) {
                "ConfigureWireMock ${configureWireMock.name}  uses same port  ${configureWireMock.port} for HTTP and HTTPS."
            }
        }
    }
}

fun getConfigureWireMocksOrDefault(configureWireMock: List<ConfigureWireMock>): List<ConfigureWireMock> {
    if (configureWireMock.isEmpty()) {
        return listOf(DEFAULT_CONFIGURE_WIREMOCK)
    }
    return configureWireMock
}
