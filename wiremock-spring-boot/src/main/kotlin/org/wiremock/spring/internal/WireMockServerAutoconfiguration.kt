package org.wiremock.spring.internal

import org.springframework.beans.factory.ObjectProvider
import org.springframework.boot.autoconfigure.AutoConfiguration
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean
import org.springframework.boot.ssl.SslBundles
import org.springframework.context.annotation.Bean

@AutoConfiguration
class WireMockServerAutoconfiguration {
    @Bean
    @ConditionalOnBean(WiremockAnnotationConfigurations::class)
    fun annotationWireMockServerRegistrar(
        configurations: WiremockAnnotationConfigurations,
        sslBundles: SslBundles,
    ) = AnnotationWireMockServerRegistrar(configurations, sslBundles)

    @Bean
    fun defaultWireMockServerRegistry(registrars: ObjectProvider<WireMockServerRegistrar>): DefaultWireMockServerRegistry {
        val registry = DefaultWireMockServerRegistry()
        registrars.forEach { it.registerServers(registry) }
        return registry
    }
}
