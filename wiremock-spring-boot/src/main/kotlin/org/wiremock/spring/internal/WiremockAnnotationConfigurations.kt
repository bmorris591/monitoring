package org.wiremock.spring.internal

import org.wiremock.spring.ConfigureWireMock

interface WiremockAnnotationConfigurations : Sequence<ConfigureWireMock>

class DefaultWiremockAnnotationConfigurations(val configurations: List<ConfigureWireMock>) :
    WiremockAnnotationConfigurations,
    Sequence<ConfigureWireMock> by configurations.asSequence()
