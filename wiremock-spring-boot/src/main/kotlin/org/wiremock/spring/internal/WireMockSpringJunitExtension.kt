package org.wiremock.spring.internal

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock
import org.junit.jupiter.api.extension.AfterEachCallback
import org.junit.jupiter.api.extension.BeforeEachCallback
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.api.extension.ParameterContext
import org.junit.jupiter.api.extension.ParameterResolver
import org.junit.platform.commons.support.AnnotationSupport.findAnnotatedFields
import org.springframework.core.annotation.AnnotationUtils.findAnnotation
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.wiremock.spring.EnableWireMock
import org.wiremock.spring.InjectWiremock

/**
 * JUnit 5 extension that sets [com.github.tomakehurst.wiremock.WireMockServer] instances
 * previously registered with [org.wiremock.spring.ConfigureWireMock] on test class fields.
 *
 * @author Maciej Walkowiak
 */
class WireMockSpringJunitExtension :
    BeforeEachCallback,
    AfterEachCallback,
    ParameterResolver {
    override fun beforeEach(extensionContext: ExtensionContext) {
        extensionContext.resetWireMockServersIfConfigured()
        extensionContext.injectWireMockInstances()
        extensionContext.configureWireMockForDefaultInstance()
    }

    private fun ExtensionContext.resetWireMockServersIfConfigured() {
        wiremockServerConfigurations()
            .filter { it.resetWireMockServer }
            .map { findWireMockServer(it.name) }
            .filterNotNull()
            .forEach { it.resetAll() }
    }

    private fun ExtensionContext.configureWireMockForDefaultInstance() {
        var wiremock: WireMockServer? = null
        for (enableWireMockAnnotation in enableWiremock()) {
            if (wiremock != null) {
                return
            }
            val wireMockServers = getConfigureWireMocksOrDefault(enableWireMockAnnotation.value.asList())
            if (wireMockServers.size > 1) {
                return
            }
            val wireMockName = wireMockServers.first().name
            wiremock = findWireMockServer(wireMockName)
        }
        if (wiremock != null) {
            val config = if (wiremock.isHttpsEnabled) {
                WireMock.create()
                    .https()
                    .port(wiremock.httpsPort())
            } else {
                WireMock.create()
                    .http()
                    .port(wiremock.port())
            }
            WireMock.configureFor(config.host("localhost").build())
        }
    }

    private fun ExtensionContext.enableWiremock() = requiredTestInstances.allInstances
        .asSequence()
        .map { findAnnotation(it.javaClass, EnableWireMock::class.java) }
        .filterNotNull()

    private fun ExtensionContext.wiremockServerConfigurations() = enableWiremock()
        .flatMap { getConfigureWireMocksOrDefault(it.value.asList()).asSequence() }

    override fun afterEach(context: ExtensionContext?) {
        WireMock.configureFor(-1)
    }

    override fun supportsParameter(parameterContext: ParameterContext, extensionContext: ExtensionContext?): Boolean = parameterContext.parameter.getType() == WireMockServer::class.java &&
        (
            parameterContext.isAnnotated(
                InjectWiremock::class.java,
            )
            )

    override fun resolveParameter(parameterContext: ParameterContext, extensionContext: ExtensionContext): Any? {
        val wireMockServerName = parameterContext.findAnnotation(InjectWiremock::class.java).get().value
        return extensionContext.findWireMockServer(wireMockServerName)
    }

    private fun ExtensionContext.injectWireMockInstances() {
        requiredTestInstances.allInstances.forEach { testInstance ->
            findAnnotatedFields(testInstance.javaClass, InjectWiremock::class.java).forEach { annotatedField ->
                val annotationValue = annotatedField.getAnnotation(InjectWiremock::class.java)
                annotatedField.setAccessible(true)
                val wiremock = findWireMockServer(annotationValue.value)
                annotatedField.set(testInstance, wiremock)
            }
        }
    }

    private fun ExtensionContext.findWireMockServer(name: String): WireMockServer? {
        val applicationContext = SpringExtension.getApplicationContext(this)
        return applicationContext.getBean(WireMockServers::class.java)[name]
    }
}
