package org.wiremock.spring.internal

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.common.ClasspathFileSource
import com.github.tomakehurst.wiremock.common.SingleRootFileSource
import com.github.tomakehurst.wiremock.core.WireMockConfiguration
import org.springframework.boot.ssl.SslBundles
import org.wiremock.spring.ConfigureWireMock
import org.wiremock.spring.httpEnabled
import org.wiremock.spring.httpsEnabled
import org.wiremock.spring.setupSslBundle
import java.nio.file.Path
import kotlin.io.path.exists

class AnnotationWireMockServerRegistrar(
    private val configurations: WiremockAnnotationConfigurations,
    private val sslBundles: SslBundles,
) : WireMockServerRegistrar {

    override fun registerServers(registry: WireMockServerRegistry) {
        configurations.forEach {
            registry[it.name] = createWireMockServer(sslBundles, it)
        }
    }

    @Suppress("SpreadOperator")
    fun createWireMockServer(
        sslBundles: SslBundles,
        options: ConfigureWireMock,
    ): WireMockServer {
        val serverOptions = if (options.httpsEnabled()) {
            setupSslBundle(options.sslBundle, sslBundles)
                .httpsPort(options.httpsPort)
        } else {
            WireMockConfiguration.options()
        }.templatingEnabled(options.templatingEnabled)
            .globalTemplating(options.globalTemplatingEnabled)
            .extensionFactories(*options.extensionFactories.map { it.java }.toTypedArray())
            .extensions(*options.extensions.map { it.java }.toTypedArray())

        if (options.httpEnabled()) {
            serverOptions.port(options.port)
        } else {
            serverOptions.httpDisabled(true)
        }

        sequenceOf<() -> Boolean>(
            { serverOptions.configureFilesUnderDirectory(options.filesUnderDirectory, options.name) },
            { serverOptions.configureFilesUnderDirectory(options.filesUnderDirectory) },
            { serverOptions.configureFilesUnderClasspath(options.filesUnderClasspath, options.name) },
            { serverOptions.configureFilesUnderClasspath(options.filesUnderClasspath) },
        )
            .first { it.invoke() }
        return WireMockServer(serverOptions)
    }

    private fun WireMockConfiguration.configureFilesUnderClasspath(
        filesUnderClasspath: Array<String>,
        vararg suffix: String,
    ): Boolean {
        val classloader = Thread.currentThread().contextClassLoader
        val alternatives = filesUnderClasspath.asSequence()
            .map { Path.of(it, *suffix) }
            .map { it.toString() }
            .filter { classloader.getResource(it) != null }
            .toList()
        check(alternatives.size <= 1) { "Found several filesUnderClasspath: $alternatives" }
        if (alternatives.size == 1) {
            fileSource(ClasspathFileSource(classloader, alternatives.first()))
            return true
        }
        return false
    }

    private fun WireMockConfiguration.configureFilesUnderDirectory(
        filesUnderDirectory: Array<String>,
        vararg suffix: String,
    ): Boolean {
        val alternatives = filesUnderDirectory.asSequence()
            .map { Path.of(it, *suffix) }
            .filter { it.exists() }
            .toList()
        check(alternatives.size <= 1) { "Found several filesUnderDirectory: $alternatives" }
        if (alternatives.size == 1) {
            fileSource(SingleRootFileSource(alternatives.first().toFile()))
            return true
        }
        return false
    }
}
