package org.wiremock.spring.internal

import com.github.tomakehurst.wiremock.WireMockServer
import jakarta.annotation.PreDestroy
import org.wiremock.spring.WIREMOCK
import java.util.concurrent.ConcurrentHashMap

interface WireMockServers : Sequence<String> {
    operator fun get(name: String): WireMockServer?

    fun defaultServer() = get(WIREMOCK)
}

interface WireMockServerRegistry {
    operator fun set(name: String, server: WireMockServer)
}

interface WireMockServerRegistrar {
    fun registerServers(registry: WireMockServerRegistry)
}

class DefaultWireMockServerRegistry :
    WireMockServers,
    WireMockServerRegistry {
    private val servers = ConcurrentHashMap<String, WireMockServer>()

    override fun get(name: String) = servers[name]?.apply { startIfNotRunning() }

    override fun set(name: String, server: WireMockServer) {
        servers[name] = server
    }

    @PreDestroy
    fun destroy() {
        servers.values.forEach { it.stop() }
    }

    private fun WireMockServer.startIfNotRunning() {
        if (!isRunning) start()
    }

    override fun iterator() = servers.keys.iterator()
}
