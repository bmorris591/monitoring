package org.wiremock.spring.internal

import org.springframework.context.ConfigurableApplicationContext
import org.springframework.test.context.ContextCustomizer
import org.springframework.test.context.MergedContextConfiguration
import org.wiremock.spring.ConfigureWireMock

data class WireMockContextCustomizer(private val configurations: List<ConfigureWireMock>) : ContextCustomizer {
    override fun customizeContext(
        context: ConfigurableApplicationContext,
        mergedConfig: MergedContextConfiguration,
    ) {
        context.beanFactory
            .registerSingleton(
                "wiremockAnnotationConfigurations",
                DefaultWiremockAnnotationConfigurations(configurations),
            )
        context.environment.propertySources.addFirst(WireMockPropertySource(context))
    }
}
