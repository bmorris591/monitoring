package org.wiremock.spring

import com.github.tomakehurst.wiremock.common.HttpsSettings
import com.github.tomakehurst.wiremock.common.ssl.KeyStoreSettings
import com.github.tomakehurst.wiremock.common.ssl.KeyStoreSource
import com.github.tomakehurst.wiremock.core.WireMockConfiguration
import org.springframework.boot.ssl.SslBundles
import org.springframework.boot.ssl.SslStoreBundle
import java.security.KeyStore

fun setupSslBundle(
    bundleName: String?,
    sslBundles: SslBundles,
): WireMockConfiguration = if (bundleName.isNullOrBlank()) {
    WireMockConfiguration.options()
} else {
    val bundle = sslBundles.getBundle(bundleName)
    WiremockSslOptions(bundle.stores)
}

class WiremockSslOptions(var testCertsProperties: SslStoreBundle) : WireMockConfiguration() {

    override fun httpsSettings(): HttpsSettings {
        val settings = super.httpsSettings()
        return WiremockHttpsSettings(
            testCertsProperties,
            settings.port(),
            settings.keyStorePassword(),
            settings.trustStorePassword(),
            settings.needClientAuth(),
        )
    }
}

class WiremockHttpsSettings(
    var testCertsProperties: SslStoreBundle,
    port: Int,
    keyStorePassword: String,
    trustStorePassword: String,
    needClientAuth: Boolean,
) : HttpsSettings(
    port,
    null,
    keyStorePassword,
    keyStorePassword,
    testCertsProperties.keyStore.type,
    null,
    trustStorePassword,
    testCertsProperties.trustStore.type,
    needClientAuth,
) {
    override fun keyStore(): KeyStoreSettings = KeyStoreSettings(
        InMemoryKeyStoreSource(
            testCertsProperties::getKeyStore,
            keyStoreType(),
            keyStorePassword(),
        ),
    )

    override fun trustStore(): KeyStoreSettings? = KeyStoreSettings(
        InMemoryKeyStoreSource(
            testCertsProperties::getTrustStore,
            trustStoreType(),
            trustStorePassword(),
        ),
    )
}

class InMemoryKeyStoreSource(
    val keyStore: () -> KeyStore,
    keyStoreType: String,
    keyStorePassword: String,
) : KeyStoreSource(keyStoreType, keyStorePassword.toCharArray()) {

    val loadedKeyStore: KeyStore by lazy(keyStore)

    override fun load() = loadedKeyStore

    override fun createInputStream() = throw UnsupportedOperationException()

    override fun save(item: KeyStore?) {}

    override fun exists() = true

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (javaClass != o?.javaClass) return false
        if (!super.equals(o)) return false

        o as InMemoryKeyStoreSource

        return keyStore == o.keyStore
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + keyStore.hashCode()
        return result
    }
}
