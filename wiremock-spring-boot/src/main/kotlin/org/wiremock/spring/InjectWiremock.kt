package org.wiremock.spring

import kotlin.annotation.AnnotationTarget.FIELD
import kotlin.annotation.AnnotationTarget.PROPERTY
import kotlin.annotation.AnnotationTarget.PROPERTY_SETTER
import kotlin.annotation.AnnotationTarget.VALUE_PARAMETER

@Retention(AnnotationRetention.RUNTIME)
@Target(FIELD, PROPERTY, VALUE_PARAMETER, PROPERTY_SETTER)
annotation class InjectWiremock(val value: String = "wiremock")
