package org.wiremock.spring

import com.github.tomakehurst.wiremock.core.Options
import com.github.tomakehurst.wiremock.extension.Extension
import com.github.tomakehurst.wiremock.extension.ExtensionFactory
import org.junit.jupiter.api.extension.ExtendWith
import org.wiremock.spring.internal.WireMockSpringJunitExtension
import kotlin.annotation.AnnotationTarget.CLASS
import kotlin.reflect.KClass

const val PORT_DISABLED = -1
const val DYNAMIC_PORT = Options.DYNAMIC_PORT

@Target(CLASS)
@Retention(AnnotationRetention.RUNTIME)
@ExtendWith(WireMockSpringJunitExtension::class)
annotation class EnableWireMock(
    vararg val value: ConfigureWireMock = [],
)

const val WIREMOCK = "wiremock"

@Suppress("LongParameterList")
@Retention(AnnotationRetention.RUNTIME)
annotation class ConfigureWireMock(
    val port: Int = DYNAMIC_PORT,
    val httpsPort: Int = PORT_DISABLED,
    val sslBundle: String = "",
    val name: String = WIREMOCK,
    val templatingEnabled: Boolean = false,
    val globalTemplatingEnabled: Boolean = false,
    val filesUnderClasspath: Array<String> = [],
    val filesUnderDirectory: Array<String> = ["wiremock", "stubs", "mappings"],
    val extensions: Array<KClass<Extension>> = [],
    val extensionFactories: Array<KClass<ExtensionFactory>> = [],
    val resetWireMockServer: Boolean = true,
)

fun ConfigureWireMock.httpEnabled() = port != PORT_DISABLED
fun ConfigureWireMock.httpsEnabled() = httpsPort != PORT_DISABLED

val DEFAULT_CONFIGURE_WIREMOCK = ConfigureWireMock()
