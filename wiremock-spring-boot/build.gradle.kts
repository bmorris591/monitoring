group = "uk.co.borismorris.wiremock.spring"

dependencies {
    api("org.junit.jupiter:junit-jupiter-api")
    api(libs.wiremock)
    implementation("org.springframework.boot:spring-boot-starter-test")
}
