plugins {
    monitoring.`application-conventions`
    monitoring.`jib-conventions`
}

group = "uk.co.borismorris.speedtest"

dependencies {
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.modulith:spring-modulith-api")
    implementation(projects.restclientInfluxClient)

    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.springframework.modulith:spring-modulith-starter-test")
    testImplementation(projects.wiremockSpringBoot)

    integrationTestImplementation(projects.restclientInfluxClientTestcontainers)
    integrationTestImplementation(libs.influx.flux)
}

application {
    mainClass.set("uk.co.borismorris.speedtest.SpeedtestKt")
}

val dockerRepo: Provider<String> = project.providers.gradleProperty("dockerRepo")

jib {
    from {
        setImage(
            dockerRepo
                .map { r -> "$r/speedtest-container:${project.version}-java" }
                .orElse("speedtest-container:latest-java"),
        )
    }
}
