package uk.co.borismorris.speedtest

import com.fasterxml.jackson.databind.ObjectMapper
import io.micrometer.observation.ObservationRegistry
import org.springframework.beans.factory.ListableBeanFactory
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.http.client.ClientHttpRequestFactoryBuilder
import org.springframework.boot.http.client.ClientHttpRequestFactorySettings
import org.springframework.boot.runApplication
import org.springframework.boot.web.client.RestClientCustomizer
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.scheduling.annotation.EnableScheduling
import uk.co.borismorris.monitoring.influxclient.InfluxClient
import uk.co.borismorris.monitoring.influxclient.InfluxDB2Properties
import uk.co.borismorris.monitoring.observability.logging.LogConfigOnStartup
import uk.co.borismorris.speedtest.cli.OoklaSpeedtest
import uk.co.borismorris.speedtest.cli.SpeedTester
import uk.co.borismorris.speedtest.conf.MetricCollectionConfig
import uk.co.borismorris.speedtest.conf.SpeedTestConfig
import uk.co.borismorris.speedtest.worker.SpeedTestMetricGatherer
import java.time.Duration

@SpringBootApplication(proxyBeanMethods = false)
@EnableConfigurationProperties(SpeedTestConfig::class, MetricCollectionConfig::class)
class Speedtest {

    @Bean
    fun restClientCustomizer() = RestClientCustomizer {
        it.requestFactory(
            ClientHttpRequestFactoryBuilder.simple()
                .build(
                    ClientHttpRequestFactorySettings.defaults()
                        .withConnectTimeout(Duration.ofMinutes(5))
                        .withReadTimeout(Duration.ofMinutes(5)),
                ),
        )
    }

    @Bean
    fun ooklaSpeedTester(speedTestConfig: SpeedTestConfig, objectMapper: ObjectMapper) = OoklaSpeedtest(objectMapper, speedTestConfig)

    @Bean
    fun metricGatherer(observationRegistry: ObservationRegistry, speedTester: SpeedTester, influxClient: InfluxClient) = SpeedTestMetricGatherer(observationRegistry, speedTester, influxClient)

    @Bean
    fun logConfigOnStartup(context: ListableBeanFactory) = LogConfigOnStartup(context, SpeedTestConfig::class, InfluxDB2Properties::class, MetricCollectionConfig::class)

    @Configuration(proxyBeanMethods = false)
    @Profile("!test")
    @EnableScheduling
    class Scheduling
}

@Suppress("SpreadOperator")
fun main(args: Array<String>) {
    runApplication<Speedtest>(*args)
}
