package uk.co.borismorris.speedtest.cli

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.ObjectMapper
import io.micrometer.observation.annotation.Observed
import uk.co.borismorris.monitoring.logging.KotlinLogging
import uk.co.borismorris.speedtest.conf.SpeedTestConfig
import java.math.BigDecimal
import java.time.Instant

val args = listOf("--accept-license", "--accept-gdpr", "-f", "json")
private val logger = KotlinLogging.logger {}

open class OoklaSpeedtest(private val mapper: ObjectMapper, private val speedTestConfig: SpeedTestConfig) : SpeedTester {

    @Observed
    override fun testInternetSpeed(): OoklaInternetSpeed {
        val command = speedTestConfig.speedTestExecutable + args
        logger.atInfo().setMessage("Running command").addKeyValue("command", command).log()
        return ProcessBuilder(command)
            .redirectError(ProcessBuilder.Redirect.INHERIT)
            .start()
            .inputStream.reader().use {
                val input = it.readText()
                logger.atInfo().setMessage("Speedtest result").addKeyValue("result", input).log()
                mapper.readValue(input, OoklaInternetSpeed::class.java)
            }
    }
}

data class OoklaInternetSpeed(
    override val type: String,
    override val timestamp: Instant,
    override val ping: OoklaPing,
    override val download: OoklaTransferSpeed,
    override val upload: OoklaTransferSpeed,
    override val isp: String,
    @JsonProperty("interface") override val iface: OoklaTestInterface,
    override val server: OoklaTestServer,
    override val result: OoklaResult,
) : InternetSpeed

data class OoklaPing(
    override val jitter: BigDecimal,
    override val latency: BigDecimal,
) : Ping

data class OoklaTransferSpeed(
    override val bytes: BigDecimal,
    override val elapsed: BigDecimal,
    override val bandwidth: BigDecimal,
) : TransferSpeed

data class OoklaTestInterface(
    override val internalIp: String,
    override val externalIp: String,
    override val macAddr: String,
    override val name: String,
    override val isVpn: Boolean = false,
) : TestInterface

data class OoklaTestServer(
    override val id: Long = Long.MIN_VALUE,
    override val name: String,
    override val location: String,
    override val country: String,
    override val host: String,
    override val port: Int = 0,
    override val ip: String,
) : TestServer

data class OoklaResult(override val id: String, override val url: String) : TestResultLink
