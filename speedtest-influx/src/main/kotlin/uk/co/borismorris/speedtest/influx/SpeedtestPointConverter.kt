package uk.co.borismorris.speedtest.influx

import com.influxdb.client.domain.WritePrecision
import com.influxdb.client.write.Point.measurement
import uk.co.borismorris.speedtest.cli.InternetSpeed
import java.math.BigDecimal

object SpeedtestPointConverter {
    private val BITS_IN_BYTE = BigDecimal("8")

    fun InternetSpeed.toPoint() = measurement("speed_test_results")
        .addTag("server", server.ip)
        .addTag("server_country", server.country)
        .addTag("server_name", server.name)
        .addField("download", download.bandwidth.multiply(BITS_IN_BYTE))
        .addField("download_bytes", download.bytes)
        .addField("download_elapsed", download.elapsed)
        .addField("ping", ping.latency)
        .addField("jitter", ping.jitter)
        .addField("upload", upload.bandwidth.multiply(BITS_IN_BYTE))
        .addField("upload_bytes", upload.bytes)
        .addField("upload_elapsed", upload.elapsed)
        .time(timestamp.toEpochMilli(), WritePrecision.MS)
}
