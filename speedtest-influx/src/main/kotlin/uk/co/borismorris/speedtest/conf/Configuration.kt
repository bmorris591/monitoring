package uk.co.borismorris.speedtest.conf

import org.springframework.boot.context.properties.ConfigurationProperties
import java.time.Duration

@ConfigurationProperties(prefix = "speedtest")
data class SpeedTestConfig(val speedTestExecutable: List<String>)

@ConfigurationProperties(prefix = "metric")
data class MetricCollectionConfig(val period: Duration)
