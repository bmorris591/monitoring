package uk.co.borismorris.speedtest.native

import com.influxdb.query.dsl.Flux
import org.assertj.core.api.Assertions.assertThat
import org.awaitility.Awaitility.await
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.testcontainers.service.connection.ServiceConnection
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.testcontainers.containers.InfluxDBContainer
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers
import org.testcontainers.utility.DockerImageName
import uk.co.borismorris.monitoring.influxclient.InfluxClient
import java.time.Duration
import java.time.temporal.ChronoUnit

private const val SPEEDTESTS = "speedtests"

@SpringBootTest
@ActiveProfiles("int-test")
@Testcontainers
internal class SpeedtestIntegrationTest {
    companion object {
        val influxImage = DockerImageName.parse("influxdb")

        @Container
        @ServiceConnection
        @JvmStatic
        val influxdb = InfluxDBContainer(influxImage)
            .withBucket(SPEEDTESTS)
            .withAdminToken("abc123")
            .withEnv("INFLUXD_LOG_LEVEL", "debug")

        @JvmStatic
        @DynamicPropertySource
        fun influxProperties(registry: DynamicPropertyRegistry) {
            registry.add("speedtest.speed-test-executable") {
                if (System.getProperty("os.name").lowercase().contains("win")) {
                    "cmd,/C,src\\\\test\\\\resources\\\\mock-speedtest"
                } else {
                    "src/test/resources/mock-speedtest"
                }
            }
        }
    }

    @Autowired
    lateinit var influxClient: InfluxClient

    @Test
    fun `Speedtest results are submitted to the database`() {
        val flux = Flux.from(SPEEDTESTS).range(-10, ChronoUnit.YEARS).toString()
        await()
            .atMost(Duration.ofMinutes(5))
            .pollInterval(Duration.ofSeconds(5))
            .untilAsserted {
                val tables = influxClient.query(flux)
                println(tables)
                val results = tables.asSequence().flatMap { it.records }.toList()
                assertThat(results).hasSize(8)
            }
    }
}
