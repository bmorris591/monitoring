package uk.co.borismorris.speedtest

import org.junit.jupiter.api.Test
import org.springframework.modulith.core.ApplicationModules
import org.springframework.modulith.docs.Documenter

class SpeedtestTest {

    @Test
    fun writeDocumentationSnippets() {
        var modules = ApplicationModules.of(Speedtest::class.java).verify()
        Documenter(modules)
            .writeModulesAsPlantUml()
            .writeIndividualModulesAsPlantUml()
    }
}
