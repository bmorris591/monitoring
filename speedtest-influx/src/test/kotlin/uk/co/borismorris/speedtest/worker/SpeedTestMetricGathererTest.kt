package uk.co.borismorris.speedtest.worker

import com.github.tomakehurst.wiremock.client.WireMock.exactly
import com.github.tomakehurst.wiremock.client.WireMock.findUnmatchedRequests
import com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor
import com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo
import com.github.tomakehurst.wiremock.client.WireMock.verify
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.wiremock.spring.ConfigureWireMock
import org.wiremock.spring.EnableWireMock

@SpringBootTest(
    properties = [
        "influx.url=\${wiremock.baseUrl}",
    ],
)
@EnableWireMock(
    ConfigureWireMock(
        filesUnderClasspath = ["uk/co/borismorris/speedtest/worker/wiremock"],
    ),
)
@ActiveProfiles("test")
internal class SpeedTestMetricGathererTest {

    companion object {
        @JvmStatic
        @DynamicPropertySource
        fun influxProperties(registry: DynamicPropertyRegistry) {
            registry.add("speedtest.speed-test-executable") {
                if (System.getProperty("os.name").lowercase().contains("win")) {
                    "cmd,/C,src\\\\test\\\\resources\\\\mock-speedtest"
                } else {
                    "src/test/resources/mock-speedtest"
                }
            }
        }
    }

    @Autowired
    lateinit var metricGatherer: SpeedTestMetricGatherer

    @Test
    fun `Speedtest results are submitted to the database`() {
        metricGatherer.executeSpeedTest()

        verify(exactly(1), postRequestedFor(urlPathEqualTo("/api/v2/write")))

        val unmatched: List<*> = findUnmatchedRequests()
        assertThat(unmatched).isEmpty()
    }
}
