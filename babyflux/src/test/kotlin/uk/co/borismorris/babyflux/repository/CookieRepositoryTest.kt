package uk.co.borismorris.babyflux.repository

import org.apache.hc.client5.http.cookie.CookieStore
import org.apache.hc.client5.http.impl.cookie.BasicClientCookie
import org.assertj.core.api.Assertions.assertThat
import org.instancio.Instancio
import org.instancio.InstancioApi
import org.instancio.Select.field
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.transaction.annotation.Transactional
import org.testcontainers.jdbc.ContainerDatabaseDriver
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.time.temporal.ChronoUnit.MINUTES

@SpringBootTest(
    properties = [
        "spring.datasource.url=jdbc:tc:mariadb:latest:///babyflux",
        "baby.user-info.email=email@baby.invalid",
        "baby.user-info.password=password1234",
        "baby.url=http://url.invalid",
    ],
)
@ActiveProfiles("test")
@Transactional
class CookieRepositoryTest {

    companion object {
        @JvmStatic
        @BeforeAll
        fun setup() {
            ContainerDatabaseDriver.killContainers()
        }
    }

    @Autowired
    lateinit var cookieStore: CookieStore

    @Test
    fun `I can store a cookie`() {
        val cookie = cookieFactory()
            .supply(field("cookieExpiryDate")) { random ->
                Instant.now().plus(random.intRange(5, 10).toLong(), MINUTES)
            }
            .set(field("creationDate"), Instant.now())
            .create()
        cookieStore.addCookie(cookie)

        assertThat(cookieStore.cookies).singleElement()
            .usingRecursiveComparison()
            .withComparatorForFields(String.CASE_INSENSITIVE_ORDER, "cookieDomain")
            .withEqualsForType(
                { l, r -> l.truncatedTo(ChronoUnit.MILLIS) == r.truncatedTo(ChronoUnit.MILLIS) },
                Instant::class.java,
            )
            .isEqualTo(cookie)
    }

    @Test
    fun `I can replace a cookie`() {
        val name = "cookie_name"
        val domain = "cookie.invalid"
        val path = "/cookie/path"

        val cookies = cookieFactory()
            .set(field("name"), name)
            .set(field("cookieDomain"), domain)
            .set(field("cookiePath"), path)
            .supply(field("cookieExpiryDate")) { random ->
                Instant.now().plus(random.intRange(5, 10).toLong(), MINUTES)
            }
            .supply(field("creationDate")) { _ -> Instant.now() }
            .stream()
            .limit(10)
            .toList()

        cookies.forEach {
            cookieStore.addCookie(it)
        }

        assertThat(cookieStore.cookies).singleElement()
            .usingRecursiveComparison()
            .withComparatorForFields(String.CASE_INSENSITIVE_ORDER, "cookieDomain")
            .withEqualsForType(
                { l, r -> l.truncatedTo(ChronoUnit.MILLIS) == r.truncatedTo(ChronoUnit.MILLIS) },
                Instant::class.java,
            )
            .isEqualTo(cookies.last())
    }

    @Test
    fun `An expired cookie is not stored`() {
        val cookie = cookieFactory()
            .set(field("cookieExpiryDate"), Instant.EPOCH)
            .set(field("creationDate"), Instant.now())
            .create()
        cookieStore.addCookie(cookie)

        assertThat(cookieStore.cookies).isEmpty()
    }

    @Test
    fun `An expired cookie is cleared`() {
        val cookieExpiryDate = Instant.now().plus(10, MINUTES)
        val cookie = cookieFactory()
            .set(field("cookieExpiryDate"), cookieExpiryDate)
            .set(field("creationDate"), Instant.now())
            .create()
        cookieStore.addCookie(cookie)

        assertThat(cookieStore.clearExpired(cookieExpiryDate.plusSeconds(1))).isTrue()

        assertThat(cookieStore.cookies).isEmpty()
    }

    @Test
    fun `Cookies are cleared`() {
        cookieFactory()
            .supply(field("cookieExpiryDate")) { random ->
                Instant.now().plus(random.intRange(5, 10).toLong(), MINUTES)
            }
            .set(field("creationDate"), Instant.now())
            .stream()
            .limit(10)
            .forEach { cookieStore.addCookie(it) }

        cookieStore.clear()

        assertThat(cookieStore.cookies).isEmpty()
    }

    private fun cookieFactory(): InstancioApi<BasicClientCookie> = Instancio.of(BasicClientCookie::class.java)
        .generate(field("attribs")) { gen ->
            gen.map<String, String>()
                .withKeys(*SUPPORTED_ATTRIBUTES.toTypedArray())
        }
}
