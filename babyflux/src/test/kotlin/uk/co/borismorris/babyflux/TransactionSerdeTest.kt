package uk.co.borismorris.babyflux

import com.fasterxml.jackson.core.JsonToken
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatCode
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration
import org.springframework.boot.test.context.SpringBootTest
import uk.co.borismorris.babyflux.domain.TransactionRecord
import java.net.URL

private const val FILES = "uk/co/borismorris/babyflux/wiremock/__files"

@SpringBootTest(classes = [JacksonAutoConfiguration::class])
@TestInstance(PER_CLASS)
class TransactionSerdeTest {

    @Autowired
    lateinit var mapper: ObjectMapper

    fun exampleFiles(): List<Arguments> {
        val classLoader = Thread.currentThread().contextClassLoader
        classLoader.getResourceAsStream(FILES).bufferedReader().use {
            return it.lineSequence()
                .map { classLoader.getResource("$FILES/$it") }
                .map { Arguments.of(it) }
                .toList()
        }
    }

    @ParameterizedTest
    @MethodSource("exampleFiles")
    fun `Round trip transactions`(json: URL) {
        val count = transactionSequence(json)
            .onEach {
                assertThatCode {
                    mapper.writerWithDefaultPrettyPrinter().writeValueAsString(it)
                }.doesNotThrowAnyException()
            }
            .count()
        assertThat(count).isGreaterThan(0)
    }

    private fun transactionSequence(json: URL): Sequence<TransactionRecord> {
        val parser = mapper.factory.createParser(json)
        check(parser.nextToken() == JsonToken.START_ARRAY)
        return sequence {
            while (parser.nextToken() == JsonToken.START_OBJECT) {
                yield(mapper.readValue(parser))
            }
        }
    }
}
