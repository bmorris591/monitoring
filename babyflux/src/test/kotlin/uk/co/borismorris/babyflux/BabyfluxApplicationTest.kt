package uk.co.borismorris.babyflux

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.iterable.ThrowingExtractor
import org.jooq.DSLContext
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.modulith.core.ApplicationModules
import org.springframework.modulith.docs.Documenter
import org.springframework.test.context.ActiveProfiles
import org.wiremock.spring.ConfigureWireMock
import org.wiremock.spring.EnableWireMock
import uk.co.borismorris.babyflux.client.BabyTrackerClient
import uk.co.borismorris.babyflux.generated.database.tables.references.DEVICE
import uk.co.borismorris.babyflux.worker.CompletedSyncInfo
import uk.co.borismorris.babyflux.worker.SyncInfo
import uk.co.borismorris.babyflux.worker.TransactionSync

@SpringBootTest(
    properties = [
        "spring.datasource.url=jdbc:tc:mariadb:latest:///babyflux",
        "baby.user-info.email=email@baby.invalid",
        "baby.user-info.password=password1234",
        "baby.url=\${wiremock.baseUrl}",
    ],
)
@EnableWireMock(
    ConfigureWireMock(
        filesUnderClasspath = ["uk/co/borismorris/babyflux/wiremock"],
        templatingEnabled = true,
        globalTemplatingEnabled = true,
    ),
)
@ActiveProfiles("test", "jooq-logging")
class BabyfluxApplicationTest {
    @Autowired
    lateinit var client: BabyTrackerClient

    @Autowired
    lateinit var transactionSync: TransactionSync

    @Autowired
    lateinit var dslContext: DSLContext

    @Test
    fun `Application starts normally`() {
        client.login()

        val syncs = transactionSync.syncTransactions()
        assertThat(syncs).satisfiesExactlyInAnyOrder(
            { it.assertCompletedSyncInto("23bf9488-2108-434c-945f-e2f9a79f1542", 685) },
            { it.assertCompletedSyncInto("2880A139-45FC-4D59-A2F3-2313E54DCF82", 180) },
            { it.assertCompletedSyncInto("3D50CE2E-7441-4691-B908-55A4C2EFDDA7", 1319) },
            { it.assertCompletedSyncInto("8321B843-AEA4-47D3-AD52-4A2FF3060D46", 164) },
            { it.assertCompletedSyncInto("E3E88A5A-E245-42D9-9EF6-62BBBB090A9F", 4) },
        )

        val devices = dslContext.fetch(DEVICE)
        assertThat(devices)
            .hasSize(5)
            .extracting(ThrowingExtractor { it.id to it.sync })
            .containsExactlyInAnyOrder(
                "23bf9488-2108-434c-945f-e2f9a79f1542" to 685,
                "2880A139-45FC-4D59-A2F3-2313E54DCF82" to 180,
                "3D50CE2E-7441-4691-B908-55A4C2EFDDA7" to 1319,
                "8321B843-AEA4-47D3-AD52-4A2FF3060D46" to 164,
                "E3E88A5A-E245-42D9-9EF6-62BBBB090A9F" to 4,
            )
    }

    private fun SyncInfo.assertCompletedSyncInto(deviceId: String, sync: Int) {
        assertThat(this).isInstanceOfSatisfying(CompletedSyncInfo::class.java) {
            assertThat(it.deviceSyncInfo.device.id).isEqualTo(deviceId)
            assertThat(it.deviceSyncInfo.syncId).isEqualTo(sync)
            assertThat(it.currentSync).isEqualTo(sync)
        }
    }

    @Nested
    inner class ModulithTest {

        @Test
        fun writeDocumentationSnippets() {
            var modules = ApplicationModules.of(BabyfluxApplication::class.java).verify()
            Documenter(modules)
                .writeModulesAsPlantUml()
                .writeIndividualModulesAsPlantUml()
        }
    }
}
