package uk.co.borismorris.babyflux.client

import org.apache.hc.client5.http.cookie.CookieStore
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.web.client.HttpClientErrorException.Unauthorized
import org.wiremock.spring.ConfigureWireMock
import org.wiremock.spring.EnableWireMock
import java.util.function.Consumer

@SpringBootTest(
    properties = [
        "spring.datasource.url=jdbc:tc:mariadb:latest:///babyflux",
        "baby.user-info.email=email@baby.invalid",
        "baby.user-info.password=password1234",
        "baby.url=\${wiremock.baseUrl}",
    ],
)
@EnableWireMock(
    ConfigureWireMock(
        filesUnderClasspath = ["uk/co/borismorris/babyflux/client/wiremock"],
        templatingEnabled = true,
        globalTemplatingEnabled = true,
    ),
)
@ActiveProfiles("test")
internal class RestClientBabyTrackerClientTest {
    @Autowired
    lateinit var client: BabyTrackerClient

    @Autowired
    lateinit var cookieStore: CookieStore

    @BeforeEach
    fun cleanupCookies() {
        cookieStore.clear()
    }

    @Test
    fun `When I log in then account info is returned`() {
        val accountInfo = client.login()
        assertThat(accountInfo.accountId).isEqualTo(12345678L)

        assertThat(cookieStore.cookies).anySatisfy { cookie ->
            assertThat(cookie.name).isEqualTo("PHPSESSID")
            assertThat(cookie.domain).isEqualTo("localhost")
            assertThat(cookie.path).isEqualTo("/")
            assertThat(cookie.value).isEqualTo("a1s2d3f4g5h6j7k8l9")
        }
    }

    @Test
    fun `When unauthenticated then an error is returned`() {
        assertThatThrownBy { client.getDeviceList() }
            .isInstanceOf(Unauthorized::class.java)
    }

    @Test
    fun `When I log in then cookies are forwarded`() {
        client.login()
        val devices = client.getDeviceList().toList()
        assertThat(devices).singleElement().satisfies(
            Consumer {
                assertThat(it.device.id).isEqualTo("23bf9488-2108-434c-945f-e2f9a79f1542")
                assertThat(it.device.name).isEqualTo("Unknown")
                assertThat(it.device.osInfo).isEqualTo("Pixel 2 XL OS 28")
                assertThat(it.syncId).isEqualTo(100)
            },
        )
    }
}
