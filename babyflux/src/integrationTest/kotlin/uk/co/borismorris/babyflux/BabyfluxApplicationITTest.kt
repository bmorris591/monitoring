package uk.co.borismorris.babyflux

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.iterable.ThrowingExtractor
import org.awaitility.Awaitility.await
import org.jooq.DSLContext
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.wiremock.spring.ConfigureWireMock
import org.wiremock.spring.DYNAMIC_PORT
import org.wiremock.spring.EnableWireMock
import org.wiremock.spring.PORT_DISABLED
import uk.co.borismorris.babyflux.generated.database.tables.references.DEVICE
import java.time.Duration

@SpringBootTest(
    properties = [
        "spring.datasource.url=jdbc:tc:mariadb:latest:///babyflux",
        "baby.user-info.email=email@baby.invalid",
        "baby.user-info.password=password1234",
        "baby.url=\${wiremock.baseUrl}",
        "spring.http.client.ssl.bundle=wiremock",
        "test-certs.wiremock.enabled=true",
    ],
)
@EnableWireMock(
    ConfigureWireMock(
        port = PORT_DISABLED,
        httpsPort = DYNAMIC_PORT,
        sslBundle = "wiremock",
        filesUnderClasspath = ["uk/co/borismorris/babyflux/wiremock"],
        templatingEnabled = true,
        globalTemplatingEnabled = true,
    ),
)
@ActiveProfiles("jooq-logging", "int-test")
class BabyfluxApplicationITTest {
    @Autowired
    lateinit var dslContext: DSLContext

    @Test
    fun `The application polls for transactions`() {
        await().atMost(Duration.ofMinutes(5)).pollInterval(Duration.ofSeconds(10)).untilAsserted {
            val devices = dslContext.fetch(DEVICE)
            assertThat(devices)
                .hasSize(5)
                .extracting(ThrowingExtractor { it.id to it.sync })
                .containsExactlyInAnyOrder(
                    "23bf9488-2108-434c-945f-e2f9a79f1542" to 685,
                    "2880A139-45FC-4D59-A2F3-2313E54DCF82" to 180,
                    "3D50CE2E-7441-4691-B908-55A4C2EFDDA7" to 1319,
                    "8321B843-AEA4-47D3-AD52-4A2FF3060D46" to 164,
                    "E3E88A5A-E245-42D9-9EF6-62BBBB090A9F" to 4,
                )
        }
    }
}
