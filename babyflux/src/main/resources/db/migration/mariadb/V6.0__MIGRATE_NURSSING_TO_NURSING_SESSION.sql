INSERT INTO `nursing_session`(`id`,
                              `timestamp`,
                              `note`,
                              `picture_loaded`,
                              `time`,
                              `baby_id`,
                              `both_duration`,
                              `finish_side`,
                              `left_duration`,
                              `right_duration`,
                              `detailed`)
SELECT `id`,
       `timestamp`,
       `note`,
       `picture_loaded`,
       `time`,
       `baby_id`,
       `both_duration`,
       `finish_side`,
       `left_duration`,
       `right_duration`,
       false
FROM nursing;
DROP TABLE nursing;
