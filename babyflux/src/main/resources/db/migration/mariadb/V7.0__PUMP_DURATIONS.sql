ALTER TABLE pump
    ADD COLUMN left_duration  bigint(20) DEFAULT NULL AFTER finish_side,
    ADD COLUMN right_duration bigint(20) DEFAULT NULL AFTER left_duration;
