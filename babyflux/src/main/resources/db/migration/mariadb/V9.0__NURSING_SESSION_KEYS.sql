ALTER TABLE `nursing_session_left_timestamps`
    DROP CONSTRAINT `FKlag15jw9ny263ty9gv8wt23ct`;
ALTER TABLE `nursing_session_left_timestamps`
    ADD CONSTRAINT `FKlag15jw9ny263ty9gv8wt23ct` FOREIGN KEY (`nursing_session_id`) REFERENCES `nursing_session` (`id`) ON DELETE CASCADE;
ALTER TABLE `nursing_session_right_timestamps`
    DROP CONSTRAINT `FKn3f0dccjl4o57vjabq114djsn`;
ALTER TABLE `nursing_session_right_timestamps`
    ADD CONSTRAINT `FKn3f0dccjl4o57vjabq114djsn` FOREIGN KEY (`nursing_session_id`) REFERENCES `nursing_session` (`id`) ON DELETE CASCADE;
