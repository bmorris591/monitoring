CREATE VIEW `weight_kg_male` AS
SELECT `age_months`,
       `M` * POWER(1 + `L` * `S` * -1.881, 1 / `L`) AS 3_percent,
       `M` * POWER(1 + `L` * `S` * -1.645, 1 / `L`) AS 5_percent,
       `M` * POWER(1 + `L` * `S` * -1.282, 1 / `L`) AS 10_percent,
       `M` * POWER(1 + `L` * `S` * -0.674, 1 / `L`) AS 25_percent,
       `M` * POWER(1 + `L` * `S` * 0, 1 / `L`)      AS 50_percent,
       `M` * POWER(1 + `L` * `S` * 0.674, 1 / `L`)  AS 75_percent,
       `M` * POWER(1 + `L` * `S` * 1.036, 1 / `L`)  AS 85_percent,
       `M` * POWER(1 + `L` * `S` * 1.282, 1 / `L`)  AS 90_percent,
       `M` * POWER(1 + `L` * `S` * 1.645, 1 / `L`)  AS 95_percent,
       `M` * POWER(1 + `L` * `S` * 1.881, 1 / `L`)  AS 97_percent
FROM `us_cdc_growth`
WHERE `sex` = "male"
  AND `measurement` = "weight_kg";

CREATE VIEW `head_circumference_cm_male` AS
SELECT `age_months`,
       `M` * POWER(1 + `L` * `S` * -1.881, 1 / `L`) AS 3_percent,
       `M` * POWER(1 + `L` * `S` * -1.645, 1 / `L`) AS 5_percent,
       `M` * POWER(1 + `L` * `S` * -1.282, 1 / `L`) AS 10_percent,
       `M` * POWER(1 + `L` * `S` * -0.674, 1 / `L`) AS 25_percent,
       `M` * POWER(1 + `L` * `S` * 0, 1 / `L`)      AS 50_percent,
       `M` * POWER(1 + `L` * `S` * 0.674, 1 / `L`)  AS 75_percent,
       `M` * POWER(1 + `L` * `S` * 1.036, 1 / `L`)  AS 85_percent,
       `M` * POWER(1 + `L` * `S` * 1.282, 1 / `L`)  AS 90_percent,
       `M` * POWER(1 + `L` * `S` * 1.645, 1 / `L`)  AS 95_percent,
       `M` * POWER(1 + `L` * `S` * 1.881, 1 / `L`)  AS 97_percent
FROM `us_cdc_growth`
WHERE `sex` = "male"
  AND `measurement` = "head_circumference_cm";

CREATE VIEW `length_cm_male` AS
SELECT `age_months`,
       `M` * POWER(1 + `L` * `S` * -1.881, 1 / `L`) AS 3_percent,
       `M` * POWER(1 + `L` * `S` * -1.645, 1 / `L`) AS 5_percent,
       `M` * POWER(1 + `L` * `S` * -1.282, 1 / `L`) AS 10_percent,
       `M` * POWER(1 + `L` * `S` * -0.674, 1 / `L`) AS 25_percent,
       `M` * POWER(1 + `L` * `S` * 0, 1 / `L`)      AS 50_percent,
       `M` * POWER(1 + `L` * `S` * 0.674, 1 / `L`)  AS 75_percent,
       `M` * POWER(1 + `L` * `S` * 1.036, 1 / `L`)  AS 85_percent,
       `M` * POWER(1 + `L` * `S` * 1.282, 1 / `L`)  AS 90_percent,
       `M` * POWER(1 + `L` * `S` * 1.645, 1 / `L`)  AS 95_percent,
       `M` * POWER(1 + `L` * `S` * 1.881, 1 / `L`)  AS 97_percent
FROM `us_cdc_growth`
WHERE `sex` = "male"
  AND `measurement` = "length_cm";