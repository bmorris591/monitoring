CREATE TABLE `nursing_session_timestamps`
(
    `nursing_session_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
    `start_item`         bit(1)                                  NOT NULL,
    `time`               datetime(6) DEFAULT NULL,
    `side`               enum ('LEFT', 'RIGHT'),
    KEY `nursing_session_id` (`nursing_session_id`),
    CONSTRAINT `nursing_session_id` FOREIGN KEY (`nursing_session_id`) REFERENCES `nursing_session` (`id`) ON DELETE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

INSERT INTO `nursing_session_timestamps`(`nursing_session_id`, `start_item`, `time`, `side`)
SELECT `nursing_session_id`, `start_item`, `time`, 'LEFT'
FROM `nursing_session_left_timestamps`;

INSERT INTO `nursing_session_timestamps`(`nursing_session_id`, `start_item`, `time`, `side`)
SELECT `nursing_session_id`, `start_item`, `time`, 'RIGHT'
FROM `nursing_session_right_timestamps`;

DROP TABLE `nursing_session_left_timestamps`;
DROP TABLE `nursing_session_right_timestamps`;

CREATE VIEW `nursing_session_left_timestamps` AS
SELECT `nursing_session_id`, `start_item`, `time`
FROM nursing_session_timestamps
WHERE side = 'LEFT';

CREATE VIEW `nursing_session_right_timestamps` AS
SELECT `nursing_session_id`, `start_item`, `time`
FROM nursing_session_timestamps
WHERE side = 'RIGHT'
