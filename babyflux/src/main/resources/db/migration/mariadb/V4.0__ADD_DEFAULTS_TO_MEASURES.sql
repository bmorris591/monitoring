ALTER TABLE `pump`
  ALTER `english_measure` SET DEFAULT 0;
ALTER TABLE `pump`
  ALTER `value` SET DEFAULT 0;

ALTER TABLE `pumped`
  ALTER `english_measure` SET DEFAULT 0;
ALTER TABLE `pumped`
  ALTER `value` SET DEFAULT 0;

ALTER TABLE `temperature`
  ALTER `english_measure` SET DEFAULT 0;
ALTER TABLE `temperature`
  ALTER `value` SET DEFAULT 0;

ALTER TABLE `formula`
  ALTER `english_measure` SET DEFAULT 0;
ALTER TABLE `formula`
  ALTER `value` SET DEFAULT 0;

ALTER TABLE `growth`
  ALTER `head_english_measure` SET DEFAULT 0;
ALTER TABLE `growth`
  ALTER `head` SET DEFAULT 0;
ALTER TABLE `growth`
  ALTER `length_english_measure` SET DEFAULT 0;
ALTER TABLE `growth`
  ALTER `length` SET DEFAULT 0;
ALTER TABLE `growth`
  ALTER `weight_english_measure` SET DEFAULT 0;
ALTER TABLE `growth`
  ALTER `weight` SET DEFAULT 0;

ALTER TABLE `other_feed`
  ALTER `english_measure` SET DEFAULT 0;
ALTER TABLE `other_feed`
  ALTER `value` SET DEFAULT 0;