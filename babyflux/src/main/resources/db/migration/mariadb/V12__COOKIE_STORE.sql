CREATE TABLE `cookie`
(
    `id`            INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
    `name`          VARCHAR(255)     NOT NULL,
    `value`         text             NOT NULL,
    `domain`        VARCHAR(255)     NOT NULL DEFAULT '',
    `expiry_date`   DATETIME(6)               DEFAULT NULL,
    `path`          VARCHAR(255)     NOT NULL DEFAULT '/',
    `secure`        BIT(1)           NOT NULL DEFAULT 0,
    `creation_date` DATETIME(6)      NOT NULL,
    `http_only`     BIT(1)           NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`),
    UNIQUE INDEX (`name`, `domain`, `path`),
    INDEX cookie_expiry_date (`expiry_date`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

CREATE TABLE `cookie_attributes`
(
    `cookie_id` INTEGER UNSIGNED NOT NULL,
    `name`      VARCHAR(255)     NOT NULL,
    `value`     text             NOT NULL,
    PRIMARY KEY (`cookie_id`, `name`),
    CONSTRAINT `cookie_attributes_cookie_id` FOREIGN KEY (`cookie_id`) REFERENCES `cookie` (`id`) ON DELETE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
