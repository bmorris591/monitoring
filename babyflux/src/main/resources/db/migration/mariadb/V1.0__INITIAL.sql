/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES UTF8mb4 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activity_picture_note`
(
  `activity_id`     varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture_note_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`activity_id`, `picture_note_id`),
  UNIQUE KEY `UK_hq087av82ksuiq80toi8oigxj` (`picture_note_id`),
  CONSTRAINT `FK8ndy112is8y9l3rfpixjfkk6a` FOREIGN KEY (`picture_note_id`) REFERENCES `picture` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `baby`
(
  `id`           varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `timestamp`    datetime(6)                             DEFAULT NULL,
  `dob`          datetime(6)                             DEFAULT NULL,
  `due_day`      datetime(6)                             DEFAULT NULL,
  `gender`       varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name`         varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bath`
(
  `id`             varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `timestamp`      datetime(6)                             DEFAULT NULL,
  `note`           varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture_loaded` bit(1)                                  NOT NULL,
  `time`           datetime(6)                             DEFAULT NULL,
  `baby_id`        varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_jk1mjm19kast1rar8sh37cth1` (`baby_id`),
  CONSTRAINT `FK_jk1mjm19kast1rar8sh37cth1` FOREIGN KEY (`baby_id`) REFERENCES `baby` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conflict_record`
(
  `id`        varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `timestamp` datetime(6)                             DEFAULT NULL,
  `loser_id`  varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `winner_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device`
(
  `id`      varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name`    varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `os_info` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sync`    int(11)                                 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `diaper`
(
  `id`             varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `timestamp`      datetime(6)                             DEFAULT NULL,
  `note`           varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture_loaded` bit(1)                                  NOT NULL,
  `time`           datetime(6)                             DEFAULT NULL,
  `baby_id`        varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount`         varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status`         varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_59svpm7ktje1tbmam90ry8uh9` (`baby_id`),
  CONSTRAINT `FK_59svpm7ktje1tbmam90ry8uh9` FOREIGN KEY (`baby_id`) REFERENCES `baby` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formula`
(
  `id`              varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `timestamp`       datetime(6)                             DEFAULT NULL,
  `note`            varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture_loaded`  bit(1)                                  NOT NULL,
  `time`            datetime(6)                             DEFAULT NULL,
  `baby_id`         varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `english_measure` bit(1)                                  NOT NULL,
  `value`           double                                  NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_oo4jadxwl9e58db8o7bx320kl` (`baby_id`),
  CONSTRAINT `FK_oo4jadxwl9e58db8o7bx320kl` FOREIGN KEY (`baby_id`) REFERENCES `baby` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `growth`
(
  `id`                     varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `timestamp`              datetime(6)                             DEFAULT NULL,
  `note`                   varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture_loaded`         bit(1)                                  NOT NULL,
  `time`                   datetime(6)                             DEFAULT NULL,
  `baby_id`                varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted`                bit(1)                                  NOT NULL,
  `head_english_measure`   bit(1)                                  DEFAULT NULL,
  `head`                   double                                  DEFAULT NULL,
  `length_english_measure` bit(1)                                  DEFAULT NULL,
  `length`                 double                                  DEFAULT NULL,
  `weight_english_measure` bit(1)                                  DEFAULT NULL,
  `weight`                 double                                  DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_cptibrqqhn80p8nl6dxwtwpxx` (`baby_id`),
  CONSTRAINT `FK_cptibrqqhn80p8nl6dxwtwpxx` FOREIGN KEY (`baby_id`) REFERENCES `baby` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `journal`
(
  `id`             varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `timestamp`      datetime(6)                             DEFAULT NULL,
  `note`           varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture_loaded` bit(1)                                  NOT NULL,
  `time`           datetime(6)                             DEFAULT NULL,
  `baby_id`        varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_9aihc41k38awowsdocjd88bas` (`baby_id`),
  CONSTRAINT `FK_9aihc41k38awowsdocjd88bas` FOREIGN KEY (`baby_id`) REFERENCES `baby` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `joy`
(
  `id`             varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `timestamp`      datetime(6)                             DEFAULT NULL,
  `note`           varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture_loaded` bit(1)                                  NOT NULL,
  `time`           datetime(6)                             DEFAULT NULL,
  `baby_id`        varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_cj9bktsh70d4cdgsj809dr45d` (`baby_id`),
  CONSTRAINT `FK_cj9bktsh70d4cdgsj809dr45d` FOREIGN KEY (`baby_id`) REFERENCES `baby` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medication`
(
  `id`                      varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `timestamp`               datetime(6)                             DEFAULT NULL,
  `note`                    varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture_loaded`          bit(1)                                  NOT NULL,
  `time`                    datetime(6)                             DEFAULT NULL,
  `baby_id`                 varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount`                  double                                  NOT NULL,
  `medication_selection_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK5wbx0tgdx0l3taj3mbfbnq0sq` (`medication_selection_id`),
  KEY `FK_5c25ux62gi2aaogn0hxer3qse` (`baby_id`),
  CONSTRAINT `FK5wbx0tgdx0l3taj3mbfbnq0sq` FOREIGN KEY (`medication_selection_id`) REFERENCES `medication_selection` (`id`),
  CONSTRAINT `FK_5c25ux62gi2aaogn0hxer3qse` FOREIGN KEY (`baby_id`) REFERENCES `baby` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medication_selection`
(
  `id`              varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `timestamp`       datetime(6)                             DEFAULT NULL,
  `description`     varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name`            varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount_per_time` double                                  NOT NULL,
  `periodicity`     bigint(20)                              DEFAULT NULL,
  `prescription`    bit(1)                                  NOT NULL,
  `unit`            varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `milestone`
(
  `id`                varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `timestamp`         datetime(6)                             DEFAULT NULL,
  `note`              varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture_loaded`    bit(1)                                  NOT NULL,
  `time`              datetime(6)                             DEFAULT NULL,
  `baby_id`           varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `has_new_type`      bit(1)                                  NOT NULL,
  `milestone_type_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKa9tuj0vjloa00cgdqhigdtjn4` (`milestone_type_id`),
  KEY `FK_j1jj62v3n4npi75cqah1cdm2a` (`baby_id`),
  CONSTRAINT `FK_j1jj62v3n4npi75cqah1cdm2a` FOREIGN KEY (`baby_id`) REFERENCES `baby` (`id`),
  CONSTRAINT `FKa9tuj0vjloa00cgdqhigdtjn4` FOREIGN KEY (`milestone_type_id`) REFERENCES `milestone_selection` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `milestone_selection`
(
  `id`          varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `timestamp`   datetime(6)                             DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name`        varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `for_age`     int(11)                                 NOT NULL,
  `used`        bit(1)                                  NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nursing`
(
  `id`             varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `timestamp`      datetime(6)                             DEFAULT NULL,
  `note`           varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture_loaded` bit(1)                                  NOT NULL,
  `time`           datetime(6)                             DEFAULT NULL,
  `baby_id`        varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `both_duration`  bigint(20)                              DEFAULT NULL,
  `finish_side`    varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `left_duration`  bigint(20)                              DEFAULT NULL,
  `right_duration` bigint(20)                              DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_f8qo4cvvx1693lyput22ubpb4` (`baby_id`),
  CONSTRAINT `FK_f8qo4cvvx1693lyput22ubpb4` FOREIGN KEY (`baby_id`) REFERENCES `baby` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nursing_session`
(
  `id`                 varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `timestamp`          datetime(6)                             DEFAULT NULL,
  `note`               varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture_loaded`     bit(1)                                  NOT NULL,
  `time`               datetime(6)                             DEFAULT NULL,
  `baby_id`            varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `both_duration`      bigint(20)                              DEFAULT NULL,
  `finish_side`        varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `left_duration`      bigint(20)                              DEFAULT NULL,
  `right_duration`     bigint(20)                              DEFAULT NULL,
  `detailed`           bit(1)                                  NOT NULL,
  `pair_session_state` int(11)                                 DEFAULT NULL,
  `state`              varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_6wnsv6fqpbk3jley4af2vsgp8` (`baby_id`),
  CONSTRAINT `FK_6wnsv6fqpbk3jley4af2vsgp8` FOREIGN KEY (`baby_id`) REFERENCES `baby` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nursing_session_left_timestamps`
(
  `nursing_session_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_item`         bit(1)                                  NOT NULL,
  `time`               datetime(6) DEFAULT NULL,
  KEY `FKlag15jw9ny263ty9gv8wt23ct` (`nursing_session_id`),
  CONSTRAINT `FKlag15jw9ny263ty9gv8wt23ct` FOREIGN KEY (`nursing_session_id`) REFERENCES `nursing_session` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nursing_session_right_timestamps`
(
  `nursing_session_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_item`         bit(1)                                  NOT NULL,
  `time`               datetime(6) DEFAULT NULL,
  KEY `FKn3f0dccjl4o57vjabq114djsn` (`nursing_session_id`),
  CONSTRAINT `FKn3f0dccjl4o57vjabq114djsn` FOREIGN KEY (`nursing_session_id`) REFERENCES `nursing_session` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `other_activity`
(
  `id`             varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `timestamp`      datetime(6)                             DEFAULT NULL,
  `note`           varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture_loaded` bit(1)                                  NOT NULL,
  `time`           datetime(6)                             DEFAULT NULL,
  `baby_id`        varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `duration`       bigint(20)                              DEFAULT NULL,
  `desc_id`        varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK1aw0oopojxup9b34s1ppntevv` (`desc_id`),
  KEY `FK_gtqfbonlvei06rev1jyc5wah2` (`baby_id`),
  CONSTRAINT `FK1aw0oopojxup9b34s1ppntevv` FOREIGN KEY (`desc_id`) REFERENCES `other_activity_description` (`id`),
  CONSTRAINT `FK_gtqfbonlvei06rev1jyc5wah2` FOREIGN KEY (`baby_id`) REFERENCES `baby` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `other_activity_description`
(
  `id`          varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `timestamp`   datetime(6)                             DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name`        varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `other_feed`
(
  `id`              varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `timestamp`       datetime(6)                             DEFAULT NULL,
  `note`            varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture_loaded`  bit(1)                                  NOT NULL,
  `time`            datetime(6)                             DEFAULT NULL,
  `baby_id`         varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `english_measure` bit(1)                                  NOT NULL,
  `value`           double                                  NOT NULL,
  `unit`            varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `feed_type_id`    varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK6e5wlyxjtyr4ei55ho3fda6ub` (`feed_type_id`),
  KEY `FK_f7xkhbitox5ujxs2j1h77m62h` (`baby_id`),
  CONSTRAINT `FK6e5wlyxjtyr4ei55ho3fda6ub` FOREIGN KEY (`feed_type_id`) REFERENCES `other_feed_selection` (`id`),
  CONSTRAINT `FK_f7xkhbitox5ujxs2j1h77m62h` FOREIGN KEY (`baby_id`) REFERENCES `baby` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `other_feed_selection`
(
  `id`          varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `timestamp`   datetime(6)                             DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name`        varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bottle`      bit(1)                                  NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `picture`
(
  `id`          varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `timestamp`   datetime(6)                             DEFAULT NULL,
  `activity_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attached`    bit(1)                                  NOT NULL,
  `deleted`     bit(1)                                  NOT NULL,
  `file_name`   varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `media_type`  varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pump`
(
  `id`              varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `timestamp`       datetime(6)                             DEFAULT NULL,
  `note`            varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture_loaded`  bit(1)                                  NOT NULL,
  `time`            datetime(6)                             DEFAULT NULL,
  `english_measure` bit(1)                                  NOT NULL,
  `value`           double                                  NOT NULL,
  `finish_side`     varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `label`           varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sides`           varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pumped`
(
  `id`              varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `timestamp`       datetime(6)                             DEFAULT NULL,
  `note`            varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture_loaded`  bit(1)                                  NOT NULL,
  `time`            datetime(6)                             DEFAULT NULL,
  `baby_id`         varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `english_measure` bit(1)                                  NOT NULL,
  `value`           double                                  NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_2l1ntgdid93hq8p44xt7f456t` (`baby_id`),
  CONSTRAINT `FK_2l1ntgdid93hq8p44xt7f456t` FOREIGN KEY (`baby_id`) REFERENCES `baby` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sleep`
(
  `id`             varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `timestamp`      datetime(6)                             DEFAULT NULL,
  `note`           varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture_loaded` bit(1)                                  NOT NULL,
  `time`           datetime(6)                             DEFAULT NULL,
  `baby_id`        varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `duration`       bigint(20)                              DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_7y2ihch9bj4s9qlnndlag6hh1` (`baby_id`),
  CONSTRAINT `FK_7y2ihch9bj4s9qlnndlag6hh1` FOREIGN KEY (`baby_id`) REFERENCES `baby` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `temperature`
(
  `id`              varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `timestamp`       datetime(6)                             DEFAULT NULL,
  `note`            varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture_loaded`  bit(1)                                  NOT NULL,
  `time`            datetime(6)                             DEFAULT NULL,
  `baby_id`         varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `english_measure` bit(1)                                  NOT NULL,
  `value`           double                                  NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_tc5ysvuawgvwomsfei9857vul` (`baby_id`),
  CONSTRAINT `FK_tc5ysvuawgvwomsfei9857vul` FOREIGN KEY (`baby_id`) REFERENCES `baby` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vaccine`
(
  `id`              varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `timestamp`       datetime(6)                             DEFAULT NULL,
  `note`            varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture_loaded`  bit(1)                                  NOT NULL,
  `time`            datetime(6)                             DEFAULT NULL,
  `baby_id`         varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vaccine_type_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKmbyi3i0sua413445fvjesxewy` (`vaccine_type_id`),
  KEY `FK_d1dlhkc1dn35wxwypcs3eql0g` (`baby_id`),
  CONSTRAINT `FK_d1dlhkc1dn35wxwypcs3eql0g` FOREIGN KEY (`baby_id`) REFERENCES `baby` (`id`),
  CONSTRAINT `FKmbyi3i0sua413445fvjesxewy` FOREIGN KEY (`vaccine_type_id`) REFERENCES `vaccine_selection` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vaccine_selection`
(
  `id`          varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `timestamp`   datetime(6)                             DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name`        varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `for_age`     int(11)                                 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

