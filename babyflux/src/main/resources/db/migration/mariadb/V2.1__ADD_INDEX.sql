--
-- Time indices for range queries
--

CREATE INDEX bath_time_idx ON bath (`time`);
CREATE INDEX diaper_time_idx ON diaper (`time`);
CREATE INDEX formula_time_idx ON formula (`time`);
CREATE INDEX growth_time_idx ON growth (`time`);
CREATE INDEX journal_time_idx ON journal (`time`);
CREATE INDEX joy_time_idx ON joy (`time`);
CREATE INDEX medication_time_idx ON medication (`time`);
CREATE INDEX milestone_time_idx ON milestone (`time`);
CREATE INDEX nursing_time_idx ON nursing (`time`);
CREATE INDEX nursing_session_time_idx ON nursing_session (`time`);
CREATE INDEX other_activity_time_idx ON other_activity (`time`);
CREATE INDEX other_feed_time_idx ON other_feed (`time`);
CREATE INDEX pump_time_idx ON pump (`time`);
CREATE INDEX pumped_time_idx ON pumped (`time`);
CREATE INDEX sleep_time_idx ON sleep (`time`);
CREATE INDEX temperature_time_idx ON temperature (`time`);
CREATE INDEX vaccine_time_idx ON vaccine (`time`);

--
-- Status index for type queries
--

CREATE INDEX diaper_status_idx ON diaper (`status`);