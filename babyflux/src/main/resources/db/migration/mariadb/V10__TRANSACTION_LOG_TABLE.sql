CREATE TABLE `transaction_log`
(
    `device_id`   varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
    `sync`        int(11)                                 NOT NULL,
    `op_code`     varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
    `transaction` json NOT NULL,
    PRIMARY KEY (`device_id`, `sync`),
    INDEX (`op_code`),
    CONSTRAINT `FK_transaction_log_device_id` FOREIGN KEY (`device_id`) REFERENCES `device` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
