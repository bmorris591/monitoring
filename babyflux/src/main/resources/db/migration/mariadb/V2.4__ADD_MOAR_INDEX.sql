--
-- Side indices for pump
--

CREATE INDEX pump_finish_side_idx ON pump (`finish_side`);
CREATE INDEX pump_sides_idx ON pump (`sides`);