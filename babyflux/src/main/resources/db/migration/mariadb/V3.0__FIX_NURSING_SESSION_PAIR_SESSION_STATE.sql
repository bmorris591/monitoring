--
-- Add new column
--

ALTER TABLE nursing_session
  ADD COLUMN pair_session_state_tmp varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL AFTER pair_session_state;

--
-- Set data in new column
--

UPDATE nursing_session
SET pair_session_state_tmp=CASE
                             WHEN pair_session_state = 0 THEN "START"
                             WHEN pair_session_state = 1 THEN "LEFT_RUNNING"
                             WHEN pair_session_state = 2 THEN "RIGHT_RUNNING"
                             WHEN pair_session_state = 3 THEN "LEFT_PAUSED"
                             WHEN pair_session_state = 4 THEN "RIGHT_PAUSED"
                             WHEN pair_session_state = 5 THEN "BOTH_STOPPED"
                             WHEN pair_session_state = 6 THEN "END"
                             ELSE NULL
  END;

--
-- Drop column
--

ALTER TABLE nursing_session
  DROP COLUMN pair_session_state;

--
-- Rename new column
--

ALTER TABLE nursing_session
  CHANGE pair_session_state_tmp pair_session_state varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL;