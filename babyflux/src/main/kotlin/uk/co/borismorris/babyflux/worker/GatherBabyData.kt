package uk.co.borismorris.babyflux.worker

import org.springframework.scheduling.annotation.Scheduled
import uk.co.borismorris.babyflux.client.BabyTrackerClient
import uk.co.borismorris.monitoring.logging.KotlinLogging
import java.util.concurrent.atomic.AtomicBoolean

private val logger = KotlinLogging.logger {}

class GatherBabyData(
    private val babyTrackerClient: BabyTrackerClient,
    private val txSync: TransactionSync,
) {

    private val reauthenticate = AtomicBoolean(true)

    @Scheduled(initialDelay = 0, fixedRateString = "\${baby.polling-interval}")
    fun pollBabyData() {
        if (reauthenticate.get()) {
            val accountInfo = babyTrackerClient.login()
            logger.atInfo().setMessage("Logged in")
                .addKeyValue("accountInfo", accountInfo)
                .log()
            reauthenticate.set(false)
        }
        kotlin.runCatching {
            txSync.syncTransactions()
        }.onSuccess {
            logger.atInfo().setMessage("Finished sync run")
                .addKeyValue("syncInfos", it)
                .log()
        }.onFailure {
            reauthenticate.set(true)
            throw it
        }
    }
}
