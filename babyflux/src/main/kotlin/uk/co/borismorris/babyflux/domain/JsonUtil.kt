package uk.co.borismorris.babyflux.domain

import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DatabindContext
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JavaType
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.json.JsonMapper
import com.fasterxml.jackson.databind.jsontype.impl.TypeIdResolverBase
import com.fasterxml.jackson.databind.util.StdConverter
import com.fasterxml.jackson.module.kotlin.readValue
import uk.co.borismorris.monitoring.logging.KotlinLogging
import java.time.Duration
import java.util.*
import kotlin.text.Charsets.UTF_8

private val logger = KotlinLogging.logger {}
private val bCObjectPackage = BCObject::class.java.packageName

class GsonTypeIdResolver : TypeIdResolverBase() {
    override fun idFromValueAndType(value: Any?, suggestedType: Class<*>?) = TODO("not implemented")

    override fun getMechanism() = JsonTypeInfo.Id.CUSTOM

    override fun idFromValue(value: Any?) = value?.javaClass?.simpleName

    override fun typeFromId(context: DatabindContext, id: String): JavaType {
        val simpleName = when (id) {
            "Nursing" -> "NursingSession"
            else -> id
        }
        return context.typeFactory.constructFromCanonical("$bCObjectPackage.$simpleName")
    }
}

class MinuteDurationDeserialiser : StdDeserializer<Duration>(Duration::class.java) {
    override fun deserialize(p: JsonParser, ctxt: DeserializationContext): Duration = Duration.ofMinutes(_parseLongPrimitive(p, ctxt))
}

class BcObjectConverter(val mapper: ObjectMapper) : StdConverter<String, BCObject<*>>() {
    constructor() : this(JsonMapper.builder().build())

    companion object {
        private val decoder = Base64.getDecoder()
    }

    override fun convert(value: String?): BCObject<*> {
        val json = String(decoder.decode(value), UTF_8)
        logger.atInfo().setMessage("Transaction JSON").addKeyValue("json", json).log()
        return mapper.readValue<BCObject<*>>(json)
    }
}
