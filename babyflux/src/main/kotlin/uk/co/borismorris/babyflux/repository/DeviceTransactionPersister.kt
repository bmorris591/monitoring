package uk.co.borismorris.babyflux.repository

import com.fasterxml.jackson.databind.ObjectMapper
import io.micrometer.observation.Observation
import io.micrometer.observation.ObservationRegistry
import org.jooq.DSLContext
import org.jooq.TableRecord
import org.springframework.transaction.annotation.Transactional
import uk.co.borismorris.babyflux.client.DeviceInfo
import uk.co.borismorris.babyflux.domain.NursingSession
import uk.co.borismorris.babyflux.domain.NursingSessionTimestamp
import uk.co.borismorris.babyflux.domain.TransactionLogOpCode.CONFLICT
import uk.co.borismorris.babyflux.domain.TransactionLogOpCode.DELETE
import uk.co.borismorris.babyflux.domain.TransactionLogOpCode.INSERT
import uk.co.borismorris.babyflux.domain.TransactionLogOpCode.RELIVE
import uk.co.borismorris.babyflux.domain.TransactionLogOpCode.UPDATE
import uk.co.borismorris.babyflux.domain.TransactionRecord
import uk.co.borismorris.babyflux.generated.database.enums.NursingSessionTimestampsSide
import uk.co.borismorris.babyflux.generated.database.tables.records.DeviceRecord
import uk.co.borismorris.babyflux.generated.database.tables.references.DEVICE
import uk.co.borismorris.babyflux.generated.database.tables.references.NURSING_SESSION_TIMESTAMPS
import uk.co.borismorris.babyflux.generated.database.tables.references.TRANSACTION_LOG
import uk.co.borismorris.monitoring.logging.KotlinLogging
import java.time.ZoneOffset.UTC

private val logger = KotlinLogging.logger {}

interface DeviceTransactionPersister {
    fun processTransaction(device: DeviceInfo, transaction: TransactionRecord)
}

open class RepositoryDeviceTransactionPersister(
    private val observationRegistry: ObservationRegistry,
    private val dslContext: DSLContext,
    private val mapper: ObjectMapper,
) : DeviceTransactionPersister {

    @Transactional
    override fun processTransaction(device: DeviceInfo, transaction: TransactionRecord) {
        logger.atTrace().setMessage("Processing transaction")
            .addKeyValue("oPCode") { transaction.oPCode }
            .addKeyValue("transaction") { transaction.transaction }
            .log()
        Observation.createNotStarted("process-transaction", observationRegistry)
            .lowCardinalityKeyValue("transaction.opcode", transaction.oPCode.toString())
            .highCardinalityKeyValue("transaction.sync", transaction.sync.toString())
            .highCardinalityKeyValue("transaction.transaction", transaction.transaction.toString())
            .observe {
                val deviceRecord = device.getOrCreateDevice()
                dslContext.insertInto(
                    TRANSACTION_LOG,
                    TRANSACTION_LOG.DEVICE_ID,
                    TRANSACTION_LOG.SYNC,
                    TRANSACTION_LOG.OP_CODE,
                    TRANSACTION_LOG.TRANSACTION,
                )
                    .values(
                        deviceRecord.id,
                        transaction.sync,
                        transaction.oPCode.name,
                        mapper.writeValueAsString(transaction.transaction),
                    )
                    .onDuplicateKeyIgnore()
                    .execute()
                when (transaction.oPCode) {
                    INSERT, UPDATE, RELIVE, CONFLICT -> save(deviceRecord, transaction)
                    DELETE -> delete(deviceRecord, transaction)
                }
            }
    }

    fun delete(device: DeviceRecord, transaction: TransactionRecord) {
        val delete = transaction.transaction
        val updated = delete.toRecord(dslContext).delete()
        if (updated == 0) {
            logger.atWarn().addKeyValue("affectedRows", updated).addKeyValue("transaction", transaction)
                .log("Delete affected no rows")
        } else {
            logger.atDebug().addKeyValue("affectedRows", updated).addKeyValue("transaction", transaction)
                .log("Deleted record")
        }
        saveOrUpdateDevice(device, transaction.sync)
    }

    fun save(device: DeviceRecord, transaction: TransactionRecord) {
        val save = transaction.transaction
        var updated = save.toRecord(dslContext).merge()
        if (save is NursingSession) {
            updated += save.dealWithTimestamps()
        }
        if (updated == 0) {
            logger.atWarn().addKeyValue("affectedRows", updated).addKeyValue("transaction", transaction)
                .log("Save affected no rows")
        } else {
            logger.atDebug().addKeyValue("affectedRows", updated).addKeyValue("transaction", transaction)
                .log("Saved record")
        }
        saveOrUpdateDevice(device, transaction.sync)
    }

    fun NursingSession.dealWithTimestamps() = cleanUpTimestamps(id) + dealWithTimestamps(id, leftTimestamps, NursingSessionTimestampsSide.LEFT) + dealWithTimestamps(id, rightTimestamps, NursingSessionTimestampsSide.RIGHT)

    fun cleanUpTimestamps(nursingSessionId: String): Int = dslContext.deleteFrom(NURSING_SESSION_TIMESTAMPS)
        .where(NURSING_SESSION_TIMESTAMPS.NURSING_SESSION_ID.eq(nursingSessionId))
        .execute()

    fun <R : TableRecord<R>> dealWithTimestamps(
        nursingSessionId: String,
        timeStamps: Collection<NursingSessionTimestamp>,
        side: NursingSessionTimestampsSide,
    ): Int = timeStamps.asSequence()
        .map { session ->
            dslContext.newRecord(NURSING_SESSION_TIMESTAMPS).also {
                it.nursingSessionId = nursingSessionId
                it.startItem = session.startItem
                it.time = session.time?.atZone(UTC)?.toLocalDateTime()
                it.side = side
            }
        }
        .toList()
        .let { dslContext.batchInsert(it).execute() }
        .sum()

    private fun saveOrUpdateDevice(device: DeviceRecord, sync: Int) {
        device.sync = sync
        device.update()
    }

    private fun DeviceInfo.getOrCreateDevice() = dslContext.insertInto(DEVICE, DEVICE.ID, DEVICE.NAME, DEVICE.SYNC, DEVICE.OS_INFO)
        .values(id, name, 0, osInfo)
        .onDuplicateKeyUpdate()
        .set(DEVICE.ID, DEVICE.ID)
        .returning()
        .fetchSingle()
}
