package uk.co.borismorris.babyflux.repository

import org.apache.hc.client5.http.cookie.Cookie
import org.apache.hc.client5.http.cookie.CookieStore
import org.apache.hc.client5.http.impl.cookie.BasicClientCookie
import org.jooq.DSLContext
import org.jooq.impl.DSL.excluded
import org.jooq.impl.DSL.multiset
import org.jooq.types.UInteger
import org.springframework.transaction.annotation.Transactional
import uk.co.borismorris.babyflux.generated.database.tables.references.COOKIE
import uk.co.borismorris.babyflux.generated.database.tables.references.COOKIE_ATTRIBUTES
import uk.co.borismorris.monitoring.logging.KotlinLogging
import java.time.Clock
import java.time.Instant
import java.time.ZoneOffset.UTC
import java.util.*

private val logger = KotlinLogging.logger {}

val SUPPORTED_ATTRIBUTES = Cookie::class.java.declaredFields.asSequence()
    .filter { String::class.java.isAssignableFrom(it.type) && it.name.endsWith("_ATTR") }
    .map { it.get(null) as String }
    .toSet()

@Transactional
class CookieRepository(private val dslContext: DSLContext, private val clock: Clock = Clock.systemUTC()) : CookieStore {

    override fun addCookie(cookie: Cookie) {
        if (cookie.isExpired(clock.instant())) {
            return
        }
        val saved = requireNotNull(cookie.saveCookie())
        val attrs = cookie.saveAttributes(saved)
        logger.atInfo()
            .addKeyValue("cookie", cookie.name)
            .addKeyValue("id", saved)
            .addKeyValue("attributes", attrs)
            .log("Saved cookie")
    }

    private fun Cookie.saveAttributes(saved: UInteger): List<String> {
        val (present, missing) = SUPPORTED_ATTRIBUTES.partition { containsAttribute(it) }
        if (missing.isNotEmpty()) {
            dslContext.deleteFrom(COOKIE_ATTRIBUTES)
                .where(COOKIE_ATTRIBUTES.COOKIE_ID.eq(saved).and(COOKIE_ATTRIBUTES.NAME.`in`(missing)))
                .execute()
        }
        if (present.isNotEmpty()) {
            present
                .fold(
                    dslContext.insertInto(
                        COOKIE_ATTRIBUTES,
                        COOKIE_ATTRIBUTES.COOKIE_ID,
                        COOKIE_ATTRIBUTES.NAME,
                        COOKIE_ATTRIBUTES.VALUE,
                    ),
                ) { q, attr -> q.values(saved, attr, getAttribute(attr)) }
                .onDuplicateKeyUpdate()
                .set(COOKIE_ATTRIBUTES.VALUE, excluded(COOKIE_ATTRIBUTES.VALUE))
                .execute()
        }
        return present
    }

    private fun Cookie.saveCookie() = dslContext.insertInto(
        COOKIE,
        COOKIE.NAME,
        COOKIE.VALUE,
        COOKIE.DOMAIN,
        COOKIE.EXPIRY_DATE,
        COOKIE.PATH,
        COOKIE.SECURE,
        COOKIE.CREATION_DATE,
        COOKIE.HTTP_ONLY,
    )
        .values(
            name,
            value,
            domain.orEmpty(),
            expiryInstant?.atZone(UTC)?.toLocalDateTime(),
            path ?: "/",
            isSecure,
            creationInstant.atZone(UTC).toLocalDateTime(),
            isHttpOnly,
        )
        .onDuplicateKeyUpdate()
        .set(COOKIE.VALUE, value)
        .set(COOKIE.EXPIRY_DATE, expiryInstant?.atZone(UTC)?.toLocalDateTime())
        .set(COOKIE.SECURE, isSecure)
        .set(COOKIE.CREATION_DATE, creationInstant.atZone(UTC).toLocalDateTime())
        .set(COOKIE.HTTP_ONLY, isHttpOnly)
        .returning(COOKIE.ID)
        .fetchSingle(COOKIE.ID)

    override fun getCookies(): List<Cookie?> = dslContext.select(
        COOKIE.NAME,
        COOKIE.VALUE,
        COOKIE.DOMAIN,
        COOKIE.EXPIRY_DATE,
        COOKIE.PATH,
        COOKIE.SECURE,
        COOKIE.CREATION_DATE,
        COOKIE.HTTP_ONLY,
        multiset(
            dslContext.select(
                COOKIE_ATTRIBUTES.NAME,
                COOKIE_ATTRIBUTES.VALUE,
            ).from(COOKIE_ATTRIBUTES)
                .where(COOKIE_ATTRIBUTES.COOKIE_ID.eq(COOKIE.ID)),
        ),
    )
        .from(COOKIE)
        .fetch {
            BasicClientCookie(it[COOKIE.NAME], it[COOKIE.VALUE]).apply {
                domain = it[COOKIE.DOMAIN]
                setExpiryDate(it[COOKIE.EXPIRY_DATE]?.toInstant(UTC))
                path = it[COOKIE.PATH]
                isSecure = it[COOKIE.SECURE] == true
                setCreationDate(it[COOKIE.CREATION_DATE]?.toInstant(UTC))
                isHttpOnly = it[COOKIE.HTTP_ONLY] == true
                it.component9().forEach { attr ->
                    setAttribute(attr[COOKIE_ATTRIBUTES.NAME], attr[COOKIE_ATTRIBUTES.VALUE])
                }
            }
        }

    @Deprecated("Deprecated in Java")
    override fun clearExpired(date: Date) = clearExpired(date.toInstant())

    override fun clearExpired(date: Instant): Boolean {
        val deleted = dslContext.deleteFrom(COOKIE)
            .where(COOKIE.EXPIRY_DATE.le(date.atZone(UTC).toLocalDateTime()))
            .execute()
        logger.atInfo()
            .addKeyValue("date", date)
            .addKeyValue("deleted", deleted)
            .log("Cleared expired cookies")
        return deleted > 0
    }

    override fun clear() {
        val deleted = dslContext.deleteFrom(COOKIE).execute()
        logger.atInfo()
            .addKeyValue("deleted", deleted)
            .log("Cleared cookies")
    }
}
