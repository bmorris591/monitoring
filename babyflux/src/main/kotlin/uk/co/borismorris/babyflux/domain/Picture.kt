package uk.co.borismorris.babyflux.domain

import com.fasterxml.jackson.annotation.JsonProperty
import org.jooq.Table
import uk.co.borismorris.babyflux.generated.database.tables.records.PictureRecord
import uk.co.borismorris.babyflux.generated.database.tables.references.PICTURE

enum class MediaType {
    @JsonProperty(index = 0)
    PICTURE,

    @JsonProperty(index = 1)
    VIDEO,

    @JsonProperty(index = 2)
    AUDIO,
    UNKNOWN,
}

class Picture : BCObject<PictureRecord>() {
    @JsonProperty("activityID")
    var activityId: String = ""
    var attached: Boolean = false
    var deleted: Boolean = false
    var fileName: String = ""
    var mediaType: MediaType = MediaType.UNKNOWN

    override val table: Table<PictureRecord>
        get() = PICTURE

    override fun copyToRecord(record: PictureRecord) {
        super.copyToRecord(record)
        record.activityId = activityId
        record.attached = attached
        record.deleted = deleted
        record.fileName = fileName
        record.mediaType = mediaType.name
    }

    override fun toString() = "Picture(activityId='$activityId', attached=$attached, deleted=$deleted, fileName='$fileName', mediaType=$mediaType) ${super.toString()}"
}
