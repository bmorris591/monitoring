package uk.co.borismorris.babyflux.domain

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties("BCObjectType")
sealed class Measure {
    var englishMeasure: Boolean = false
    var value: Double = 0.toDouble()

    override fun toString() = "Measure(englishMeasure=$englishMeasure, value=$value)"
}

class VolumeMeasure : Measure() {
    companion object {
        fun zero() = VolumeMeasure()
    }

    override fun toString() = "VolumeMeasure() ${super.toString()}"
}

class TemperatureMeasure : Measure() {
    companion object {
        fun zero() = TemperatureMeasure()
    }

    override fun toString() = "TemperatureMeasure() ${super.toString()}"
}

class WeightMeasure : Measure() {
    companion object {
        fun zero() = WeightMeasure()
    }

    override fun toString() = "WeightMeasure() ${super.toString()}"
}

class LengthMeasure : Measure() {
    companion object {
        fun zero() = LengthMeasure()
    }

    override fun toString() = "LengthMeasure() ${super.toString()}"
}
