package uk.co.borismorris.babyflux.client

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonUnwrapped

class UserInfo {
    @JsonProperty("AppInfo")
    var appInfo: AppInfo = AppInfo()

    @JsonProperty("Device")
    var device: DeviceInfo = DeviceInfo()

    @JsonProperty("EmailAddress")
    lateinit var email: String

    @JsonProperty("Password")
    lateinit var password: String

    override fun toString() = "UserInfo(appInfo=$appInfo, device=$device, email='$email', password='XXXX')"
}

class AppInfo {
    @JsonProperty("AccountType")
    var accountType: Int = 0

    @JsonProperty("AppType")
    var appType: Int = 0

    override fun toString() = "AppInfo(accountType=$accountType, appType=$appType)"
}

class DeviceInfo {
    @JsonProperty("DeviceName")
    lateinit var name: String

    @JsonProperty("DeviceOSInfo")
    lateinit var osInfo: String

    @JsonProperty("DeviceUUID")
    lateinit var id: String

    override fun toString() = "DeviceInfo(name='$name', osInfo='$osInfo', id='$id')"
}

data class AccountInfo(@JsonProperty("AccountID") val accountId: Long)

data class DeviceSyncInfo(@JsonProperty("LastSyncID") val syncId: Int) {
    @JsonUnwrapped
    lateinit var device: DeviceInfo

    constructor(syncId: Int, device: DeviceInfo) : this(syncId) {
        this.device = device
    }

    override fun toString() = "DeviceSyncInfo(syncId=$syncId, device=$device)"
}
