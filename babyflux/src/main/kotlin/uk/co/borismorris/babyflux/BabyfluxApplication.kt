package uk.co.borismorris.babyflux

import com.fasterxml.jackson.databind.ObjectMapper
import io.micrometer.observation.ObservationRegistry
import org.apache.hc.client5.http.cookie.CookieStore
import org.jooq.DSLContext
import org.springframework.beans.factory.ListableBeanFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.autoconfigure.jooq.DefaultConfigurationCustomizer
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.http.client.ClientHttpRequestFactoryBuilder
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.transaction.annotation.EnableTransactionManagement
import org.springframework.web.client.RestClient
import uk.co.borismorris.babyflux.client.BabyTrackerClient
import uk.co.borismorris.babyflux.client.RestClientBabyTrackerClient
import uk.co.borismorris.babyflux.config.BabyProps
import uk.co.borismorris.babyflux.domain.BCObject
import uk.co.borismorris.babyflux.domain.BcObjectConverter
import uk.co.borismorris.babyflux.repository.CookieRepository
import uk.co.borismorris.babyflux.repository.DeviceTransactionPersister
import uk.co.borismorris.babyflux.repository.RepositoryDeviceTransactionPersister
import uk.co.borismorris.babyflux.worker.GatherBabyData
import uk.co.borismorris.babyflux.worker.SimpleTransactionSync
import uk.co.borismorris.babyflux.worker.TransactionSync
import uk.co.borismorris.monitoring.observability.logging.LogConfigOnStartup

@SpringBootApplication(proxyBeanMethods = false)
@EnableTransactionManagement
@EnableConfigurationProperties(BabyProps::class)
@EntityScan(basePackageClasses = [BCObject::class])
class BabyfluxApplication {

    @Bean
    fun jooqCookieStore(dslContext: DSLContext) = CookieRepository(dslContext)

    @Bean
    fun clientRequestFactoryBuilder(cookieStore: CookieStore): ClientHttpRequestFactoryBuilder<*> = ClientHttpRequestFactoryBuilder.httpComponents()
        .withHttpClientCustomizer { it.setDefaultCookieStore(cookieStore) }

    @Bean
    fun jooqConfigurationCustomizer(@Value("\${spring.jooq.logging:false}") logging: Boolean) = DefaultConfigurationCustomizer {
        it.settings().apply {
            isRenderGroupConcatMaxLenSessionVariable = false
            isExecuteLogging = logging
            isDiagnosticsLogging = logging
        }
    }

    @Bean
    fun bcObjectConverter(mapper: ObjectMapper) = BcObjectConverter(mapper)

    @Bean
    fun babyTrackerClient(
        babyProps: BabyProps,
        restClientBuilder: RestClient.Builder,
    ) = RestClientBabyTrackerClient(babyProps, restClientBuilder)

    @Bean
    fun repositoryTransactionPersister(
        observationRegistry: ObservationRegistry,
        dslContext: DSLContext,
        mapper: ObjectMapper,
    ) = RepositoryDeviceTransactionPersister(observationRegistry, dslContext, mapper)

    @Bean
    fun simpleTransactionSync(
        observationRegistry: ObservationRegistry,
        babyTrackerClient: BabyTrackerClient,
        transactionPersister: DeviceTransactionPersister,
        dslContext: DSLContext,
    ) = SimpleTransactionSync(observationRegistry, babyTrackerClient, transactionPersister, dslContext)

    @Bean
    fun gatherBabyData(babyTrackerClient: BabyTrackerClient, transactionSync: TransactionSync) = GatherBabyData(babyTrackerClient, transactionSync)

    @Bean
    fun logConfigOnStartup(context: ListableBeanFactory) = LogConfigOnStartup(context, BabyProps::class)

    @Configuration(proxyBeanMethods = false)
    @Profile("!test")
    @EnableScheduling
    class Scheduling
}

@Suppress("SpreadOperator")
fun main(args: Array<String>) {
    runApplication<BabyfluxApplication>(*args)
}
