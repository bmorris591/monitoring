package uk.co.borismorris.babyflux.worker

import io.micrometer.observation.Observation
import io.micrometer.observation.ObservationRegistry
import io.micrometer.observation.annotation.Observed
import org.jooq.DSLContext
import uk.co.borismorris.babyflux.client.BabyTrackerClient
import uk.co.borismorris.babyflux.client.DeviceSyncInfo
import uk.co.borismorris.babyflux.generated.database.tables.references.DEVICE
import uk.co.borismorris.babyflux.repository.DeviceTransactionPersister
import uk.co.borismorris.monitoring.logging.KotlinLogging
import java.util.function.Supplier

private val logger = KotlinLogging.logger {}

interface TransactionSync {
    fun syncTransactions(): List<SyncInfo>
}

sealed interface SyncInfo {
    val deviceSyncInfo: DeviceSyncInfo
    val previousSync: Int
    val syncCount: Long
}

data class CompletedSyncInfo(
    override val deviceSyncInfo: DeviceSyncInfo,
    override val previousSync: Int,
    override val syncCount: Long,
    val currentSync: Int,
) : SyncInfo

data class SkippedSync(
    override val deviceSyncInfo: DeviceSyncInfo,
    override val previousSync: Int,
) : SyncInfo {
    override val syncCount: Long
        get() = 0
}

open class SimpleTransactionSync(
    private val observationRegistry: ObservationRegistry,
    private val babyTrackerClient: BabyTrackerClient,
    private val transactionPersister: DeviceTransactionPersister,
    private val dslContext: DSLContext,
) : TransactionSync {

    @Observed
    override fun syncTransactions(): List<SyncInfo> = dslContext.fetch(DEVICE)
        .associate { it.id to it.sync }.withDefault { 0 }
        .let { syncDevices(it) }

    private fun syncDevices(devices: Map<String, Int>): List<SyncInfo> {
        logger.atInfo().setMessage("Syncing devices").addKeyValue("devices", devices).log()
        return babyTrackerClient.getDeviceList()
            .map { syncDevice(devices.getValue(it.device.id), it) }
            .toList()
    }

    private fun syncDevice(previousSync: Int, deviceSyncInfo: DeviceSyncInfo): SyncInfo = Observation.createNotStarted("sync-device-transactions", observationRegistry)
        .highCardinalityKeyValue("device.id", deviceSyncInfo.device.id)
        .highCardinalityKeyValue("device.name", deviceSyncInfo.device.name)
        .observe(
            Supplier {
                if (deviceSyncInfo.syncId == previousSync) {
                    logger.atInfo().setMessage("Transactions up to date")
                        .addKeyValue("deviceSyncInfo", deviceSyncInfo)
                        .addKeyValue("previousSync", previousSync)
                        .log()
                    SkippedSync(deviceSyncInfo, previousSync)
                } else {
                    syncDevice(deviceSyncInfo, previousSync)
                }
            },
        )!!

    private fun syncDevice(deviceSyncInfo: DeviceSyncInfo, existingSync: Int): CompletedSyncInfo {
        val deviceInfo = deviceSyncInfo.device
        return babyTrackerClient.getTransactionLog(deviceInfo, existingSync)
            .onEach { transactionPersister.processTransaction(deviceInfo, it) }
            .count()
            .let { count ->
                val device = dslContext.fetchSingle(DEVICE, DEVICE.ID.eq(deviceInfo.id))
                CompletedSyncInfo(deviceSyncInfo, existingSync, count.toLong(), device.sync)
            }
    }
}
