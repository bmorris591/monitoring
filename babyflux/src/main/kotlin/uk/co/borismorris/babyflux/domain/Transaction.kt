package uk.co.borismorris.babyflux.domain

import com.fasterxml.jackson.annotation.JsonAlias
import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonTypeIdResolver
import org.jooq.DSLContext
import org.jooq.Table
import org.jooq.UpdatableRecord
import org.jooq.impl.DSL.field
import uk.co.borismorris.babyflux.generated.database.tables.records.ConflictRecordRecord
import uk.co.borismorris.babyflux.generated.database.tables.references.CONFLICT_RECORD
import uk.co.borismorris.monitoring.logging.KotlinLogging
import java.time.Instant

private val logger = KotlinLogging.logger {}

enum class TransactionLogOpCode {
    @JsonProperty(index = 0)
    INSERT,

    @JsonProperty(index = 1)
    UPDATE,

    @JsonProperty(index = 2)
    DELETE,

    @JsonProperty(index = 3)
    RELIVE,

    @JsonProperty(index = 4)
    CONFLICT,
}

data class TransactionRecord(
    @JsonAlias(value = ["SyncID", "sync"])
    val sync: Int,
    @JsonAlias(value = ["OPCode", "opcode"])
    val oPCode: TransactionLogOpCode,
    @JsonAlias(value = ["Transaction", "encodedTransaction"])
    @JsonDeserialize(converter = BcObjectConverter::class)
    val transaction: BCObject<*>,
)

@JsonTypeInfo(
    use = JsonTypeInfo.Id.CUSTOM,
    include = JsonTypeInfo.As.PROPERTY,
    property = "BCObjectType",
)
@JsonTypeIdResolver(GsonTypeIdResolver::class)
abstract class BCObject<R : UpdatableRecord<R>> {
    @get:JsonIgnore
    abstract val table: Table<R>

    @JsonProperty("objectID")
    var id: String = ""

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss X", timezone = "UTC")
    var timestamp: Instant? = null

    @JsonAlias("newFlage")
    var newFlag: Boolean = false

    var flag: Int = 0

    var queryIndex: Int = 0

    open fun copyToRecord(record: R) {
        record.set(field("id"), id)
        record.set(field("timestamp"), timestamp)
    }

    open fun toRecord(dslContext: DSLContext): UpdatableRecord<R> = dslContext.newRecord(table).apply { copyToRecord(this) }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is BCObject<*>) return false

        if (id != other.id) return false

        return true
    }

    override fun hashCode() = id.hashCode()

    override fun toString() = "BCObject(id='$id', timestamp=$timestamp)"
}

class ConflictRecord : BCObject<ConflictRecordRecord>() {
    var loser: BCObject<*>? = null

    var winner: BCObject<*>? = null

    override fun copyToRecord(record: ConflictRecordRecord) {
        super.copyToRecord(record)
        loser?.id?.let { record.set(CONFLICT_RECORD.LOSER_ID, it) }
        winner?.id?.let { record.set(CONFLICT_RECORD.WINNER_ID, it) }
    }

    override val table: Table<ConflictRecordRecord>
        get() = CONFLICT_RECORD

    override fun toString() = "ConflictRecord(loser=$loser, winner=$winner) ${super.toString()}"
}
