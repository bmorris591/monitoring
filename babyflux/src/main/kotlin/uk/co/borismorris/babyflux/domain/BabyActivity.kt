package uk.co.borismorris.babyflux.domain

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import org.jooq.Table
import org.jooq.UpdatableRecord
import org.jooq.impl.DSL
import uk.co.borismorris.babyflux.generated.database.tables.records.BathRecord
import uk.co.borismorris.babyflux.generated.database.tables.records.DiaperRecord
import uk.co.borismorris.babyflux.generated.database.tables.records.GrowthRecord
import uk.co.borismorris.babyflux.generated.database.tables.records.JournalRecord
import uk.co.borismorris.babyflux.generated.database.tables.records.JoyRecord
import uk.co.borismorris.babyflux.generated.database.tables.records.MedicationRecord
import uk.co.borismorris.babyflux.generated.database.tables.records.MilestoneRecord
import uk.co.borismorris.babyflux.generated.database.tables.records.OtherActivityRecord
import uk.co.borismorris.babyflux.generated.database.tables.records.SleepRecord
import uk.co.borismorris.babyflux.generated.database.tables.records.TemperatureRecord
import uk.co.borismorris.babyflux.generated.database.tables.records.VaccineRecord
import uk.co.borismorris.babyflux.generated.database.tables.references.BATH
import uk.co.borismorris.babyflux.generated.database.tables.references.DIAPER
import uk.co.borismorris.babyflux.generated.database.tables.references.GROWTH
import uk.co.borismorris.babyflux.generated.database.tables.references.JOURNAL
import uk.co.borismorris.babyflux.generated.database.tables.references.JOY
import uk.co.borismorris.babyflux.generated.database.tables.references.MEDICATION
import uk.co.borismorris.babyflux.generated.database.tables.references.MILESTONE
import uk.co.borismorris.babyflux.generated.database.tables.references.OTHER_ACTIVITY
import uk.co.borismorris.babyflux.generated.database.tables.references.SLEEP
import uk.co.borismorris.babyflux.generated.database.tables.references.TEMPERATURE
import uk.co.borismorris.babyflux.generated.database.tables.references.VACCINE
import java.time.Duration

abstract class BabyActivity<R : UpdatableRecord<R>> : Activity<R>() {
    @JsonTypeInfo(use = JsonTypeInfo.Id.NONE)
    var baby: Baby? = null

    override fun copyToRecord(record: R) {
        super.copyToRecord(record)
        record.set(DSL.field("baby_id"), baby?.id)
    }

    override fun toString() = "BabyActivity(baby=$baby) ${super.toString()}"
}

class Bath : BabyActivity<BathRecord>() {
    override fun toString() = "Bath() ${super.toString()}"

    override val table: Table<BathRecord>
        get() = BATH
}

enum class DiaperAmount {
    @JsonProperty(index = 0)
    NONE,

    @JsonProperty(index = 1)
    LIGHT,

    @JsonProperty(index = 2)
    NORMAL,

    @JsonProperty(index = 3)
    HEAVY,

    @JsonProperty(index = 4)
    OVERFLOW,

    @JsonProperty(index = 5)
    NOT_SET,
}

enum class DiaperStatus {
    @JsonProperty(index = 0)
    WET,

    @JsonProperty(index = 1)
    DIRTY,

    @JsonProperty(index = 2)
    MIXED,

    @JsonProperty(index = 3)
    DRY,
}

@JsonIgnoreProperties("pooColor", "peeColor", "texture")
class Diaper : BabyActivity<DiaperRecord>() {
    @JsonProperty("amount")
    var diaperAmount: DiaperAmount = DiaperAmount.NONE

    var status: DiaperStatus = DiaperStatus.DIRTY

    override val table: Table<DiaperRecord>
        get() = DIAPER

    override fun copyToRecord(record: DiaperRecord) {
        super.copyToRecord(record)
        record.status = status.name
    }

    override fun toString() = "Diaper(amount=$diaperAmount, status=$status) ${super.toString()}"
}

class Growth : BabyActivity<GrowthRecord>() {
    var deleted: Boolean = false
    var head: LengthMeasure? = LengthMeasure.zero()
    var length: LengthMeasure? = LengthMeasure.zero()
    var weight: WeightMeasure? = WeightMeasure.zero()

    override val table: Table<GrowthRecord>
        get() = GROWTH

    override fun copyToRecord(record: GrowthRecord) {
        super.copyToRecord(record)
        record.deleted = deleted
        head?.let {
            record.head = it.value
            record.headEnglishMeasure = it.englishMeasure
        }
        length?.let {
            record.length = it.value
            record.lengthEnglishMeasure = it.englishMeasure
        }
        weight?.let {
            record.weight = it.value
            record.weightEnglishMeasure = it.englishMeasure
        }
    }

    override fun toString() = "Growth(deleted=$deleted, head=$head, length=$length, weight=$weight) ${super.toString()}"
}

class Journal : BabyActivity<JournalRecord>() {
    override fun toString() = "Journal() ${super.toString()}"

    override val table: Table<JournalRecord>
        get() = JOURNAL
}

class Joy : BabyActivity<JoyRecord>() {
    override fun toString() = "Joy() ${super.toString()}"

    override val table: Table<JoyRecord>
        get() = JOY
}

class Medication : BabyActivity<MedicationRecord>() {
    @JsonProperty("amount")
    var medicationAmount: Double = 0.toDouble()

    @JsonTypeInfo(use = JsonTypeInfo.Id.NONE)
    var medicationSelection: MedicationSelection? = null

    override val table: Table<MedicationRecord>
        get() = MEDICATION

    override fun copyToRecord(record: MedicationRecord) {
        super.copyToRecord(record)
        record.amount = medicationAmount
        record.medicationSelectionId = medicationSelection?.id
    }

    override fun toString() = "Medication(amount=$medicationAmount, medicationSelection=$medicationSelection) ${super.toString()}"
}

class Milestone : BabyActivity<MilestoneRecord>() {
    var hasNewType: Boolean = false

    @JsonTypeInfo(use = JsonTypeInfo.Id.NONE)
    var milestoneType: MilestoneSelection? = null

    override val table: Table<MilestoneRecord>
        get() = MILESTONE

    override fun copyToRecord(record: MilestoneRecord) {
        super.copyToRecord(record)
        record.hasNewType = hasNewType
        record.milestoneTypeId = milestoneType?.id
    }

    override fun toString() = "Milestone(hasNewType=$hasNewType, milestoneType=$milestoneType) ${super.toString()}"
}

class OtherActivity : BabyActivity<OtherActivityRecord>() {
    @JsonTypeInfo(use = JsonTypeInfo.Id.NONE)
    var desc: OtherActivityDescription? = null

    @JsonDeserialize(using = MinuteDurationDeserialiser::class)
    var duration: Duration = Duration.ZERO

    override val table: Table<OtherActivityRecord>
        get() = OTHER_ACTIVITY

    override fun copyToRecord(record: OtherActivityRecord) {
        super.copyToRecord(record)
        record.descId = desc?.id
        record.duration = duration.toNanos()
    }

    override fun toString() = "OtherActivity(desc=$desc, duration=$duration) ${super.toString()}"
}

class Sleep : BabyActivity<SleepRecord>() {
    @JsonDeserialize(using = MinuteDurationDeserialiser::class)
    var duration: Duration = Duration.ZERO

    override val table: Table<SleepRecord>
        get() = SLEEP

    override fun copyToRecord(record: SleepRecord) {
        super.copyToRecord(record)
        record.duration = duration.toNanos()
    }

    override fun toString() = "Sleep(duration=$duration) ${super.toString()}"
}

@Suppress("MemberNameEqualsClassName")
class Temperature : BabyActivity<TemperatureRecord>() {
    var temperature: TemperatureMeasure? = TemperatureMeasure.zero()

    override val table: Table<TemperatureRecord>
        get() = TEMPERATURE

    override fun copyToRecord(record: TemperatureRecord) {
        super.copyToRecord(record)
        temperature?.let {
            record.value = it.value
            record.englishMeasure = it.englishMeasure
        }
    }

    override fun toString() = "Temperature(temperature=$temperature)"
}

class Vaccine : BabyActivity<VaccineRecord>() {
    @JsonTypeInfo(use = JsonTypeInfo.Id.NONE)
    var vaccineType: VaccineSelection? = null

    override val table: Table<VaccineRecord>
        get() = VACCINE

    override fun copyToRecord(record: VaccineRecord) {
        super.copyToRecord(record)
        record.vaccineTypeId = vaccineType?.id
    }

    override fun toString() = "Vaccine(vaccineType=$vaccineType) ${super.toString()}"
}
