package uk.co.borismorris.babyflux.domain

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import org.jooq.Table
import org.jooq.UpdatableRecord
import org.jooq.impl.DSL.field
import uk.co.borismorris.babyflux.generated.database.tables.records.PumpRecord
import uk.co.borismorris.babyflux.generated.database.tables.references.PUMP
import java.time.Duration
import java.time.Instant
import java.time.ZoneOffset.UTC

abstract class Activity<R : UpdatableRecord<R>> :
    BCObject<R>(),
    Comparable<Activity<R>> {
    var note: String = ""
    var pictureLoaded: Boolean = false

    @Suppress("DoubleMutabilityForCollection")
    var pictureNote: MutableSet<Picture> = mutableSetOf()

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss X", timezone = "UTC")
    var time: Instant? = null

    override fun copyToRecord(record: R) {
        super.copyToRecord(record)
        record.set(field("note"), note)
        record.set(field("picture_loaded"), pictureLoaded)
        record.set(field("time"), time?.atZone(UTC)?.toLocalDateTime())
    }

    override fun compareTo(other: Activity<R>) = compareValuesBy(this, other) { it.time }

    override fun toString() = "Activity(note='$note', pictureLoaded=$pictureLoaded, pictureNote=$pictureNote, time=$time) ${super.toString()}"
}

enum class FinishSide {
    @JsonProperty(index = 0)
    NOT_SET,

    @JsonProperty(index = 1)
    LEFT,

    @JsonProperty(index = 2)
    RIGHT,
}

enum class PumpSides {
    @JsonProperty(index = 0)
    BOTH,

    @JsonProperty(index = 1)
    LEFT,

    @JsonProperty(index = 2)
    RIGHT,
    NOT_SET,
}

class Pump : Activity<PumpRecord>() {

    @JsonProperty("amount")
    var pumpAmount: VolumeMeasure? = VolumeMeasure.zero()

    var finishSide: FinishSide = FinishSide.NOT_SET

    var label: String = ""

    var sides: PumpSides = PumpSides.NOT_SET

    @JsonDeserialize(using = MinuteDurationDeserialiser::class)
    var leftDuration: Duration = Duration.ZERO

    @JsonDeserialize(using = MinuteDurationDeserialiser::class)
    var rightDuration: Duration = Duration.ZERO

    override val table: Table<PumpRecord>
        get() = PUMP

    override fun copyToRecord(record: PumpRecord) {
        super.copyToRecord(record)
        pumpAmount?.let {
            record.value = it.value
            record.englishMeasure = it.englishMeasure
        }
        record.finishSide = finishSide.name
        record.label = label
        record.sides = sides.name
        record.leftDuration = leftDuration.toNanos()
        record.rightDuration = rightDuration.toNanos()
    }

    override fun toString() = "Pump(pumpAmount=$pumpAmount, finishSide=$finishSide, label='$label', sides=$sides, leftDuration=$leftDuration, rightDuration=$rightDuration) ${super.toString()}"
}
