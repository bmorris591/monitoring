package uk.co.borismorris.babyflux.config

import org.springframework.boot.context.properties.ConfigurationProperties
import java.net.URL
import java.time.Duration

@ConfigurationProperties(prefix = "baby")
data class BabyProps(val url: URL, val pollingInterval: Duration, val userInfo: UserInfo)

data class UserInfo(val appInfo: AppInfo = AppInfo(), val device: DeviceInfo, val email: String, val password: String)

data class AppInfo(val accountType: Int = 0, val appType: Int = 0)

data class DeviceInfo(val name: String, val osInfo: String, val id: String)
