package uk.co.borismorris.babyflux.domain

data class Device(
    var name: String,
    var osInfo: String,
    var id: String,
    var sync: Int,
)
