package uk.co.borismorris.babyflux.client

import io.micrometer.observation.annotation.Observed
import org.springframework.http.MediaType
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.http.MediaType.TEXT_HTML
import org.springframework.http.converter.json.AbstractJackson2HttpMessageConverter
import org.springframework.web.client.RestClient
import org.springframework.web.client.body
import uk.co.borismorris.babyflux.config.BabyProps
import uk.co.borismorris.babyflux.config.UserInfo
import uk.co.borismorris.babyflux.domain.TransactionRecord

interface BabyTrackerClient {
    fun getTransactionLog(deviceInfo: DeviceInfo, startTx: Int): Sequence<TransactionRecord>
    fun getDeviceList(): Sequence<DeviceSyncInfo>
    fun login(): AccountInfo
}

@Suppress("UnsafeCallOnNullableType")
@Observed
open class RestClientBabyTrackerClient(
    private val babyProps: BabyProps,
    restClientBuilder: RestClient.Builder,
) : BabyTrackerClient {

    private val restClient = restClientBuilder
        .messageConverters { converters ->
            converters.find { it is AbstractJackson2HttpMessageConverter }?.let { converter ->
                (converter as AbstractJackson2HttpMessageConverter).supportedMediaTypes =
                    listOf(APPLICATION_JSON, TEXT_HTML)
            }
        }
        .defaultHeaders {
            it.accept = listOf(MediaType.ALL)
        }
        .baseUrl(babyProps.url.toString())
        .build()

    override fun getTransactionLog(deviceInfo: DeviceInfo, startTx: Int): Sequence<TransactionRecord> = restClient.get()
        .uri("/account/transaction/{deviceId}/{startTx}", mapOf("deviceId" to deviceInfo.id, "startTx" to startTx))
        .retrieve()
        .body<List<TransactionRecord>>()!!
        .asSequence()

    override fun getDeviceList(): Sequence<DeviceSyncInfo> = restClient.get().uri("/account/device")
        .retrieve()
        .body<List<DeviceSyncInfo>>()!!
        .asSequence()

    override fun login(): AccountInfo = restClient.post().uri("/session")
        .contentType(APPLICATION_JSON)
        .body(babyProps.userInfo.toUserInfo())
        .retrieve()
        .body<AccountInfo>()!!

    private fun UserInfo.toUserInfo(): uk.co.borismorris.babyflux.client.UserInfo = UserInfo().also {
        with(it.appInfo) {
            accountType = appInfo.accountType
            appType = appInfo.appType
        }
        with(it.device) {
            name = device.name
            osInfo = device.osInfo
            id = device.id
        }

        it.password = password
        it.email = email
    }
}

class ResponseParseException(message: String, cause: Throwable) : RuntimeException("Failed to parse response. $message", cause)
