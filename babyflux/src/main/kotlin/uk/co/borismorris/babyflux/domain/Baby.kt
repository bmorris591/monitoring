package uk.co.borismorris.babyflux.domain

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonFormat
import org.jooq.Table
import uk.co.borismorris.babyflux.generated.database.tables.records.BabyRecord
import uk.co.borismorris.babyflux.generated.database.tables.references.BABY
import java.time.Instant
import java.time.ZoneOffset.UTC

enum class Gender {
    MALE,
    FEMALE,
    OTHER,
    NOT_SET, ;

    companion object {
        @JsonCreator
        @JvmStatic
        fun parse(value: Boolean) = if (value) MALE else FEMALE
    }
}

class Baby : BCObject<BabyRecord>() {
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss X", timezone = "UTC")
    var dob: Instant? = null

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss X", timezone = "UTC")
    var dueDay: Instant? = null

    var gender: Gender = Gender.NOT_SET
    var name: String = ""
    var pictureName: String = ""

    override val table: Table<BabyRecord>
        get() = BABY

    override fun copyToRecord(record: BabyRecord) {
        super.copyToRecord(record)
        dob?.atZone(UTC)?.toLocalDateTime()?.let { record.dob = it }
        dueDay?.atZone(UTC)?.toLocalDateTime()?.let { record.dueDay = it }
        record.gender = gender.name
        record.name = name
        record.pictureName = pictureName
    }

    override fun toString() = "Baby(dob=$dob, dueDay=$dueDay, gender=$gender, name='$name', pictureName='$pictureName') ${super.toString()}"
}
