package uk.co.borismorris.babyflux.domain

import com.fasterxml.jackson.annotation.JsonAlias
import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import org.jooq.Table
import org.jooq.UpdatableRecord
import org.jooq.impl.DSL.field
import uk.co.borismorris.babyflux.generated.database.tables.records.FormulaRecord
import uk.co.borismorris.babyflux.generated.database.tables.records.NursingSessionRecord
import uk.co.borismorris.babyflux.generated.database.tables.records.OtherFeedRecord
import uk.co.borismorris.babyflux.generated.database.tables.records.PumpedRecord
import uk.co.borismorris.babyflux.generated.database.tables.references.FORMULA
import uk.co.borismorris.babyflux.generated.database.tables.references.NURSING_SESSION
import uk.co.borismorris.babyflux.generated.database.tables.references.OTHER_FEED
import uk.co.borismorris.babyflux.generated.database.tables.references.PUMPED
import java.time.Duration
import java.time.Instant

abstract class Feed<R : UpdatableRecord<R>> : BabyActivity<R>() {
    override fun toString() = "Feed() ${super.toString()}"
}

abstract class Bottle<R : UpdatableRecord<R>> : Feed<R>() {
    @JsonProperty("amount")
    var bottleAmount: VolumeMeasure? = VolumeMeasure.zero()

    override fun copyToRecord(record: R) {
        super.copyToRecord(record)
        bottleAmount?.let {
            record.set(field("value"), it.value)
            record.set(field("english_measure"), it.englishMeasure)
        }
    }

    override fun toString() = "Bottle(amount=$bottleAmount) ${super.toString()}"
}

class Formula : Bottle<FormulaRecord>() {
    override val table: Table<FormulaRecord>
        get() = FORMULA

    override fun toString() = "Formula() ${super.toString()}"
}

class Pumped : Bottle<PumpedRecord>() {
    override val table: Table<PumpedRecord>
        get() = PUMPED

    override fun toString() = "Pumped() ${super.toString()}"
}

class NursingSessionTimestamp : Comparable<NursingSessionTimestamp> {
    var startItem: Boolean = false

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss X", timezone = "UTC")
    var time: Instant? = null

    override fun compareTo(other: NursingSessionTimestamp) = compareValuesBy(this, other) { it.time }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as NursingSessionTimestamp

        if (startItem != other.startItem) return false
        if (time != other.time) return false

        return true
    }

    override fun hashCode(): Int {
        var result = startItem.hashCode()
        result = 31 * result + time.hashCode()
        return result
    }

    override fun toString() = "NursingSessionTimestamp(startItem=$startItem, time=$time)"
}

enum class NursingSessionState {
    @JsonProperty(index = 0)
    START,

    @JsonProperty(index = 1)
    LEFT_RUNNING,

    @JsonProperty(index = 2)
    RIGHT_RUNNING,

    @JsonProperty(index = 3)
    LEFT_PAUSED,

    @JsonProperty(index = 4)
    RIGHT_PAUSED,

    @JsonProperty(index = 5)
    BOTH_STOPPED,

    @JsonProperty(index = 6)
    END,
}

@Suppress("DoubleMutabilityForCollection")
class NursingSession : Feed<NursingSessionRecord>() {
    @JsonDeserialize(using = MinuteDurationDeserialiser::class)
    var bothDuration: Duration = Duration.ZERO

    var finishSide: FinishSide = FinishSide.NOT_SET

    @JsonDeserialize(using = MinuteDurationDeserialiser::class)
    var leftDuration: Duration = Duration.ZERO

    @JsonDeserialize(using = MinuteDurationDeserialiser::class)
    var rightDuration: Duration = Duration.ZERO

    @JsonAlias("isDetailed")
    var detailed: Boolean = false

    var leftTimestamps: Set<NursingSessionTimestamp> = mutableSetOf()

    var pairSessionState: NursingSessionState = NursingSessionState.START

    var rightTimestamps: Set<NursingSessionTimestamp> = mutableSetOf()

    var state: NursingSessionState = NursingSessionState.START

    override val table: Table<NursingSessionRecord>
        get() = NURSING_SESSION

    override fun copyToRecord(record: NursingSessionRecord) {
        super.copyToRecord(record)
        record.bothDuration = bothDuration.toNanos()
        record.finishSide = finishSide.name
        record.leftDuration = leftDuration.toNanos()
        record.rightDuration = rightDuration.toNanos()
        record.detailed = detailed
        record.pairSessionState = pairSessionState.name
        record.state = state.name
    }

    override fun toString() = "NursingSession(bothDuration=$bothDuration, finishSide=$finishSide, leftDuration=$leftDuration, rightDuration=$rightDuration, detailed=$detailed, leftTimestamps=$leftTimestamps, pairSessionState=$pairSessionState, rightTimestamps=$rightTimestamps, state=$state) ${super.toString()}"
}

class OtherFeed : Feed<OtherFeedRecord>() {
    @JsonProperty("amount")
    var otherFeedAmount: VolumeMeasure? = VolumeMeasure.zero()

    @JsonTypeInfo(use = JsonTypeInfo.Id.NONE)
    var feedType: OtherFeedSelection? = null
    var unit: String = ""

    override val table: Table<OtherFeedRecord>
        get() = OTHER_FEED

    override fun copyToRecord(record: OtherFeedRecord) {
        super.copyToRecord(record)
        otherFeedAmount?.let {
            record.value = it.value
            record.englishMeasure = it.englishMeasure
        }
        record.feedTypeId = feedType?.id
        record.unit = unit
    }

    override fun toString() = "OtherFeed(amount=$otherFeedAmount, feedType=$feedType, unit='$unit') ${super.toString()}"
}
