package uk.co.borismorris.babyflux.domain

import com.fasterxml.jackson.annotation.JsonAlias
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import org.jooq.Table
import org.jooq.UpdatableRecord
import org.jooq.impl.DSL
import uk.co.borismorris.babyflux.generated.database.tables.records.MedicationSelectionRecord
import uk.co.borismorris.babyflux.generated.database.tables.records.MilestoneSelectionRecord
import uk.co.borismorris.babyflux.generated.database.tables.records.OtherActivityDescriptionRecord
import uk.co.borismorris.babyflux.generated.database.tables.records.OtherFeedSelectionRecord
import uk.co.borismorris.babyflux.generated.database.tables.records.VaccineSelectionRecord
import uk.co.borismorris.babyflux.generated.database.tables.references.MEDICATION_SELECTION
import uk.co.borismorris.babyflux.generated.database.tables.references.MILESTONE_SELECTION
import uk.co.borismorris.babyflux.generated.database.tables.references.OTHER_ACTIVITY_DESCRIPTION
import uk.co.borismorris.babyflux.generated.database.tables.references.OTHER_FEED_SELECTION
import uk.co.borismorris.babyflux.generated.database.tables.references.VACCINE_SELECTION
import java.time.Duration

abstract class EditableSelection<R : UpdatableRecord<R>> : BCObject<R>() {
    @JsonProperty("desc")
    var description: String = ""
    var name: String = ""

    override fun copyToRecord(record: R) {
        super.copyToRecord(record)
        record.set(DSL.field("description"), description)
        record.set(DSL.field("name"), name)
    }

    override fun toString() = "EditableSelection(description='$description', name='$name') ${super.toString()}"
}

class MilestoneSelection : EditableSelection<MilestoneSelectionRecord>() {
    var forAge: Int = 0
    var used: Boolean = false

    override val table: Table<MilestoneSelectionRecord>
        get() = MILESTONE_SELECTION

    override fun copyToRecord(record: MilestoneSelectionRecord) {
        super.copyToRecord(record)
        record.forAge = forAge
        record.used = used
    }

    override fun toString() = "MilestoneSelection(forAge=$forAge, used=$used) ${super.toString()}"
}

class MedicationSelection : EditableSelection<MedicationSelectionRecord>() {
    var amountPerTime: Double = 0.toDouble()

    @JsonDeserialize(using = MinuteDurationDeserialiser::class)
    @JsonProperty("interval")
    var periodicity: Duration = Duration.ZERO

    @JsonAlias("isPresciption")
    var prescription: Boolean = false
    var unit: String = ""

    override val table: Table<MedicationSelectionRecord>
        get() = MEDICATION_SELECTION

    override fun copyToRecord(record: MedicationSelectionRecord) {
        super.copyToRecord(record)
        record.amountPerTime = amountPerTime
        record.periodicity = periodicity.toNanos()
        record.prescription = prescription
        record.unit = unit
    }

    override fun toString() = "MedicationSelection(amountPerTime=$amountPerTime, periodicity=$periodicity, prescription=$prescription, unit='$unit') ${super.toString()}"
}

class OtherActivityDescription : EditableSelection<OtherActivityDescriptionRecord>() {

    override val table: Table<OtherActivityDescriptionRecord>
        get() = OTHER_ACTIVITY_DESCRIPTION

    override fun toString() = "OtherActivityDescription() ${super.toString()}"
}

class OtherFeedSelection : EditableSelection<OtherFeedSelectionRecord>() {
    var bottle: Boolean = false

    override val table: Table<OtherFeedSelectionRecord>
        get() = OTHER_FEED_SELECTION

    override fun copyToRecord(record: OtherFeedSelectionRecord) {
        super.copyToRecord(record)
        record.bottle = bottle
    }

    override fun toString() = "OtherFeedSelection(bottle=$bottle) ${super.toString()}"
}

class VaccineSelection : EditableSelection<VaccineSelectionRecord>() {
    var forAge: Int = 0

    override val table: Table<VaccineSelectionRecord>
        get() = VACCINE_SELECTION

    override fun copyToRecord(record: VaccineSelectionRecord) {
        super.copyToRecord(record)
        record.forAge = forAge
    }

    override fun toString() = "VaccineSelection(forAge=$forAge) ${super.toString()}"
}
