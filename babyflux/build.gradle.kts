import io.gitlab.arturbosch.detekt.Detekt
import org.jooq.meta.jaxb.Generate
import uk.co.borismorris.jooq.JOOQ_CODEGEN_TASK_NAME
import uk.co.borismorris.jooq.JooqCodegenExtension
import uk.co.borismorris.jooq.JooqCodegenPlugin

plugins {
    monitoring.`application-conventions`
    monitoring.`jib-conventions`
}

apply<JooqCodegenPlugin>()

group = "uk.co.borismorris.babyflux"

val jooqDir = layout.buildDirectory.dir("generated/jooq")
configure<JooqCodegenExtension> {
    migrations = file("src/main/resources/db/migration/mariadb")
    outputDirectory = jooqDir
    packageName = "uk.co.borismorris.babyflux.generated.database"
    excludesRegex = "us_cdc_growth | weight_kg_male | head_circumference_cm_male | length_cm_male"
    generate =
        Generate()
            .withImplicitJoinPathsAsKotlinProperties(true)
            .withKotlinSetterJvmNameAnnotationsOnIsPrefix(true)
            .withKotlinNotNullRecordAttributes(true)
            .withKotlinDefaultedNullableRecordAttributes(true)
            .withRecords(true)
}

kotlin {
    sourceSets {
        main {
            kotlin.srcDir(tasks[JOOQ_CODEGEN_TASK_NAME])
        }
    }
}

allOpen {
    annotations(
        "jakarta.persistence.Entity",
        "jakarta.persistence.MappedSuperclass",
        "jakarta.persistence.Embeddable",
    )
}

dependencies {
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.data:spring-data-jdbc")
    implementation("org.springframework.modulith:spring-modulith-api")

    implementation(libs.jdbc.micrometer)
    implementation("org.apache.httpcomponents.client5:httpclient5")
    implementation(libs.mariadb.jdbc)
    implementation("com.zaxxer:HikariCP")
    implementation("org.flywaydb:flyway-mysql")
    implementation(libs.jooq)
    implementation(libs.jooq.kotlin)

    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.springframework.modulith:spring-modulith-starter-test")
    testImplementation(projects.wiremockSpringBoot)
    testImplementation(projects.testCerts.springTestCerts)
    testImplementation(libs.commons.compress)
    testImplementation(libs.instancio)

    testImplementation("org.testcontainers:mariadb")
}

tasks.withType<Detekt> {
    dependsOn(JOOQ_CODEGEN_TASK_NAME)
}

application {
    mainClass.set("uk.co.borismorris.babyflux.BabyfluxApplicationKt")
}
