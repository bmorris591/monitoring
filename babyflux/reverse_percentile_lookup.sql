SELECT time,
       age_low,
       (POWER(weight_kg / lms_low.M, lms_low.L) - 1) / (lms_low.L * lms_low.S)     AS z_low,
       weight_age.age_months,
       age_high,
       (POWER(weight_kg / lms_high.M, lms_high.L) - 1) / (lms_high.L * lms_high.S) AS z_high,
       weight_kg
FROM (SELECT time,
             IF(age_months < 0.5, 0, ROUND(age_months - 0.5) + 0.5) AS age_low,
             age_months,
             IF(age_months < 0.5, 0.5, ROUND(age_months) + 0.5)     AS age_high,
             weight_kg
      FROM (SELECT time,
                   TIMESTAMPDIFF(DAY, dob, time) / 30                      AS age_months,
                   IF(weight_english_measure, weight / 0.45359236, weight) AS weight_kg
            FROM growth
                   JOIN baby) measurement) weight_age
       LEFT JOIN (SELECT age_months, L, M, S
                  from us_cdc_growth
                  WHERE sex = 'male'
                    AND measurement = 'weight_kg') lms_low
                 ON (lms_low.age_months = weight_age.age_low)
       LEFT JOIN (SELECT age_months, L, M, S
                  from us_cdc_growth
                  WHERE sex = 'male'
                    AND measurement = 'weight_kg') lms_high
                 ON (lms_high.age_months = weight_age.age_high)
ORDER BY time