import org.jetbrains.kotlin.gradle.dsl.JvmTarget

plugins {
    `kotlin-dsl`
}

java {
    sourceCompatibility = JavaVersion.VERSION_21
    targetCompatibility = JavaVersion.VERSION_21
}

kotlin {
    compilerOptions {
        jvmTarget.set(JvmTarget.JVM_21)
        allWarningsAsErrors = true
        freeCompilerArgs.addAll(
            "-Xjsr305=strict",
            "-opt-in=kotlin.RequiresOptIn",
            "-opt-in=kotlin.io.path.ExperimentalPathApi"
        )
    }
}

dependencies {
    implementation(libs.jib)
    implementation(libs.graalvm)
    implementation(libs.spring.boot.plugin)
    implementation(libs.nebula)

    implementation(libs.testcontainers.mariadb)
    implementation(libs.mariadb.jdbc)
    implementation(libs.flyway.mysql)
    implementation(libs.jooq.codegen)
}
