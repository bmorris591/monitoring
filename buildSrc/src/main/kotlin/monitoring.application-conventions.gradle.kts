import org.springframework.boot.gradle.tasks.bundling.BootBuildImage

plugins {
    application
    `maven-publish`

    id("org.springframework.boot")
    id("com.netflix.nebula.info")
}

application {
    applicationDefaultJvmArgs =
        listOfNotNull(
            "-XX:+DisableAttachMechanism",
            "-Dcom.sun.management.jmxremote",
            "-Dcom.sun.management.jmxremote.port=9000",
            "-Dcom.sun.management.jmxremote.local.only=false",
            "-Dcom.sun.management.jmxremote.authenticate=false",
            "-Dcom.sun.management.jmxremote.ssl=false",
            "-Dcom.sun.management.jmxremote.rmi.port=9000",
            "-Djava.rmi.server.hostname=127.0.0.1",
        )
}

tasks.named<AbstractArchiveTask>("bootJar") {
    archiveClassifier.set("boot")
}

tasks.named<AbstractArchiveTask>("jar") {
    archiveClassifier.set("")
}

dependencies {
    implementation(project(":observability"))
}

publishing {
    publications {
        maybeCreate<MavenPublication>("maven").apply {
            artifact(tasks.distZip)
            artifact(tasks.distTar)
        }
    }
}

val dockerRepo: Provider<String> = project.providers.gradleProperty("dockerRepo")

tasks.named<BootBuildImage>("bootBuildImage") {
    imageName.set(dockerRepo.map { "$it/${project.name}" })
    environment.set(
        mapOf(
            "BPE_DELIM_JAVA_TOOL_OPTIONS" to " ",
            "BPE_APPEND_JAVA_TOOL_OPTIONS" to "-XX:+PrintCommandLineFlags -XX:+DisableExplicitGC",
            "SPRING_PROFILES_ACTIVE" to "prod"
        )
    )
}
