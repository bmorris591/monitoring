package uk.co.borismorris.jooq

import org.flywaydb.core.Flyway
import org.gradle.api.DefaultTask
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.file.DirectoryProperty
import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.Property
import org.gradle.api.tasks.CacheableTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputDirectory
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.PathSensitive
import org.gradle.api.tasks.PathSensitivity.RELATIVE
import org.gradle.api.tasks.TaskAction
import org.gradle.kotlin.dsl.create
import org.gradle.kotlin.dsl.register
import org.jooq.codegen.GenerationTool
import org.jooq.meta.jaxb.Configuration
import org.jooq.meta.jaxb.Database
import org.jooq.meta.jaxb.ForcedType
import org.jooq.meta.jaxb.Generate
import org.jooq.meta.jaxb.Generator
import org.jooq.meta.jaxb.Jdbc
import org.jooq.meta.jaxb.Target
import org.testcontainers.containers.JdbcDatabaseContainer
import org.testcontainers.containers.MariaDBContainerProvider
import org.testcontainers.dockerclient.DockerClientProviderStrategy
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.reflect.KCallable
import kotlin.reflect.full.declaredMembers
import kotlin.reflect.jvm.isAccessible

const val JOOQ_CODEGEN_EXTENSION_NAME = "jooqCodegen"
const val JOOQ_CODEGEN_TASK_NAME = "jooqCodegen"

interface JooqCodegenExtension {
    val migrations: DirectoryProperty

    val outputDirectory: DirectoryProperty

    val packageName: Property<String>

    val forcedTypes: ListProperty<ForcedType>

    val excludesRegex: Property<String>

    val generate: Property<Generate>
}

abstract class JooqCodegenPlugin : Plugin<Project> {
    override fun apply(project: Project) {
        project.applyInternal()
    }

    private fun Project.applyInternal() {
        val jooqCodegenExtension = extensions.create<JooqCodegenExtension>(JOOQ_CODEGEN_EXTENSION_NAME)
        jooqCodegenExtension.generate.convention(
            Generate()
                .withImplicitJoinPathsAsKotlinProperties(true)
                .withKotlinSetterJvmNameAnnotationsOnIsPrefix(true)
                .withRecords(false)
        )
        tasks.register<JooqCodegenTask>(JOOQ_CODEGEN_TASK_NAME) {
            migrations.set(jooqCodegenExtension.migrations)
            outputDirectory.set(jooqCodegenExtension.outputDirectory)
            packageName.set(jooqCodegenExtension.packageName)
            forcedTypes.set(jooqCodegenExtension.forcedTypes)
            excludesRegex.set(jooqCodegenExtension.excludesRegex)
            generate.set(jooqCodegenExtension.generate)
        }
    }
}

@CacheableTask
abstract class JooqCodegenTask : DefaultTask() {

    @get:InputDirectory
    @get:PathSensitive(RELATIVE)
    abstract val migrations: DirectoryProperty

    @get:Input
    abstract val packageName: Property<String>

    @get:Input
    abstract val forcedTypes: ListProperty<ForcedType>

    @get:Input
    @get:Optional
    abstract val excludesRegex: Property<String>

    @get:Input
    abstract val generate: Property<Generate>

    @get:OutputDirectory
    abstract val outputDirectory: DirectoryProperty

    @TaskAction
    fun jooqCodegen() {
        testcontainersDisableFailFast()

        withMariadbContainer { mariadbContainer ->
            val flyway = Flyway.configure()
                .dataSource(mariadbContainer.jdbcUrl, mariadbContainer.username, mariadbContainer.password)
                .locations("filesystem:${migrations.get().asFile.absolutePath}")
                .load()
            flyway.migrate()

            GenerationTool.generate(
                Configuration()
                    .withJdbc(
                        Jdbc()
                            .withUrl(mariadbContainer.jdbcUrl)
                            .withUser(mariadbContainer.username)
                            .withPassword(mariadbContainer.password)
                    )
                    .withGenerator(
                        Generator()
                            .withName("org.jooq.codegen.KotlinGenerator")
                            .withDatabase(
                                Database()
                                    .withInputSchema(mariadbContainer.databaseName)
                                    .withOutputSchemaToDefault(true)
                                    .withExcludes(excludesRegex.map { "${flyway.configuration.table} | $it" }
                                        .getOrElse(flyway.configuration.table))
                                    .withForcedTypes(forcedTypes.get())
                            )
                            .withGenerate(generate.get())
                            .withTarget(
                                Target()
                                    .withPackageName(packageName.get())
                                    .withDirectory(outputDirectory.asFile.get().absolutePath)
                            )
                    )
            )
        }
    }

    private fun testcontainersDisableFailFast() {
        val failFastAlways = DockerClientProviderStrategy::class.declaredMembers
            .single { it.name == "FAIL_FAST_ALWAYS" }
            .apply { isAccessible = true }
            .let {
                @Suppress("UNCHECKED_CAST")
                it as KCallable<AtomicBoolean>
            }
            .call()
        failFastAlways.set(false)
    }

    private fun withMariadbContainer(withContainer: (JdbcDatabaseContainer<*>) -> Unit) {
        val mariadbContainer: JdbcDatabaseContainer<*> = MariaDBContainerProvider().newInstance("latest")
            .withUsername("migrate")
            .withPassword("migrate")
            .withDatabaseName("migrate")
        mariadbContainer.start()
        mariadbContainer.use(withContainer)
    }
}
