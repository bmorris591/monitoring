import com.google.cloud.tools.jib.gradle.JibTask

plugins {
    application
    id("com.google.cloud.tools.jib")
}

val applicationMainClass = project.extensions.findByType<JavaApplication>()!!.mainClass
val dockerRepo: Provider<String> = project.providers.gradleProperty("dockerRepo")

jib {
    from {
        setImage(dockerRepo.map { r -> "$r/graalvm-container:${project.version}" }.orElse("docker://graalvm-container:latest"))
        platforms {
            platform {
                architecture = "amd64"
                os = "linux"
            }
            platform {
                architecture = "arm64"
                os = "linux"
            }
        }
    }
    to {
        setImage(dockerRepo.map { "$it/${project.name}" })

    }
    container {
        setMainClass(applicationMainClass)
        environment = mapOf("SPRING_PROFILES_ACTIVE" to "prod")
        jvmFlags = listOfNotNull("-XX:+PrintCommandLineFlags", "-XX:+DisableExplicitGC")
        creationTime.set("USE_CURRENT_TIMESTAMP")
        labels = mapOf(
            "org.opencontainers.image.title " to project.name
        )
    }
}

// workaround https://github.com/GoogleContainerTools/jib/issues/3132
tasks.withType<JibTask>().configureEach {
    notCompatibleWithConfigurationCache("because https://github.com/GoogleContainerTools/jib/issues/3132")
}
