rootProject.name = "monitoring"

enableFeaturePreview("TYPESAFE_PROJECT_ACCESSORS")
enableFeaturePreview("STABLE_CONFIGURATION_CACHE")

pluginManagement {
    repositories {
        maven("https://repo.spring.io/milestone")
        mavenCentral()
        gradlePluginPortal()
    }
    resolutionStrategy {
        eachPlugin {
            if (requested.id.id == "org.springframework.boot") {
                useModule("org.springframework.boot:spring-boot-gradle-plugin:${requested.version}")
            }
        }
    }
}

plugins {
    id("com.gradle.develocity") version ("3.19.2")
}

develocity {
    buildScan {
        termsOfUseUrl = "https://gradle.com/help/legal-terms-of-use"
        termsOfUseAgree = "yes"
        uploadInBackground = false
        if (!System.getenv("CI").isNullOrEmpty()) {
            publishing.onlyIf { true }
            tag("CI")
            link("build", System.getenv("CI_JOB_URL"))
        }
    }
}

dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        mavenCentral()
        maven("https://repo.spring.io/milestone")
    }
}

include("air-quality-monitor:air-quality-monitor")
include("air-quality-monitor:device-api")
include("air-quality-monitor:device-discovery")
include("air-quality-monitor:device-locator")
include("air-quality-monitor:dns-device-locator")
include("air-quality-monitor:dyson-mqtt-client")
include("air-quality-monitor:kmqtt-mqtt-client")
include("air-quality-monitor:local-credentials-decoder")
include("air-quality-monitor:mock-broker")
include("air-quality-monitor:mock-dyson-fan")
include("air-quality-monitor:static-discovery")
include("babyflux")
include("netatmo-influx")
include("observability")
include("logging")
include("test-certs:test-certs")
include("test-certs:spring-test-certs")
include("test-certs:test-certs-junit-extension")
include("wiremock-spring-boot")
include("restclient-influx-client")
include("restclient-influx-client-testcontainers")
include("speedtest-influx")
include("spring-oauth2")
include("tado-influx")
include("volvooncall-mysql")
include("cloud-config-server")
