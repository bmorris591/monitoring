package uk.co.borismorris.monitoring.observability.otel

import io.opentelemetry.exporter.otlp.http.metrics.OtlpHttpMetricExporter
import io.opentelemetry.sdk.metrics.SdkMeterProvider
import io.opentelemetry.sdk.metrics.export.MetricExporter
import io.opentelemetry.sdk.metrics.export.MetricReader
import io.opentelemetry.sdk.metrics.export.PeriodicMetricReader
import io.opentelemetry.sdk.resources.Resource
import org.springframework.boot.actuate.autoconfigure.metrics.export.otlp.OtlpMetricsConnectionDetails
import org.springframework.boot.actuate.autoconfigure.opentelemetry.OpenTelemetryAutoConfiguration
import org.springframework.boot.autoconfigure.AutoConfiguration
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.context.annotation.Profile
import org.springframework.context.annotation.PropertySource

@Configuration(proxyBeanMethods = false)
@PropertySource("classpath:uk/co/borismorris/monitoring/observability/otel/tracing.properties")
@EnableConfigurationProperties(OtlpMeterRegistryProperties::class)
@Import(ConnectionAutoConfiguration::class, ExporterAutoConfiguration::class)
class OtelAutoConfiguration

@Configuration(proxyBeanMethods = false)
class ConnectionAutoConfiguration(private val otlpProperties: OtlpMeterRegistryProperties) {
    @Bean
    @ConditionalOnMissingBean
    fun otlpMetricsConnectionDetails(): OtlpMetricsConnectionDetails = PropertiesOtlpMetricsConnectionDetails(otlpProperties)
}

@Configuration(proxyBeanMethods = false)
@AutoConfiguration(
    before = [OpenTelemetryAutoConfiguration::class],
    after = [OtelResources::class],
)
class ExporterAutoConfiguration(private val otlpProperties: OtlpMeterRegistryProperties) {
    @Bean
    fun otlpExporter(connectionDetails: OtlpMetricsConnectionDetails): OtlpHttpMetricExporter = OtlpHttpMetricExporter.builder()
        .setEndpoint(connectionDetails.url)
        .setTimeout(otlpProperties.connectTimeout)
        .also { builder ->
            otlpProperties.headers?.forEach(builder::addHeader)
        }
        .build()

    @Bean
    fun periodicMetricReader(exporter: MetricExporter): PeriodicMetricReader = PeriodicMetricReader.builder(exporter)
        .setInterval(otlpProperties.step)
        .build()

    @Bean
    fun sdkMeterProvider(meterReader: MetricReader, resource: Resource): SdkMeterProvider = SdkMeterProvider.builder()
        .setResource(resource)
        .registerMetricReader(meterReader)
        .build()
}

@Configuration(proxyBeanMethods = false)
@Profile("test", "int-test")
@PropertySource("classpath:uk/co/borismorris/monitoring/observability/otel/test.properties")
class OtelTestAutoconfiguration

data class PropertiesOtlpMetricsConnectionDetails(private val url: String) : OtlpMetricsConnectionDetails {
    constructor(properties: OtlpMeterRegistryProperties) : this(properties.url)

    override fun getUrl() = url
}
