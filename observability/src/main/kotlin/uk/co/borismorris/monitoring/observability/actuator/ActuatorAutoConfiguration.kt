package uk.co.borismorris.monitoring.observability.actuator

import org.springframework.boot.autoconfigure.AutoConfigureBefore
import org.springframework.boot.autoconfigure.availability.ApplicationAvailabilityAutoConfiguration
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.PropertySource

@Configuration(proxyBeanMethods = false)
@AutoConfigureBefore(ApplicationAvailabilityAutoConfiguration::class)
@PropertySource("classpath:uk/co/borismorris/monitoring/observability/actuator/actuator.properties")
class ActuatorAutoConfiguration
