package uk.co.borismorris.monitoring.observability.otel

import org.springframework.boot.actuate.autoconfigure.metrics.export.properties.StepRegistryProperties
import org.springframework.boot.context.properties.ConfigurationProperties
import java.util.concurrent.TimeUnit

@ConfigurationProperties(prefix = "management.otlp.metrics.export")
class OtlpMeterRegistryProperties : StepRegistryProperties() {
    var url = "http://localhost:4318/v1/metrics"

    var headers: Map<String, String>? = null

    var baseTimeUnit = TimeUnit.MILLISECONDS
}
