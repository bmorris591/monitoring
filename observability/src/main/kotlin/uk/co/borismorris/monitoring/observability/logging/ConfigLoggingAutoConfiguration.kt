package uk.co.borismorris.monitoring.observability.logging

import org.springframework.beans.factory.ListableBeanFactory
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.event.ApplicationStartedEvent
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.event.EventListener
import uk.co.borismorris.monitoring.logging.KotlinLogging
import java.util.*
import kotlin.reflect.KClass

private val logger = KotlinLogging.logger {}

@Configuration(proxyBeanMethods = false)
class ConfigLoggingAutoConfiguration {
    @Bean
    fun logManifestInfoOnStartup() = LogManifestInfoOnStartup()
}

class LogConfigOnStartup(private val context: ListableBeanFactory, private vararg val beans: KClass<*>) {
    @EventListener(ApplicationStartedEvent::class)
    fun onStartup() {
        beans.asSequence()
            .flatMap { context.getBeansOfType(it.java).asSequence() }
            .forEach { (name, bean) ->
                logger.atInfo().setMessage("Configuration for {}").addArgument(name).addKeyValue("bean", bean).log()
            }
    }
}

class LogManifestInfoOnStartup {
    @EventListener
    fun onStartup(event: ApplicationStartedEvent) {
        event.applicationContext.getBeansWithAnnotation(SpringBootApplication::class.java)
            .asSequence()
            .map { it.value::class.java.classLoader.getResource("META-INF/nebula-info.properties") }
            .filterNotNull()
            .map {
                Properties().apply {
                    it.openStream().use { load(it) }
                }
            }.forEach {
                logger.atInfo().setMessage("Git commit information")
                    .addKeyValue("branch") { it["Branch"] }
                    .addKeyValue("commitId") { it["Full-Change"] }
                    .log()
                logger.atInfo().setMessage("Version information")
                    .addKeyValue("implementationTitle") { it["Implementation-Title"] }
                    .log()
            }
    }
}
