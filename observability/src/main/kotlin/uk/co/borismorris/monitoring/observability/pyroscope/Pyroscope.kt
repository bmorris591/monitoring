package uk.co.borismorris.monitoring.observability.pyroscope

import io.opentelemetry.sdk.trace.SpanProcessor
import io.otel.pyroscope.PyroscopeOtelConfiguration
import io.otel.pyroscope.PyroscopeOtelSpanProcessor
import io.otel.pyroscope.shadow.javaagent.PyroscopeAgent
import io.otel.pyroscope.shadow.javaagent.PyroscopeAgent.Options
import io.otel.pyroscope.shadow.javaagent.api.ConfigurationProvider
import io.otel.pyroscope.shadow.javaagent.api.Logger.Level.DEBUG
import io.otel.pyroscope.shadow.javaagent.api.Logger.Level.ERROR
import io.otel.pyroscope.shadow.javaagent.api.Logger.Level.INFO
import io.otel.pyroscope.shadow.javaagent.api.Logger.Level.WARN
import io.otel.pyroscope.shadow.labels.io.pyroscope.PyroscopeAsyncProfiler
import jakarta.annotation.PostConstruct
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.ImportRuntimeHints
import org.springframework.context.annotation.Lazy
import org.springframework.context.annotation.Profile
import org.springframework.context.annotation.PropertySource
import org.springframework.core.env.Environment
import uk.co.borismorris.monitoring.logging.KotlinLogging
import java.util.*
import io.otel.pyroscope.shadow.javaagent.config.Config as PyroscopeAgentConfig

private val logger = KotlinLogging.logger {}

@Configuration(proxyBeanMethods = false)
@PropertySource("classpath:uk/co/borismorris/monitoring/observability/pyroscope/pyroscope.properties")
@ConditionalOnProperty(prefix = "pyroscope", name = ["enabled"], matchIfMissing = true)
@ImportRuntimeHints(PyroscopeRuntimeHints::class)
class PyroscopeConfig {

    @Bean
    fun springConfigProvider(environment: Environment) = SpringConfigProvider(environment)

    @Bean
    internal fun pyroscopeAgentConfig(configurationProvider: ConfigurationProvider) = PyroscopeAgentConfig.build(configurationProvider)

    @Bean
    @Lazy(false)
    fun pyroscope(pyroscopeConfig: PyroscopeAgentConfig) = Pyroscope(pyroscopeConfig)

    @Bean
    internal fun pyroscopeOtelConfig(pyroscopeConfig: PyroscopeAgentConfig) = PyroscopeOtelConfiguration.Builder()
        .setAppName(pyroscopeConfig.applicationName)
        .setPyroscopeEndpoint(pyroscopeConfig.serverAddress)
        .setAddSpanName(true)
        .setRootSpanOnly(true)
        .setAddProfileURL(true)
        .setAddProfileBaselineURLs(true)
        .setOptimisticTimestamps(true)
        .build()

    @Bean
    fun pyroscopeSpanProcessor(pyroscopeConfig: PyroscopeOtelConfiguration): SpanProcessor {
        val profiler = PyroscopeAsyncProfiler.getAsyncProfiler()
        logger.atInfo().addKeyValue("version", profiler.version).log("Initialised Pyroscope Async Profiler")
        return PyroscopeOtelSpanProcessor(pyroscopeConfig, null)
    }
}

@Configuration(proxyBeanMethods = false)
@Profile("test", "int-test")
@PropertySource("classpath:uk/co/borismorris/monitoring/observability/pyroscope/test.properties")
class PyroscopeTestConfig

class Pyroscope(private val config: PyroscopeAgentConfig) {

    @PostConstruct
    @Suppress("SpreadOperator")
    fun init() {
        logger.atInfo()
            .setMessage("Starting Pyroscope")
            .addKeyValue("config", config)
            .log()

        PyroscopeAgent.start(
            Options.Builder(config)
                .setLogger { l, msg, args ->
                    when (l) {
                        DEBUG -> logger.atDebug().log { String.format(msg, *args) }
                        INFO -> logger.atInfo().log { String.format(msg, *args) }
                        WARN -> logger.atWarn().log { String.format(msg, *args) }
                        ERROR -> logger.atError().log { String.format(msg, *args) }
                        null -> logger.atInfo().log { String.format(msg, *args) }
                    }
                }
                .build(),
        )
    }
}

class SpringConfigProvider(private val config: Environment) : ConfigurationProvider {
    override fun get(key: String): String? {
        config.getProperty(key)?.let { return it }
        val lowerKey = key.lowercase(Locale.ROOT)
        val trimmedKey = lowerKey.substring(10)
        val kebabKey = trimmedKey.replace('_', '-')
        return config.getProperty("pyroscope.$kebabKey")
    }
}
