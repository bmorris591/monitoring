package uk.co.borismorris.monitoring.observability.otel

import io.micrometer.core.instrument.Clock
import io.micrometer.core.instrument.MeterRegistry
import io.opentelemetry.api.OpenTelemetry
import io.opentelemetry.instrumentation.micrometer.v1_5.OpenTelemetryMeterRegistry
import org.springframework.boot.actuate.autoconfigure.metrics.CompositeMeterRegistryAutoConfiguration
import org.springframework.boot.actuate.autoconfigure.metrics.MetricsAutoConfiguration
import org.springframework.boot.actuate.autoconfigure.opentelemetry.OpenTelemetryAutoConfiguration
import org.springframework.boot.autoconfigure.AutoConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary

@Configuration(proxyBeanMethods = false)
@AutoConfiguration(
    after = [OpenTelemetryAutoConfiguration::class, MetricsAutoConfiguration::class],
    before = [CompositeMeterRegistryAutoConfiguration::class],
)
class MicrometerOtelAutoConfiguration(private val otlpProperties: OtlpMeterRegistryProperties) {

    @Bean
    @Primary
    fun opentelemetryMeterRegistry(openTelemetry: OpenTelemetry, clock: Clock): MeterRegistry = OpenTelemetryMeterRegistry.builder(openTelemetry)
        .setClock(clock)
        .setBaseTimeUnit(otlpProperties.baseTimeUnit)
        .setMicrometerHistogramGaugesEnabled(true)
        .build()
}
