plugins {
    `java-library`
}

group = "uk.co.borismorris.monitoring.observability"

dependencies {
    api("io.micrometer:micrometer-core")
    api("io.micrometer:micrometer-tracing")

    implementation("org.springframework.boot:spring-boot-starter-aop")
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("io.micrometer:micrometer-tracing-bridge-otel")
    implementation("io.opentelemetry:opentelemetry-exporter-otlp")
    implementation("io.opentelemetry:opentelemetry-sdk-extension-autoconfigure")
    implementation(libs.otel.micrometer)
    runtimeOnly(libs.otel.resources)

    runtimeOnly(libs.logback.logstash)

    implementation(libs.pyroscope.otel)
}
