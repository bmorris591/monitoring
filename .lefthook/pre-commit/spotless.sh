#!/usr/bin/env -S bash -e

./gradlew --console=plain --warning-mode none -q spotlessCheck
