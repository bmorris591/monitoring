plugins {
    `java-library`
}

group = "uk.co.borismorris.monitoring.logging"

dependencies {
    api("org.slf4j:slf4j-api")
}
