package uk.co.borismorris.monitoring.logging

/**
 * See https://github.com/oshai/kotlin-logging/blob/master/src/javaMain/kotlin/io/github/oshai/kotlinlogging/internal/KLoggerNameResolver.kt
 */
object LoggerNameResolver {
    internal fun name(func: () -> Unit): String {
        val name = func.javaClass.name
        val slicedName =
            when {
                name.contains("Kt$") -> name.substringBefore("Kt$")
                name.contains("$") -> name.substringBefore("$")
                else -> name
            }
        return slicedName
    }
}
