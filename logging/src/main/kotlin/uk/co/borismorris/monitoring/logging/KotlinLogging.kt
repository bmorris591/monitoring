package uk.co.borismorris.monitoring.logging

import org.slf4j.Logger
import org.slf4j.LoggerFactory

object KotlinLogging {
    fun logger(func: () -> Unit): Logger = logger(LoggerNameResolver.name(func))

    fun logger(name: String): Logger = LoggerFactory.getLogger(name)
}
