package uk.co.borismorris.volvooncall.jooq.type

import org.geolatte.geom.G2D
import org.geolatte.geom.Geometry
import org.geolatte.geom.Point
import org.geolatte.geom.Polygon
import org.geolatte.geom.codec.Wkt
import org.geolatte.geom.codec.WktDecoder
import org.jooq.Field
import org.jooq.exception.DataTypeException
import org.jooq.impl.AbstractConverter
import org.jooq.impl.DSL
import org.jooq.impl.SQLDataType
import uk.co.borismorris.volvooncall.jooq.bind.G2DGeometry
import uk.co.borismorris.volvooncall.jooq.bind.G2DPoint
import uk.co.borismorris.volvooncall.jooq.bind.G2DPolygon
import uk.co.borismorris.volvooncall.jooq.coordinateReferenceSystem

private fun <G : G2DGeometry<*>> Field<in G>.toWkt() = DSL.function("ST_AsWKT", SQLDataType.CLOB, this)

@JvmName("asWktPoint")
fun Field<G2DPoint?>.asWkt() = toWkt().convert(G2DGeometryConverter.from { G2DPoint(it as Point<G2D>) })

@JvmName("asWktPolygon")
fun Field<G2DPolygon?>.asWkt() = toWkt().convert(G2DGeometryConverter.from { G2DPolygon(it as Polygon<G2D>) })

class G2DGeometryConverter<G : G2DGeometry<*>>(toType: Class<G>, private val wrap: (Geometry<G2D>) -> G) : AbstractConverter<String, G>(String::class.java, toType) {
    companion object {
        inline fun <reified G : G2DGeometry<*>> from(noinline wrap: (Geometry<G2D>) -> G) = G2DGeometryConverter(G::class.java, wrap)
    }

    private val decoder = Wkt.newDecoder(Wkt.Dialect.MYSQL_WKT)

    override fun from(databaseObject: String?) = databaseObject?.let { decoder.decodeWkt(it) }

    override fun to(userObject: G?): String = throw DataTypeException("Conversion function not implemented")

    private fun WktDecoder.decodeWkt(wkt: String?) = synchronized(this) {
        val geometry = decode(wkt, coordinateReferenceSystem)
        wrap(geometry)
    }
}
