@file:Suppress("TooManyFunctions")

package uk.co.borismorris.volvooncall

import io.micrometer.observation.ObservationRegistry
import org.jooq.DSLContext
import org.springframework.beans.factory.ListableBeanFactory
import org.springframework.beans.factory.ObjectProvider
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.jooq.DefaultConfigurationCustomizer
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientProvider
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientProviderBuilder
import org.springframework.security.oauth2.client.web.client.OAuth2ClientHttpRequestInterceptor
import org.springframework.transaction.annotation.EnableTransactionManagement
import org.springframework.web.client.RestClient
import uk.co.borismorris.monitoring.oauth2.Oauth2RestClientCustomizer
import uk.co.borismorris.monitoring.oauth2.OauthCachePath
import uk.co.borismorris.monitoring.oauth2.restClient
import uk.co.borismorris.monitoring.observability.logging.LogConfigOnStartup
import uk.co.borismorris.volvooncall.config.VocConfig
import uk.co.borismorris.volvooncall.dao.TripRepository
import uk.co.borismorris.volvooncall.dao.TripWaypointRepository
import uk.co.borismorris.volvooncall.dao.VehicleRepository
import uk.co.borismorris.volvooncall.dao.VehicleStatusRepository
import uk.co.borismorris.volvooncall.service.DaoTripService
import uk.co.borismorris.volvooncall.service.DaoVehicleService
import uk.co.borismorris.volvooncall.service.TripService
import uk.co.borismorris.volvooncall.service.VehicleService
import uk.co.borismorris.volvooncall.vocclient.RestclientVocClient
import uk.co.borismorris.volvooncall.worker.MonitorVolvoOnCall
import java.nio.file.Path

@SpringBootApplication(proxyBeanMethods = false)
@EnableTransactionManagement
class VolvoOnCallApplication {
    companion object {
        init {
            SecurityContextHolder.setStrategyName(SecurityContextHolder.MODE_GLOBAL)
        }
    }

    @ConfigurationProperties(prefix = "voc")
    @Bean
    fun vocConfig() = VocConfig()

    @Bean
    @OauthCachePath
    fun cachePath(@Value("\${voc.cache-path}") path: Path) = path

    @Bean
    fun authorisedClientProvider(
        restClientBuilder: RestClient.Builder,
        customizers: ObjectProvider<Oauth2RestClientCustomizer>,
    ): OAuth2AuthorizedClientProvider {
        customizers.forEach {
            it.customize(restClientBuilder)
        }
        return OAuth2AuthorizedClientProviderBuilder.builder()
            .refreshToken { it.restClient(restClientBuilder.build()) }
            .build()
    }

    @Bean
    fun vocClient(
        config: VocConfig,
        restclientBuilder: RestClient.Builder,
        authFilter: OAuth2ClientHttpRequestInterceptor,
    ) = RestclientVocClient(config, restclientBuilder.requestInterceptor(authFilter))

    @Bean
    fun jooqConfigurationCustomizer(@Value("\${spring.jooq.logging:false}") logging: Boolean) = DefaultConfigurationCustomizer {
        it.settings().apply {
            isRenderGroupConcatMaxLenSessionVariable = false
            isExecuteLogging = logging
            isDiagnosticsLogging = logging
        }
    }

    @Bean
    fun vehicleRepo(context: DSLContext) = VehicleRepository(context)

    @Bean
    fun vehicleStatsRepo(context: DSLContext) = VehicleStatusRepository(context)

    @Bean
    fun tripRepo(context: DSLContext) = TripRepository(context)

    @Bean
    fun tripWaypointRepo(ccontext: DSLContext) = TripWaypointRepository(ccontext)

    @Bean
    fun vehicleService(
        vocClient: RestclientVocClient,
        vehicleRepository: VehicleRepository,
        vehicleStatusRepository: VehicleStatusRepository,
    ) = DaoVehicleService(vocClient, vehicleRepository, vehicleStatusRepository)

    @Bean
    fun tripService(
        vocClient: RestclientVocClient,
        tripRepository: TripRepository,
        tripWaypointRepository: TripWaypointRepository,
    ) = DaoTripService(vocClient, tripRepository, tripWaypointRepository)

    @Bean
    @Profile("!test")
    fun monitorVolvoOnCall(
        observationRegistry: ObservationRegistry,
        vocClient: RestclientVocClient,
        vehicleService: VehicleService,
        tripService: TripService,
    ) = MonitorVolvoOnCall(observationRegistry, vocClient, vehicleService, tripService)

    @Bean
    fun logConfigOnStartup(context: ListableBeanFactory) = LogConfigOnStartup(context, VocConfig::class)

    @Configuration(proxyBeanMethods = false)
    @Profile("!test")
    @EnableScheduling
    class Scheduling
}

@Suppress("SpreadOperator")
fun main(args: Array<String>) {
    runApplication<VolvoOnCallApplication>(*args)
}
