package uk.co.borismorris.volvooncall.config

import java.net.URL

class VocConfig {
    lateinit var baseUrl: URL

    override fun toString() = "VocConfig(baseUrl=$baseUrl)"
}
