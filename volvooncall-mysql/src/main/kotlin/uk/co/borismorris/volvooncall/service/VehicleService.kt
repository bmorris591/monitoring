package uk.co.borismorris.volvooncall.service

import org.slf4j.MDC
import org.springframework.transaction.annotation.Transactional
import uk.co.borismorris.monitoring.logging.KotlinLogging
import uk.co.borismorris.volvooncall.dao.VehicleRepository
import uk.co.borismorris.volvooncall.dao.VehicleStatusRepository
import uk.co.borismorris.volvooncall.vocclient.CustomerAccount
import uk.co.borismorris.volvooncall.vocclient.RestclientVocClient
import uk.co.borismorris.volvooncall.vocclient.Vehicle
import java.time.Instant

private val logger = KotlinLogging.logger {}

interface VehicleService {
    fun vehicles(account: CustomerAccount): Sequence<Vehicle>

    fun saveStatus(vehicle: Vehicle): Int
}

@Transactional
class DaoVehicleService(
    private val vocClient: RestclientVocClient,
    private val vehicleRepository: VehicleRepository,
    private val vehicleStatusRepository: VehicleStatusRepository,
) : VehicleService {
    override fun vehicles(account: CustomerAccount) = vocClient.vehicles(account).asSequence().onEach { vehicle ->
        MDC.putCloseable("vehicle", vehicle.attributes.registrationNumber).use {
            if (!vehicleRepository.exists(vehicle.id)) {
                logger.info("Saving Vehicle")
                val updated = vehicleRepository.save(vehicle)
                logger.atInfo().setMessage("Saved vehicle").addKeyValue("updatedRows", updated).log()
            }
        }
    }

    override fun saveStatus(vehicle: Vehicle) = vocClient.vehicleStatus(vehicle).let { status ->
        MDC.putCloseable("vehicle", vehicle.attributes.registrationNumber).use {
            logger.info("Saving Vehicle status")
            val (id, updated) = vehicleStatusRepository.save(vehicle, status, Instant.now())
            logger.atInfo().setMessage("Saved vehicle status").addKeyValue("updatedRows", updated).log()
            id
        }
    }
}
