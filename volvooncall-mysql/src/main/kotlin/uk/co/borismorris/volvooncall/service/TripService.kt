package uk.co.borismorris.volvooncall.service

import org.slf4j.MDC
import org.springframework.transaction.annotation.Transactional
import uk.co.borismorris.monitoring.logging.KotlinLogging
import uk.co.borismorris.volvooncall.dao.TripRepository
import uk.co.borismorris.volvooncall.dao.TripWaypointRepository
import uk.co.borismorris.volvooncall.vocclient.RestclientVocClient
import uk.co.borismorris.volvooncall.vocclient.Trip
import uk.co.borismorris.volvooncall.vocclient.Vehicle

private val logger = KotlinLogging.logger {}

interface TripService {
    fun saveTrips(vehicle: Vehicle): Long
}

@Transactional
class DaoTripService(
    private val vocClient: RestclientVocClient,
    private val tripRepository: TripRepository,
    private val waypointRepository: TripWaypointRepository,
) : TripService {
    override fun saveTrips(vehicle: Vehicle) = vocClient.vehicleTrips(vehicle).trips.sumOf { trip ->
        MDC.putCloseable("trip", trip.id.toString()).use {
            if (!tripRepository.exists(trip.id)) {
                trip.saveTrip(vehicle)
            } else {
                0L
            }
        }
    }

    private fun Trip.saveTrip(vehicle: Vehicle): Long {
        logger.atInfo().setMessage("Saving trip").addKeyValue("trip", this).log()
        val updatedTrip = tripRepository.save(vehicle, this)
        logger.atInfo().setMessage("Saved trip").addKeyValue("updatedRows", updatedTrip).log()
        val updatedRoute = saveRoute()
        logger.atInfo().setMessage("Saved route").addKeyValue("updatedRows", updatedRoute).log()
        return updatedTrip + updatedRoute
    }

    private fun Trip.saveRoute(): Long = waypointRepository.save(this, vocClient.tripRoute(this))
}
