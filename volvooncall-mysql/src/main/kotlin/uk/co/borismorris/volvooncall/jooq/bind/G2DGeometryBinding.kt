package uk.co.borismorris.volvooncall.jooq.bind

import org.geolatte.geom.ByteBuffer
import org.geolatte.geom.ByteOrder
import org.geolatte.geom.G2D
import org.geolatte.geom.Geometry
import org.geolatte.geom.codec.Wkb
import org.geolatte.geom.codec.WkbDecoder
import org.geolatte.geom.codec.WkbEncoder
import org.jooq.BindingGetResultSetContext
import org.jooq.BindingGetStatementContext
import org.jooq.BindingSQLContext
import org.jooq.BindingSetStatementContext
import org.jooq.Converter
import org.jooq.impl.AbstractBinding
import org.jooq.impl.AbstractConverter
import org.jooq.impl.DSL
import uk.co.borismorris.monitoring.logging.KotlinLogging
import uk.co.borismorris.volvooncall.jooq.coordinateReferenceSystem
import java.sql.Types

private val logger = KotlinLogging.logger {}

open class G2DGeometryBinding<G : G2DGeometry<*>>(toType: Class<G>, wrap: (Geometry<G2D>) -> G) : AbstractBinding<Any, G>() {

    private val converter = G2DGeometryConverter(toType, wrap)

    override fun converter(): Converter<Any, G> = converter

    override fun get(ctx: BindingGetStatementContext<G>) {
        ctx.convert(converter).value(ctx.statement().getBytes(ctx.index()))
    }

    override fun get(ctx: BindingGetResultSetContext<G>) {
        ctx.convert(converter).value(ctx.resultSet().getBytes(ctx.index()))
    }

    override fun set(ctx: BindingSetStatementContext<G>) {
        val value = ctx.value()
        if (false != ctx.settings().isExecuteLogging) {
            logger.atTrace().setMessage("Binding geometry variable")
                .addKeyValue("index", ctx.index())
                .addKeyValue("value", value)
                .log()
        }
        when (val converted = converter.to(value)) {
            null -> ctx.statement().setNull(ctx.index(), Types.BINARY)
            else -> ctx.statement().setBytes(ctx.index(), converted)
        }
    }

    override fun sqlInline(ctx: BindingSQLContext<G>) {
        val inline = converter.to(ctx.value())
        ctx.render().visit(DSL.inline(inline))
    }
}

internal class G2DGeometryConverter<G : G2DGeometry<*>>(toType: Class<G>, private val wrap: (Geometry<G2D>) -> G) : AbstractConverter<Any, G>(Any::class.java, toType) {
    private val encoder = Wkb.newEncoder(Wkb.Dialect.MYSQL_WKB)
    private val decoder = Wkb.newDecoder(Wkb.Dialect.MYSQL_WKB)

    override fun from(databaseObject: Any?) = databaseObject?.let { decoder.decode(it as ByteArray) }

    override fun to(userObject: G?): ByteArray? = userObject?.let { encoder.encode(it) }

    private fun WkbDecoder.decode(bytes: ByteArray?) = synchronized(this) {
        val geometry = decode(ByteBuffer.from(bytes), coordinateReferenceSystem)
        wrap(geometry)
    }

    private fun WkbEncoder.encode(box: G) = synchronized(this) {
        encode(box.geometry, ByteOrder.NDR).toByteArray()
    }
}
