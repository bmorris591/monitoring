/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES UTF8mb4 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = 'UTC' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;

CREATE TABLE `vehicle`
(
    `vehicle_id`                        CHAR(20) NOT NULL,
    `model_year`                        YEAR(4)  NOT NULL,
    `vehicle_type`                      TEXT     NOT NULL,
    `registration_number`               CHAR(10) NOT NULL,
    `car_locator_supported`             BIT(1)   NOT NULL DEFAULT 0,
    `honk_and_blink_supported`          BIT(1)   NOT NULL DEFAULT 0,
    `remote_heater_supported`           BIT(1)   NOT NULL DEFAULT 0,
    `unlock_supported`                  BIT(1)   NOT NULL DEFAULT 0,
    `lock_supported`                    BIT(1)   NOT NULL DEFAULT 0,
    `journal_log_supported`             BIT(1)   NOT NULL DEFAULT 0,
    `assistance_call_supported`         BIT(1)   NOT NULL DEFAULT 0,
    `high_voltage_battery_supported`    BIT(1)   NOT NULL DEFAULT 0,
    `preclimatization_supported`        BIT(1)   NOT NULL DEFAULT 0,
    `override_delay_charging_supported` BIT(1)   NOT NULL DEFAULT 0,
    `engine_start_supported`            BIT(1)   NOT NULL DEFAULT 0,
    `status_parked_indoor_supported`    BIT(1)   NOT NULL DEFAULT 0,
    PRIMARY KEY (`vehicle_id`),
    UNIQUE KEY `vehicle_unique_license_plate` (`registration_number`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

CREATE TABLE `vehicle_status`
(
    `vehicle_status_id`                     INT UNSIGNED    NOT NULL AUTO_INCREMENT,
    `vehicle_status_timestamp`              DATETIME(6)     NOT NULL DEFAULT 0,
    `vehicle_id`                            CHAR(20)        NOT NULL,
    `average_fuel_consumption`              DOUBLE          NOT NULL DEFAULT 0,
    `average_fuel_consumption_timestamp`    DATETIME        NOT NULL DEFAULT 0,
    `average_speed`                         DOUBLE          NOT NULL DEFAULT 0,
    `average_speed_timestamp`               DATETIME        NOT NULL DEFAULT 0,
    `brake_fluid`                           TEXT                     DEFAULT NULL,
    `brake_fluid_timestamp`                 DATETIME        NOT NULL DEFAULT 0,
    `car_locked`                            BIT(1)          NOT NULL DEFAULT 0,
    `car_locked_timestamp`                  DATETIME        NOT NULL DEFAULT 0,
    `connection_status`                     TEXT                     DEFAULT NULL,
    `connection_status_timestamp`           DATETIME        NOT NULL DEFAULT 0,
    `distance_to_empty`                     DOUBLE          NOT NULL DEFAULT 0,
    `distance_to_empty_timestamp`           DATETIME        NOT NULL DEFAULT 0,
    `engine_running`                        BIT(1)          NOT NULL DEFAULT 0,
    `engine_running_timestamp`              DATETIME        NOT NULL DEFAULT 0,
    `fuel_amount`                           DOUBLE          NOT NULL DEFAULT 0,
    `fuel_amount_timestamp`                 DATETIME        NOT NULL DEFAULT 0,
    `fuel_amount_level`                     DOUBLE          NOT NULL DEFAULT 0,
    `fuel_amount_level_timestamp`           DATETIME        NOT NULL DEFAULT 0,
    `odometer`                              BIGINT UNSIGNED NOT NULL DEFAULT 0,
    `odometer_timestamp`                    DATETIME        NOT NULL DEFAULT 0,
    `parked_indoor`                         BIT(1)          NOT NULL DEFAULT 0,
    `parked_indoor_timestamp`               DATETIME        NOT NULL DEFAULT 0,
    `remote_climatization_status`           TEXT                     DEFAULT NULL,
    `remote_climatization_status_timestamp` DATETIME        NOT NULL DEFAULT 0,
    `service_warning_status`                TEXT                     DEFAULT NULL,
    `service_warning_status_timestamp`      DATETIME        NOT NULL DEFAULT 0,
    `theft_alarm`                           TEXT                     DEFAULT NULL,
    `time_fully_accessible_until`           DATETIME        NOT NULL DEFAULT 0,
    `time_partially_accessible_until`       DATETIME        NOT NULL DEFAULT 0,
    `washer_fluid_level`                    TEXT                     DEFAULT NULL,
    `washer_fluid_level_timestamp`          DATETIME        NOT NULL DEFAULT 0,
    PRIMARY KEY (`vehicle_status_id`),
    FOREIGN KEY (`vehicle_id`) REFERENCES `vehicle` (`vehicle_id`),
    INDEX (`vehicle_status_timestamp`),
    INDEX (`fuel_amount_level_timestamp`),
    INDEX (`average_speed_timestamp`),
    INDEX (`brake_fluid_timestamp`),
    INDEX (`car_locked_timestamp`),
    INDEX (`connection_status_timestamp`),
    INDEX (`distance_to_empty_timestamp`),
    INDEX (`engine_running_timestamp`),
    INDEX (`fuel_amount_timestamp`),
    INDEX (`fuel_amount_level_timestamp`),
    INDEX (`odometer_timestamp`),
    INDEX (`parked_indoor_timestamp`),
    INDEX (`remote_climatization_status_timestamp`),
    INDEX (`service_warning_status_timestamp`),
    INDEX (`washer_fluid_level_timestamp`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

CREATE TABLE `trip_meter`
(
    `vehicle_status_id` INT UNSIGNED     NOT NULL,
    `trip_meter_id`     TINYINT UNSIGNED NOT NULL,
    `trip_meter`        DOUBLE           NOT NULL DEFAULT 0,
    `status_timestamp`  DATETIME         NOT NULL DEFAULT 0,
    PRIMARY KEY (`vehicle_status_id`, `trip_meter_id`),
    FOREIGN KEY (`vehicle_status_id`) REFERENCES `vehicle_status` (`vehicle_status_id`),
    INDEX (`status_timestamp`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;


CREATE TABLE `bulb_failure`
(
    `vehicle_status_id` INT UNSIGNED NOT NULL,
    `bulb_failure`      TEXT         NOT NULL,
    `status_timestamp`  DATETIME     NOT NULL DEFAULT 0,
    FOREIGN KEY (`vehicle_status_id`) REFERENCES `vehicle_status` (`vehicle_status_id`),
    INDEX (`status_timestamp`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

CREATE TABLE `vehicle_door_status`
(
    `vehicle_status_id`     INT UNSIGNED NOT NULL,
    `front_left_door_open`  BIT(1)       NOT NULL DEFAULT 0,
    `front_right_door_open` BIT(1)       NOT NULL DEFAULT 0,
    `rear_left_door_open`   BIT(1)       NOT NULL DEFAULT 0,
    `rear_right_door_open`  BIT(1)       NOT NULL DEFAULT 0,
    `hood_open`             BIT(1)       NOT NULL DEFAULT 0,
    `tailgate_open`         BIT(1)       NOT NULL DEFAULT 0,
    `status_timestamp`      DATETIME     NOT NULL DEFAULT 0,
    PRIMARY KEY (`vehicle_status_id`),
    FOREIGN KEY (`vehicle_status_id`) REFERENCES `vehicle_status` (`vehicle_status_id`),
    INDEX (`status_timestamp`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

CREATE TABLE `vehicle_heater_status`
(
    `vehicle_status_id`            INT UNSIGNED NOT NULL,
    `heater_status`                TEXT                  DEFAULT NULL,
    `front_driver_side_seat_on`    BIT(1)       NOT NULL DEFAULT 0,
    `front_passenger_side_seat_on` BIT(1)       NOT NULL DEFAULT 0,
    `rear_driver_side_seat_on`     BIT(1)       NOT NULL DEFAULT 0,
    `rear_passenger_side_seat_on`  BIT(1)       NOT NULL DEFAULT 0,
    `rear_mid_seat_on`             BIT(1)       NOT NULL DEFAULT 0,
    `status_timestamp`             DATETIME     NOT NULL DEFAULT 0,
    PRIMARY KEY (`vehicle_status_id`),
    FOREIGN KEY (`vehicle_status_id`) REFERENCES `vehicle_status` (`vehicle_status_id`),
    INDEX (`status_timestamp`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

CREATE TABLE `vehicle_heater_timer_status`
(
    `vehicle_status_id` INT UNSIGNED     NOT NULL,
    `timer_id`          TINYINT UNSIGNED NOT NULL,
    `state`             BIT(1)           NOT NULL DEFAULT 0,
    `timer_time`        TIME             NOT NULL DEFAULT 0,
    PRIMARY KEY (`vehicle_status_id`, `timer_id`),
    FOREIGN KEY (`vehicle_status_id`) REFERENCES `vehicle_status` (`vehicle_status_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

CREATE TABLE `vehicle_battery_status`
(
    `vehicle_status_id`                          INT UNSIGNED NOT NULL,
    `distance_to_hvbattery_empty`                DOUBLE       NOT NULL DEFAULT 0,
    `distance_to_hvbattery_empty_timestamp`      DATETIME     NOT NULL DEFAULT 0,
    `hv_battery_charge_mode_status`              TEXT                  DEFAULT NULL,
    `hv_battery_charge_mode_status_timestamp`    DATETIME     NOT NULL DEFAULT 0,
    `hv_battery_charge_status`                   TEXT                  DEFAULT NULL,
    `hv_battery_charge_status_timestamp`         DATETIME     NOT NULL DEFAULT 0,
    `hv_battery_charge_status_derived`           TEXT                  DEFAULT NULL,
    `hv_battery_charge_status_derived_timestamp` DATETIME     NOT NULL DEFAULT 0,
    `hv_battery_charge_warning`                  TEXT                  DEFAULT NULL,
    `hv_battery_charge_warning_timestamp`        DATETIME     NOT NULL DEFAULT 0,
    `hv_battery_level`                           DOUBLE       NOT NULL DEFAULT 0,
    `hv_battery_level_timestamp`                 DATETIME     NOT NULL DEFAULT 0,
    `time_to_hvbattery_fully_charged`            DOUBLE       NOT NULL DEFAULT 0,
    `time_to_hvbattery_fully_charged_timestamp`  DATETIME     NOT NULL DEFAULT 0,
    PRIMARY KEY (`vehicle_status_id`),
    FOREIGN KEY (`vehicle_status_id`) REFERENCES `vehicle_status` (`vehicle_status_id`),
    INDEX (`distance_to_hvbattery_empty_timestamp`),
    INDEX (`hv_battery_charge_mode_status_timestamp`),
    INDEX (`hv_battery_charge_status_timestamp`),
    INDEX (`hv_battery_charge_status_derived_timestamp`),
    INDEX (`hv_battery_charge_warning_timestamp`),
    INDEX (`hv_battery_level_timestamp`),
    INDEX (`time_to_hvbattery_fully_charged_timestamp`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

CREATE TABLE `vehicle_tyre_pressure_status`
(
    `vehicle_status_id`           INT UNSIGNED NOT NULL,
    `front_left_pressure_status`  TEXT                  DEFAULT NULL,
    `front_right_pressure_status` TEXT                  DEFAULT NULL,
    `rear_left_pressure_status`   TEXT                  DEFAULT NULL,
    `rear_right_pressure_status`  TEXT                  DEFAULT NULL,
    `status_timestamp`            DATETIME     NOT NULL DEFAULT 0,
    PRIMARY KEY (`vehicle_status_id`),
    FOREIGN KEY (`vehicle_status_id`) REFERENCES `vehicle_status` (`vehicle_status_id`),
    INDEX (`status_timestamp`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

CREATE TABLE `vehicle_window_status`
(
    `vehicle_status_id`       INT UNSIGNED NOT NULL,
    `front_left_window_open`  BIT(1)       NOT NULL DEFAULT 0,
    `front_right_window_open` BIT(1)       NOT NULL DEFAULT 0,
    `rear_left_window_open`   BIT(1)       NOT NULL DEFAULT 0,
    `rear_right_window_open`  BIT(1)       NOT NULL DEFAULT 0,
    `status_timestamp`        DATETIME     NOT NULL DEFAULT 0,
    PRIMARY KEY (`vehicle_status_id`),
    FOREIGN KEY (`vehicle_status_id`) REFERENCES `vehicle_status` (`vehicle_status_id`),
    INDEX (`status_timestamp`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

CREATE TABLE `trip`
(
    `vehicle_id`   CHAR(20)        NOT NULL,
    `trip_id`      BIGINT UNSIGNED NOT NULL,
    `category`     TEXT         DEFAULT NULL,
    `name`         TEXT         DEFAULT NULL,
    `notes`        TEXT         DEFAULT NULL,
    `waypoints`    INT UNSIGNED DEFAULT NULL,
    `bounding_box` POLYGON      DEFAULT NULL,
    PRIMARY KEY (`vehicle_id`, `trip_id`),
    UNIQUE KEY `trip_unique_trip_id` (`trip_id`),
    FOREIGN KEY (`vehicle_id`) REFERENCES `vehicle` (`vehicle_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

CREATE TABLE `trip_detail`
(
    `trip_id`                 BIGINT UNSIGNED NOT NULL,
    `trip_leg`                INT UNSIGNED    NOT NULL,
    `distance`                INT UNSIGNED    NOT NULL,
    `electrical_consumption`  DOUBLE UNSIGNED NOT NULL DEFAULT 0,
    `fuel_consumption`        DOUBLE UNSIGNED NOT NULL DEFAULT 0,
    `electrical_regeneration` DOUBLE UNSIGNED NOT NULL DEFAULT 0,
    `start_odometer`          BIGINT UNSIGNED NOT NULL DEFAULT 0,
    `end_odometer`            BIGINT UNSIGNED NOT NULL DEFAULT 0,
    `start_time`              DATETIME        NOT NULL DEFAULT 0,
    `end_time`                DATETIME        NOT NULL DEFAULT 0,
    PRIMARY KEY (`trip_id`, `trip_leg`),
    FOREIGN KEY (`trip_id`) REFERENCES `trip` (`trip_id`),
    INDEX (`start_time`),
    INDEX (`end_time`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

CREATE TABLE `trip_position`
(
    `trip_id`        BIGINT UNSIGNED       NOT NULL,
    `trip_leg`       INT UNSIGNED          NOT NULL,
    `position_type`  ENUM ('START', 'END') NOT NULL,
    `city`           VARCHAR(100)          NOT NULL,
    `country`        CHAR(2)               NOT NULL,
    `street_address` TEXT                  NOT NULL,
    `region`         TEXT                  NOT NULL,
    `post_code`      VARCHAR(100)          NOT NULL,
    `gps_position`   POINT                 NOT NULL,
    PRIMARY KEY (`trip_id`, `trip_leg`, `position_type`),
    FOREIGN KEY (`trip_id`, `trip_leg`) REFERENCES `trip_detail` (`trip_id`, `trip_leg`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

CREATE TABLE `trip_waypoint`
(
    `trip_id`                 BIGINT UNSIGNED NOT NULL,
    `waypoint_timestamp`      DATETIME        NOT NULL DEFAULT 0,
    `electrical_consumption`  DOUBLE UNSIGNED NOT NULL DEFAULT 0,
    `fuel_consumption`        DOUBLE UNSIGNED NOT NULL DEFAULT 0,
    `electrical_regeneration` DOUBLE UNSIGNED NOT NULL DEFAULT 0,
    `speed`                   DOUBLE UNSIGNED NOT NULL DEFAULT 0,
    `heading`                 DOUBLE UNSIGNED NOT NULL DEFAULT 0,
    `odometer`                BIGINT UNSIGNED NOT NULL DEFAULT 0,
    `gps_position`            POINT           NOT NULL,
    FOREIGN KEY (`trip_id`) REFERENCES `trip` (`trip_id`),
    INDEX (`waypoint_timestamp`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;
