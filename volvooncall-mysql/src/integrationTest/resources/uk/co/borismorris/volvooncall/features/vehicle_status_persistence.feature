Feature: When I request that the service save vehicle status, the status is saved

  Background:
    Given I retrieve the customer account
    And the customer account is not null
    And I retrieve vehicles

  Scenario: I can persist vehicle status
    When I request that vehicle status is persisted for "AA1AAAAAA0A1234567"
    Then the count of vehicle statuses for "AA1AAAAAA0A1234567" is 1
