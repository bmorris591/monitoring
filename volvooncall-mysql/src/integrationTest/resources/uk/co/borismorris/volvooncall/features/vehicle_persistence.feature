Feature: When I use the service to retrieve vehicles, they are persisted

  Background:
    Given I retrieve the customer account
    And the customer account is not null

  Scenario: I can persist a vehicle
    When I retrieve vehicles
    Then there should be 2 vehicles returned
    And the count of vehicles should be 2
