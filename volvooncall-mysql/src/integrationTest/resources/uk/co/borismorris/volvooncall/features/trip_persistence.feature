Feature: When I request that the service save trips

  Background:
    Given I retrieve the customer account
    And the customer account is not null
    And I retrieve vehicles

  Scenario: I can persist a vehicle's trips
    When I request that trips are persisted for "AA1AAAAAA0A1234567"
    Then the count of trips is 3
    And the tips persisted match those downloaded for "AA1AAAAAA0A1234567"

  Scenario: I can persist a trip route
    When I request that trips are persisted for "AA1AAAAAA0A1234567"
    Then the route persisted for 1234567890 in "AA1AAAAAA0A1234567" matches that downloaded
