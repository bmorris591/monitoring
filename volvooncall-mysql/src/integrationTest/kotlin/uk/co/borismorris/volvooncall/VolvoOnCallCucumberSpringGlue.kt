package uk.co.borismorris.volvooncall

import io.cucumber.java.After
import io.cucumber.spring.CucumberContextConfiguration
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.testcontainers.jdbc.ContainerDatabaseDriver
import org.wiremock.spring.ConfigureWireMock
import org.wiremock.spring.DYNAMIC_PORT
import org.wiremock.spring.EnableWireMock
import org.wiremock.spring.PORT_DISABLED
import java.nio.file.Files
import kotlin.io.path.absolutePathString

@CucumberContextConfiguration
@SpringBootTest(
    properties = [
        "spring.datasource.url=jdbc:tc:mariadb:latest:///volvooncall",
        "voc.username=username",
        "voc.password=password1234",
        "voc.base-url=\${wiremock.baseUrl}/api",
        "spring.http.client.ssl.bundle=wiremock",
        "test-certs.wiremock.enabled=true",
    ],
)
@EnableWireMock(
    ConfigureWireMock(
        port = PORT_DISABLED,
        httpsPort = DYNAMIC_PORT,
        sslBundle = "wiremock",
        filesUnderClasspath = ["uk/co/borismorris/volvooncall/vocclient/wiremock"],
        templatingEnabled = true,
        globalTemplatingEnabled = true,
    ),
)
@DirtiesContext
@ActiveProfiles("test")
open class VolvoOnCallCucumberSpringGlue {
    companion object {
        @JvmStatic
        @DynamicPropertySource
        fun registerVocProperties(registry: DynamicPropertyRegistry) {
            registry.add("voc.cache-path") {
                Files.createTempDirectory("token-refresh").resolve("oauth_client_registry.json").absolutePathString()
            }
        }
    }
}

class VolvoOnCallTestContainers {
    @After
    fun killContainers() {
        ContainerDatabaseDriver.killContainers()
    }
}
