package uk.co.borismorris.volvooncall.steps

import io.cucumber.java8.En
import org.assertj.core.api.Assertions.assertThat
import uk.co.borismorris.volvooncall.dao.VehicleRepository
import uk.co.borismorris.volvooncall.dao.VehicleStatusRepository
import uk.co.borismorris.volvooncall.service.VehicleService
import uk.co.borismorris.volvooncall.vocclient.Vehicle

class VehicleSteps(
    private val customerAccountSteps: CustomerAccountSteps,
    private val vehicleService: VehicleService,
    private val vehicleRepository: VehicleRepository,
    private val vehicleStatusRepository: VehicleStatusRepository,
) : En {

    lateinit var vehicles: List<Vehicle>

    init {
        When("I retrieve vehicles") {
            vehicles = vehicleService.vehicles(customerAccountSteps.customerAccount).toList()
        }
        Then("there should be {int} vehicles returned") { expectedCount: Int ->
            assertThat(vehicles).hasSize(expectedCount)
        }
        Then("the count of vehicles should be {int}") { expectedCount: Int ->
            val count = vehicleRepository.count()
            assertThat(count).isEqualTo(expectedCount.toLong())
        }
        When("I request that vehicle status is persisted for {string}") { vin: String ->
            vehicleService.saveStatus(vehicle(vin))
        }
        Then("the count of vehicle statuses for {string} is {int}") { vin: String, expectedCount: Int ->
            val count = vehicleStatusRepository.count(vehicle(vin))
            assertThat(count).isEqualTo(expectedCount.toLong())
        }
    }

    fun vehicle(vin: String) = vehicles.first { it.attributes.vehicleIdentificationNumber == vin }
}
