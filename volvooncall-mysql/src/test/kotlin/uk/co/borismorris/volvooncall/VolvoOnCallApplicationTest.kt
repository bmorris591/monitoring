package uk.co.borismorris.volvooncall

import org.springframework.modulith.core.ApplicationModules
import org.springframework.modulith.docs.Documenter
import kotlin.test.Test

class VolvoOnCallApplicationTest {

    @Test
    fun writeDocumentationSnippets() {
        var modules = ApplicationModules.of(VolvoOnCallApplication::class.java).verify()
        Documenter(modules)
            .writeModulesAsPlantUml()
            .writeIndividualModulesAsPlantUml()
    }
}
