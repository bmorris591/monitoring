package uk.co.borismorris.volvooncall.vocclient

import com.github.tomakehurst.wiremock.common.Slf4jNotifier
import com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig
import com.github.tomakehurst.wiremock.junit5.WireMockExtension
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS
import org.junit.jupiter.api.extension.RegisterExtension
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import org.mockito.kotlin.mock
import org.springframework.boot.http.client.ClientHttpRequestFactoryBuilder
import org.springframework.web.client.RestClient
import uk.co.borismorris.volvooncall.config.VocConfig
import java.net.URI
import java.util.function.Function

@TestInstance(PER_CLASS)
internal class RestclientVocClientTest {

    companion object {
        @RegisterExtension
        @JvmStatic
        val wiremock: WireMockExtension = WireMockExtension.newInstance()
            .options(
                wireMockConfig()
                    .dynamicPort()
                    .usingFilesUnderClasspath("uk/co/borismorris/volvooncall/vocclient/wiremock")
                    .notifier(Slf4jNotifier(false))
                    .templatingEnabled(true)
                    .globalTemplating(true),
            )
            .configureStaticDsl(true)
            .build()
    }

    lateinit var mockBaseUrl: String
    lateinit var client: RestclientVocClient

    @BeforeAll
    fun setup() {
        mockBaseUrl = wiremock.runtimeInfo.httpBaseUrl
        val config = VocConfig().apply {
            baseUrl = URI.create("$mockBaseUrl/api").toURL()
        }
        val restClientBuilder = RestClient.builder()
            .requestFactory(ClientHttpRequestFactoryBuilder.simple().build())
        client = RestclientVocClient(config, restClientBuilder)
    }

    @Test
    fun `When the customer account is requested then it is retrieved`() {
        val customerAccount = client.customerAccount()

        assertThat(customerAccount).isNotNull()
        assertThat(customerAccount.username).isEqualTo("bmorris591@gmail.com")
        assertThat(customerAccount.firstName).isEqualTo("Boris")
        assertThat(customerAccount.lastName).isEqualTo("Morris")
        assertThat(customerAccount.accountId).isEqualTo("af17ds76-17f3-45dw-3fcf-1111ee3333f22")
        assertThat(customerAccount).isInstanceOfSatisfying(CustomerAccount::class.java) {
            assertThat(it.accountUrl).isEqualTo(
                URI.create("$mockBaseUrl/api/customeraccounts/af17ds76-17f3-45dw-3fcf-1111ee3333f22").toURL(),
            )
            assertThat(it.vehicleRelations).containsExactly(
                URI.create("$mockBaseUrl/api/vehicle-account-relations/1234567").toURL(),
                URI.create("$mockBaseUrl/api/vehicle-account-relations/7654321").toURL(),
            )
        }
    }

    @Test
    fun `When vehicles are requested then they are retrieved`() {
        val customerAccount = CustomerAccount(
            "",
            "",
            "",
            "",
            URI.create(mockBaseUrl).toURL(),
            listOf(
                URI.create("$mockBaseUrl/api/vehicle-account-relations/1234567").toURL(),
                URI.create("$mockBaseUrl/api/vehicle-account-relations/7654321").toURL(),
            ),
        )

        val vehicles = client.vehicles(customerAccount).toList()

        assertThat(vehicles).isNotNull().hasSize(2)
        assertThat(vehicles.map { it.attributes.vehicleIdentificationNumber }).containsExactlyInAnyOrder(
            "AA1AAAAAA0A1234567",
            "AA1AAAAAA0A7654321",
        )
    }

    @ValueSource(strings = ["AA1AAAAAA0A1234567", "AA1AAAAAA0A8901234"])
    @ParameterizedTest
    fun `When vehicle status is requested then it is retrieved`(vehicleId: String) {
        val vehicle = mockVehicle(vehicleId)

        val status = client.vehicleStatus(vehicle)

        assertThat(status).isNotNull()
        assertThat(status.engineRunning).isFalse()
        assertThat(status.odometer).isEqualTo(100_000)
    }

    @Test
    fun `When vehicle trips are requested then they are retrieved`() {
        val vehicle = mockVehicle("AA1AAAAAA0A1234567")

        val trips = client.vehicleTrips(vehicle).trips

        assertThat(trips).isNotNull().hasSize(3)
        assertThat(trips).extracting(Function { it.id })
            .containsExactlyInAnyOrder(1_234_567_890L, 9_876_542_310L, 9_876_542_311L)
    }

    @Test
    fun `When route is requested this it is received`() {
        val vid = "AA1AAAAAA0A1234567"
        val tid = "1234567890"
        val routeUrl = URI.create("$mockBaseUrl/api/vehicles/$vid/trips/$tid/route").toURL()
        val box = BoundingBox(0.0, 0.0, 0.0, 0.0)
        val routeDetails = RouteDetails(box, routeUrl, 10)
        val trip = Trip("", 0, "", routeDetails, URI.create("http://test.invalid").toURL(), emptyList(), "")

        val route = client.tripRoute(trip).toList()

        assertThat(route).isNotNull().hasSize(10)
    }

    @Test
    fun `Given RouteDetails are null, when route is requested this it is received`() {
        val vid = "AA1AAAAAA0A1234567"
        val tid = "1234567890"
        val tripUrl = URI.create("$mockBaseUrl/api/vehicles/$vid/trips/$tid").toURL()
        val trip = Trip("", 0, "", null, tripUrl, emptyList(), "")

        val route = client.tripRoute(trip).toList()

        assertThat(route).isNotNull().hasSize(10)
    }

    private fun mockVehicle(id: String): Vehicle {
        val vehicleUrls = VehicleUrls(
            id,
            URI.create("$mockBaseUrl/api/vehicles/$id/attributes").toURL(),
            URI.create("$mockBaseUrl/api/vehicles/$id/status").toURL(),
            emptyList(),
        )
        val vehicleAttributes = mock<VehicleAttributes> { }
        return Vehicle(vehicleUrls, vehicleAttributes)
    }
}
