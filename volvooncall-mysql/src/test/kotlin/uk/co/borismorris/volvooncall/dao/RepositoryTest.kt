package uk.co.borismorris.volvooncall.dao

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.recursive.comparison.RecursiveComparisonConfiguration
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import uk.co.borismorris.volvooncall.mockRouteDetails
import uk.co.borismorris.volvooncall.mockTrip
import uk.co.borismorris.volvooncall.mockTrips
import uk.co.borismorris.volvooncall.mockVehicle
import uk.co.borismorris.volvooncall.mockVehicleStatus
import uk.co.borismorris.volvooncall.mockWaypoint
import uk.co.borismorris.volvooncall.utcNoNano
import uk.co.borismorris.volvooncall.vocclient.Trip
import uk.co.borismorris.volvooncall.vocclient.Vehicle
import java.nio.file.Files
import java.nio.file.Path
import java.time.Clock
import java.time.Duration
import java.time.Instant
import java.time.ZoneOffset.UTC
import java.time.ZonedDateTime
import kotlin.io.path.absolutePathString

@SpringBootTest(
    properties = [
        "spring.datasource.url=jdbc:tc:mariadb:latest:///volvooncall",
    ],
)
@ActiveProfiles("test", "jooq-logging")
internal class RepositoryTest {
    companion object {
        @JvmStatic
        @TempDir
        lateinit var tempDir: Path

        @JvmStatic
        @DynamicPropertySource
        fun registerVocProperties(registry: DynamicPropertyRegistry) {
            registry.add("voc.cache-path") {
                Files.createTempDirectory(tempDir, "token-refresh").resolve("oauth_client_registry.json")
                    .absolutePathString()
            }
        }
    }

    @Autowired
    lateinit var vehicleRepository: VehicleRepository

    @Autowired
    lateinit var vehicleStatusRepository: VehicleStatusRepository

    @Autowired
    lateinit var tripRepository: TripRepository

    @Autowired
    lateinit var waypointRepository: TripWaypointRepository

    @Test
    fun `Run basic query in transaction`() {
        val count = vehicleRepository.count()

        assertThat(count).isNotNull()
    }

    @Test
    fun `I can insert a vehicle`() {
        val vehicle = mockVehicle()

        vehicleRepository.save(vehicle)

        val count = vehicleRepository.count()
        assertThat(count).isGreaterThanOrEqualTo(1)
    }

    @Test
    fun `I can select a vehicle`() {
        val vehicle = mockVehicle()

        vehicleRepository.save(vehicle)

        assertThat(vehicleRepository.exists(vehicle.id))
            .`as`("Vehicle exists")
            .isTrue()

        val actual = vehicleRepository.load(vehicle.id)
        assertThat(actual)
            .usingRecursiveComparison(vehicleComparison())
            .isEqualTo(vehicle)
    }

    @Test
    fun `Random vehicle doesnt exist`() {
        assertThat(vehicleRepository.exists("foo"))
            .`as`("Vehicle doesns't exist")
            .isFalse()
    }

    @Test
    fun `I can select vehicles`() {
        val vehicle = mockVehicle()

        vehicleRepository.save(vehicle)

        val actual = vehicleRepository.loadAll().toList()
        assertThat(actual).hasSizeGreaterThanOrEqualTo(1).anySatisfy {
            assertThat(it)
                .usingRecursiveComparison(vehicleComparison())
                .isEqualTo(vehicle)
        }
    }

    private fun vehicleComparison() = RecursiveComparisonConfiguration.builder()
        .withIgnoredFields(
            "attributes.country.iso2",
            "urls.attributesUrl",
            "urls.statusUrl",
        )
        .build()

    @Test
    fun `I can insert a vehicle status`() {
        val vehicle = mockVehicle()
        val vehicleStatus = mockVehicleStatus()

        vehicleRepository.save(vehicle)
        val updated = vehicleStatusRepository.save(vehicle, vehicleStatus, Instant.now())
        assertThat(updated.id).isGreaterThan(0)
        assertThat(updated.updated).isGreaterThanOrEqualTo(10)

        val count = vehicleStatusRepository.count(vehicle)
        assertThat(count).isGreaterThanOrEqualTo(1L)
    }

    @Test
    fun `I can select vehicle status`() {
        val vehicle = mockVehicle()
        val vehicleStatus = mockVehicleStatus()

        vehicleRepository.save(vehicle)
        vehicleStatusRepository.save(vehicle, vehicleStatus, Instant.now())

        val statuses = vehicleStatusRepository.load(vehicle).toList()
        assertThat(statuses).singleElement()
            .usingRecursiveComparison()
            .withEqualsForType({ l, r -> l.utcNoNano() == r.utcNoNano() }, ZonedDateTime::class.java)
            .isEqualTo(vehicleStatus)
    }

    @Test
    fun `I can insert a trip`() {
        val vehicle = mockVehicle()
        val trip = mockTrip(mockRouteDetails())

        vehicleRepository.save(vehicle)

        tripRepository.save(vehicle, trip)

        val count = tripRepository.count()
        assertThat(count).isGreaterThanOrEqualTo(1)
    }

    @Test
    fun `I can select a trip`() {
        val vehicle = mockVehicle()
        val trip = mockTrip(mockRouteDetails())
        saveAndCheckTrip(vehicle, trip)
    }

    @Test
    fun `I can select all trips`() {
        val vehicles = (5..10).asSequence()
            .map { mockVehicle() }
            .map { it to mockTrips() }
            .toList()
        vehicles.forEach { (v, _) -> vehicleRepository.save(v) }
        vehicles.asSequence()
            .flatMap { (v, ts) -> ts.asSequence().map { it to v } }
            .forEach { (t, v) -> tripRepository.save(v, t) }

        val trips = tripRepository.loadAll().toList()
        val createdTrips = vehicles.asSequence()
            .flatMap { it.second.asSequence() }
            .map { it.id }
            .toSet()
        val insertedTrips = trips
            .filter { createdTrips.contains(it.id) }

        assertThat(insertedTrips)
            .usingRecursiveComparison(
                tripComparator()
                    .withIgnoredCollectionOrderInFields("")
                    .build(),
            )
            .isEqualTo(vehicles.flatMap { it.second })
    }

    @Test
    fun `I can insert a trip with no route details`() {
        val vehicle = mockVehicle()
        val trip = mockTrip(null)

        vehicleRepository.save(vehicle)

        tripRepository.save(vehicle, trip)

        val count = tripRepository.count()
        assertThat(count).isGreaterThanOrEqualTo(1)
    }

    @Test
    fun `I can select a trip with no route details`() {
        val vehicle = mockVehicle()
        val trip = mockTrip(null)
        saveAndCheckTrip(vehicle, trip)
    }

    @Test
    fun `I can load trips after a point in time`() {
        val start = Instant.parse("2007-12-03T10:15:30.00Z")
        val clock = Clock.fixed(start, UTC)
        val vehicle = mockVehicle()
        vehicleRepository.save(vehicle)

        val trips = listOf(
            mockTrip(mockRouteDetails(), clock),
            mockTrip(mockRouteDetails(), Clock.offset(clock, Duration.ofHours(1))),
            mockTrip(mockRouteDetails(), Clock.offset(clock, Duration.ofHours(2))),
            mockTrip(mockRouteDetails(), Clock.offset(clock, Duration.ofHours(3))),
            mockTrip(mockRouteDetails(), Clock.offset(clock, Duration.ofHours(4))),
        )
        trips.forEach {
            tripRepository.save(vehicle, it)
        }

        assertThat(tripRepository.loadAllStartedAfter(vehicle, ZonedDateTime.ofInstant(start, UTC).plusHours(5)).toList()).isEmpty()
        assertThat(tripRepository.loadAllStartedAfter(vehicle, ZonedDateTime.ofInstant(start, UTC).minusHours(1)).toList()).hasSameSizeAs(trips)
        assertThat(tripRepository.loadAllStartedAfter(vehicle, ZonedDateTime.ofInstant(start, UTC).plusHours(2)).toList()).hasSize(3)
    }

    private fun saveAndCheckTrip(vehicle: Vehicle, trip: Trip) {
        vehicleRepository.save(vehicle)

        tripRepository.save(vehicle, trip)

        assertThat(tripRepository.exists(trip.id))
            .`as`("Trip exists")
            .isTrue()

        val value = tripRepository.load(trip.id)
        assertThat(value)
            .usingRecursiveComparison(tripComparator().build())
            .isEqualTo(trip)
    }

    @Test
    fun `Random trip doesnt exist`() {
        assertThat(tripRepository.exists(100L))
            .`as`("Trip doesns't exist")
            .isFalse()
    }

    private fun tripComparator() = RecursiveComparisonConfiguration.builder()
        .withIgnoredFields("trip", "routeDetails.route")
        .withEqualsForType({ l, r -> l.utcNoNano() == r.utcNoNano() }, ZonedDateTime::class.java)
        .withEqualsForFields({ l, r -> (l as Double).toInt().compareTo((r as Double).toInt()) == 0 }, "tripDetails.distance")

    @Test
    fun `I can insert a flow of waypoints`() {
        val generateCount = 10_000L
        val trip = saveSomeWaypoints(generateCount)

        val count = waypointRepository.count(trip)
        assertThat(count).isEqualTo(generateCount)
    }

    private fun saveSomeWaypoints(generateCount: Long): Trip {
        val vehicle = mockVehicle()
        val trip = mockTrip(mockRouteDetails())
        val waypoints = (1..generateCount).asSequence().map {
            mockWaypoint((it * 2).toDouble(), (it * 3).toDouble())
        }

        vehicleRepository.save(vehicle)
        tripRepository.save(vehicle, trip)
        waypointRepository.save(trip, waypoints)
        return trip
    }

    @Test
    fun `I can load waypoints`() {
        val vehicle = mockVehicle()
        val trip = mockTrip(mockRouteDetails())
        val waypoints = (1..10).map {
            mockWaypoint((it * 2).toDouble(), (it * 3).toDouble())
        }.toList()

        vehicleRepository.save(vehicle)
        tripRepository.save(vehicle, trip)
        waypointRepository.save(trip, waypoints.asSequence())

        val loaded = waypointRepository.load(trip.id).toList()
        assertThat(loaded)
            .usingRecursiveComparison()
            .withEqualsForType({ l, r -> l.utcNoNano() == r.utcNoNano() }, ZonedDateTime::class.java)
            .isEqualTo(waypoints)
    }

    @Test
    fun `I can delete a trip with waypoints`() {
        val generateCount = 100L
        val trip1 = saveSomeWaypoints(generateCount)
        saveSomeWaypoints(generateCount)

        assertThat(tripRepository.deleteById(trip1.id)).`as`("Updated on delete").isGreaterThan(0)
        assertThat(waypointRepository.count(trip1)).isEqualTo(0)
    }
}
