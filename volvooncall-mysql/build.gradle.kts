import io.gitlab.arturbosch.detekt.Detekt
import org.jooq.meta.jaxb.ForcedType
import uk.co.borismorris.jooq.JOOQ_CODEGEN_TASK_NAME
import uk.co.borismorris.jooq.JooqCodegenExtension
import uk.co.borismorris.jooq.JooqCodegenPlugin

plugins {
    monitoring.`application-conventions`
    monitoring.`jib-conventions`

    alias(libs.plugins.openapi.generator)
}

apply<JooqCodegenPlugin>()

group = "uk.co.borismorris.volvooncall"

val jooqDir = layout.buildDirectory.dir("generated/jooq")
configure<JooqCodegenExtension> {
    migrations.set(file("src/main/resources/db/migration/mariadb"))
    outputDirectory.set(jooqDir)
    packageName.set("uk.co.borismorris.volvooncall.generated.database")
    forcedTypes.set(
        listOf(
            ForcedType()
                .withUserType("uk.co.borismorris.volvooncall.jooq.bind.G2DPoint")
                .withBinding("uk.co.borismorris.volvooncall.jooq.bind.PointBinding")
                .withIncludeTypes("POINT"),
            ForcedType()
                .withUserType("uk.co.borismorris.volvooncall.jooq.bind.G2DPolygon")
                .withBinding("uk.co.borismorris.volvooncall.jooq.bind.PolygonBinding")
                .withIncludeTypes("POLYGON"),
        ),
    )
}

kotlin {
    sourceSets {
        main {
            kotlin.srcDir(tasks[JOOQ_CODEGEN_TASK_NAME])
        }
    }
}

dependencies {
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.data:spring-data-jdbc")
    implementation("org.springframework.modulith:spring-modulith-api")
    implementation(projects.springOauth2)

    implementation(libs.jdbc.micrometer)
    implementation(libs.mariadb.jdbc)
    implementation("com.zaxxer:HikariCP")
    implementation("org.flywaydb:flyway-mysql")
    implementation(libs.geolatte)
    implementation(libs.jooq)
    implementation(libs.jooq.kotlin)

    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.springframework.modulith:spring-modulith-starter-test")
    testImplementation(projects.wiremockSpringBoot)
    testImplementation(projects.testCerts.springTestCerts)
    testImplementation(libs.commons.compress)

    testImplementation("org.testcontainers:mariadb")

    integrationTestImplementation("io.cucumber:cucumber-java8")
    integrationTestImplementation("io.cucumber:cucumber-java")
    integrationTestImplementation("io.cucumber:cucumber-spring")
}

tasks.withType<Detekt> {
    dependsOn(JOOQ_CODEGEN_TASK_NAME)
}

application {
    mainClass = "uk.co.borismorris.volvooncall.VolvoOnCallApplicationKt"
}
