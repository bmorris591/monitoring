plugins {
    `java-library`
}

group = "uk.co.borismorris.monitoring.oauth2"

dependencies {
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.springframework.boot:spring-boot-starter-web")
    api("org.springframework.security:spring-security-oauth2-client")
    implementation("org.springframework.security:spring-security-config")
}
