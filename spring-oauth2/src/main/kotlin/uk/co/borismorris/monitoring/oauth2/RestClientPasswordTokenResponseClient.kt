@file:Suppress("DEPRECATION")

package uk.co.borismorris.monitoring.oauth2

import org.springframework.security.oauth2.client.endpoint.DefaultOAuth2TokenRequestHeadersConverter
import org.springframework.security.oauth2.client.endpoint.DefaultOAuth2TokenRequestParametersConverter
import org.springframework.security.oauth2.client.endpoint.OAuth2AccessTokenResponseClient
import org.springframework.security.oauth2.client.endpoint.OAuth2PasswordGrantRequest
import org.springframework.security.oauth2.core.OAuth2AuthorizationException
import org.springframework.security.oauth2.core.OAuth2Error
import org.springframework.security.oauth2.core.endpoint.OAuth2AccessTokenResponse
import org.springframework.web.client.RestClient
import org.springframework.web.client.RestClientException
import org.springframework.web.client.body

const val INVALID_TOKEN_RESPONSE_ERROR_CODE = "invalid_token_response"

class RestClientPasswordTokenResponseClient(private val restClient: RestClient) : OAuth2AccessTokenResponseClient<OAuth2PasswordGrantRequest> {
    private val headersConverter = DefaultOAuth2TokenRequestHeadersConverter<OAuth2PasswordGrantRequest>()
    private val parametersConverter = DefaultOAuth2TokenRequestParametersConverter<OAuth2PasswordGrantRequest>()

    override fun getTokenResponse(grantRequest: OAuth2PasswordGrantRequest): OAuth2AccessTokenResponse {
        try {
            return restClient.post()
                .uri(grantRequest.clientRegistration.providerDetails.tokenUri)
                .headers { h -> h.addAll(headersConverter.convert(grantRequest)!!) }
                .body(parametersConverter.convert(grantRequest)!!)
                .retrieve()
                .body() ?: throw OAuth2AuthorizationException(
                OAuth2Error(
                    INVALID_TOKEN_RESPONSE_ERROR_CODE,
                    "Empty OAuth 2.0 Access Token Response",
                    null,
                ),
            )
        } catch (ex: RestClientException) {
            val error = OAuth2Error(
                INVALID_TOKEN_RESPONSE_ERROR_CODE,
                "An error occurred while attempting to retrieve the OAuth 2.0 Access Token Response:${ex.message}",
                grantRequest.clientRegistration.providerDetails.tokenUri,
            )
            throw OAuth2AuthorizationException(error, ex)
        }
    }
}
