package uk.co.borismorris.monitoring.oauth2

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser.Feature.AUTO_CLOSE_SOURCE
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature.INDENT_OUTPUT
import com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATES_AS_TIMESTAMPS
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.databind.deser.std.FromStringDeserializer
import com.fasterxml.jackson.databind.ser.std.StdScalarSerializer
import com.fasterxml.jackson.module.kotlin.readValue
import org.springframework.security.core.Authentication
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository
import org.springframework.security.oauth2.core.OAuth2AccessToken
import org.springframework.security.oauth2.core.OAuth2AccessToken.TokenType
import org.springframework.security.oauth2.core.OAuth2RefreshToken
import java.io.Reader
import java.nio.charset.StandardCharsets.UTF_8
import java.nio.file.Path
import java.nio.file.StandardOpenOption.CREATE
import java.nio.file.StandardOpenOption.TRUNCATE_EXISTING
import java.time.Instant
import java.util.concurrent.locks.ReentrantReadWriteLock
import kotlin.io.path.exists
import kotlin.io.path.reader
import kotlin.io.path.writer

class JsonFileOauth2AuthorizedClientService(
    private val clientRegistrationRepository: ClientRegistrationRepository,
    mapper: ObjectMapper,
    private val path: Path,
) : OAuth2AuthorizedClientService {

    private val mapper = mapper.copy()
        .enable(AUTO_CLOSE_SOURCE)
        .disable(WRITE_DATES_AS_TIMESTAMPS)
        .enable(INDENT_OUTPUT)
        .setSerializationInclusion(NON_EMPTY)
        .addMixIn(OAuth2AccessToken::class.java, OAuth2AccessTokenMixin::class.java)
        .addMixIn(OAuth2RefreshToken::class.java, OAuth2RefreshTokenMixin::class.java)
    private val lock = ReentrantReadWriteLock()

    override fun <T : OAuth2AuthorizedClient> loadAuthorizedClient(
        clientRegistrationId: String?,
        principalName: String?,
    ): T? {
        lock.readLock().lock()
        try {
            val authorizedClient = readFile()
                .find { it.clientRegistrationId == clientRegistrationId && it.principalName == principalName }
                ?: return null
            @Suppress("UNCHECKED_CAST")
            return OAuth2AuthorizedClient(
                clientRegistrationRepository.findByRegistrationId(authorizedClient.clientRegistrationId),
                principalName,
                authorizedClient.accessToken,
                authorizedClient.refreshToken,
            ) as T
        } finally {
            lock.readLock().unlock()
        }
    }

    override fun saveAuthorizedClient(authorizedClient: OAuth2AuthorizedClient, principal: Authentication) {
        mutateStore { clients ->
            val filtered = clients.asSequence()
                .filter { it.clientRegistrationId != authorizedClient.clientRegistration.registrationId || it.principalName != principal.name }
            val newClient = AuthorizedClient(
                authorizedClient.clientRegistration.registrationId,
                principal.name,
                authorizedClient.accessToken,
                authorizedClient.refreshToken,
            )
            buildList {
                addAll(filtered)
                add(newClient)
            }
        }
    }

    override fun removeAuthorizedClient(clientRegistrationId: String, principalName: String) {
        if (clientRegistrationRepository.findByRegistrationId(clientRegistrationId) == null) {
            return
        }
        mutateStore { clients ->
            clients.asSequence()
                .filter { it.clientRegistrationId != clientRegistrationId || it.principalName != principalName }
                .toList()
        }
    }

    private fun mutateStore(mutate: (List<AuthorizedClient>) -> List<AuthorizedClient>) {
        lock.writeLock().lock()
        try {
            val mutated = mutate(readFile())
            path.writer(UTF_8, CREATE, TRUNCATE_EXISTING).use {
                mapper.writeValue(it, mutated)
            }
        } finally {
            lock.writeLock().unlock()
        }
    }

    private fun readFile() = if (!path.exists()) emptyList() else path.reader(UTF_8).use { read(it) }

    internal fun read(fileReader: Reader): List<AuthorizedClient> = mapper.readValue(fileReader)

    data class AuthorizedClient(
        val clientRegistrationId: String,
        val principalName: String,
        val accessToken: OAuth2AccessToken,
        val refreshToken: OAuth2RefreshToken?,
    )
}

class TokenTypeSerializer : StdScalarSerializer<TokenType>(TokenType::class.java) {
    override fun serialize(value: TokenType, gen: JsonGenerator, provider: SerializerProvider) {
        gen.writeString(value.value)
    }
}

class TokenTypeDeserializer : FromStringDeserializer<TokenType>(TokenType::class.java) {
    override fun _deserialize(value: String, ctxt: DeserializationContext): TokenType {
        check(value == TokenType.BEARER.value)
        return TokenType.BEARER
    }
}

class OAuth2AccessTokenMixin
@JsonCreator
@Suppress("UnusedPrivateProperty")
constructor(
    @JsonProperty("tokenType")
    @JsonSerialize(using = TokenTypeSerializer::class)
    @JsonDeserialize(using = TokenTypeDeserializer::class)
    tokenType: TokenType,
    @JsonProperty("tokenValue") tokenValue: String,
    @JsonProperty("issuedAt") issuedAt: Instant,
    @JsonProperty("expiresAt") expiresAt: Instant,
    @JsonProperty("scopes") scopes: Set<String>,
)

class OAuth2RefreshTokenMixin
@JsonCreator
@Suppress("UnusedPrivateProperty")
constructor(
    @JsonProperty("tokenValue") tokenValue: String,
    @JsonProperty("issuedAt") issuedAt: Instant,
    @JsonProperty("expiresAt") expiresAt: Instant?,
)
