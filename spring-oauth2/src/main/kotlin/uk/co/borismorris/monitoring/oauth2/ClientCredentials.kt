package uk.co.borismorris.monitoring.oauth2

import org.springframework.beans.factory.InitializingBean
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.AuthorityUtils
import org.springframework.security.core.context.SecurityContextHolder

interface ClientCredentials : InitializingBean {
    val username: String
    val password: String
    fun authorities(): List<GrantedAuthority> = AuthorityUtils.NO_AUTHORITIES

    override fun afterPropertiesSet() {
        SecurityContextHolder.getContext().authentication = asAuthentication()
    }
}

fun ClientCredentials.asAuthentication(): UsernamePasswordAuthenticationToken = UsernamePasswordAuthenticationToken.authenticated(username, password, authorities())
