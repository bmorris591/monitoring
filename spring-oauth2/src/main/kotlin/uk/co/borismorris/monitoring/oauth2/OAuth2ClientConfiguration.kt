package uk.co.borismorris.monitoring.oauth2

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.ObjectProvider
import org.springframework.beans.factory.annotation.Value
import org.springframework.beans.factory.config.BeanDefinition
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.boot.autoconfigure.security.oauth2.client.OAuth2ClientProperties
import org.springframework.boot.autoconfigure.security.oauth2.client.OAuth2ClientPropertiesMapper
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Scope
import org.springframework.http.converter.FormHttpMessageConverter
import org.springframework.security.core.Authentication
import org.springframework.security.oauth2.client.AuthorizedClientServiceOAuth2AuthorizedClientManager
import org.springframework.security.oauth2.client.AuthorizedClientServiceOAuth2AuthorizedClientManager.DefaultContextAttributesMapper
import org.springframework.security.oauth2.client.OAuth2AuthorizationContext.PASSWORD_ATTRIBUTE_NAME
import org.springframework.security.oauth2.client.OAuth2AuthorizationContext.USERNAME_ATTRIBUTE_NAME
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientManager
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientProvider
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientProviderBuilder
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService
import org.springframework.security.oauth2.client.endpoint.RestClientRefreshTokenTokenResponseClient
import org.springframework.security.oauth2.client.http.OAuth2ErrorResponseErrorHandler
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository
import org.springframework.security.oauth2.client.registration.InMemoryClientRegistrationRepository
import org.springframework.security.oauth2.client.web.client.OAuth2ClientHttpRequestInterceptor
import org.springframework.security.oauth2.client.web.client.OAuth2ClientHttpRequestInterceptor.ClientRegistrationIdResolver
import org.springframework.security.oauth2.core.http.converter.OAuth2AccessTokenResponseHttpMessageConverter
import org.springframework.web.client.RestClient
import java.nio.file.Path

@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties(OAuth2ClientProperties::class)
class OAuth2ClientConfiguration {

    @Bean
    fun clientRegistrationRepository(properties: OAuth2ClientProperties) = InMemoryClientRegistrationRepository(ArrayList(OAuth2ClientPropertiesMapper(properties).asClientRegistrations().values))

    @Bean
    fun oauth2AuthorizedClientService(
        registrationRepository: ClientRegistrationRepository,
        mapper: ObjectMapper,
        @OauthCachePath path: Path,
    ) = JsonFileOauth2AuthorizedClientService(registrationRepository, mapper, path)

    @Bean
    fun oauth2AuthorizedClientManager(
        registrationRepository: ClientRegistrationRepository,
        authorizedClientService: OAuth2AuthorizedClientService,
        customizers: ObjectProvider<AuthorizedClientServiceOAuth2AuthorizedClientManagerCustomizer>,
    ): OAuth2AuthorizedClientManager = AuthorizedClientServiceOAuth2AuthorizedClientManager(
        registrationRepository,
        authorizedClientService,
    ).apply {
        customizers.forEach { it.customize(this) }
    }.let {
        LockingOAuth2AuthorizedClientManager(it)
    }

    @Bean
    fun oAuth2AuthorizedClientProviderClientManagerCustomizer(authorizedClientProvider: OAuth2AuthorizedClientProvider) = AuthorizedClientServiceOAuth2AuthorizedClientManagerCustomizer {
        it.setAuthorizedClientProvider(authorizedClientProvider)
    }

    @Bean
    fun contextAttributesClientManagerCustomizer() = AuthorizedClientServiceOAuth2AuthorizedClientManagerCustomizer {
        val mapper = DefaultContextAttributesMapper()
        it.setContextAttributesMapper { request ->
            mapper.apply(request) + request.principal.toAttributes()
        }
    }

    private fun Authentication.toAttributes() = mapOf(
        USERNAME_ATTRIBUTE_NAME to principal,
        PASSWORD_ATTRIBUTE_NAME to credentials,
    )

    @Bean
    @ConditionalOnMissingBean
    fun clientRegistrationIdResolver(@Value("\${spring.security.oauth2.client.default-client}") defaultClient: String) = ClientRegistrationIdResolver { _ -> defaultClient }

    @Bean
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    fun oauthFilter(
        authorizedClientManager: OAuth2AuthorizedClientManager,
        clientRegistrationIdResolver: ClientRegistrationIdResolver,
    ) = OAuth2ClientHttpRequestInterceptor(authorizedClientManager).apply {
        setClientRegistrationIdResolver(clientRegistrationIdResolver)
    }

    @Bean
    @ConditionalOnMissingBean
    fun oAuth2AccessTokenResponseHttpMessageConverter() = OAuth2AccessTokenResponseHttpMessageConverter()

    @Bean
    fun defaultOauth2RestClientCustomizer(
        tokenConverter: OAuth2AccessTokenResponseHttpMessageConverter,
    ) = Oauth2RestClientCustomizer {
        it.messageConverters { messageConverters ->
            messageConverters.clear()
            messageConverters.add(FormHttpMessageConverter())
            messageConverters.add(tokenConverter)
        }.defaultStatusHandler(OAuth2ErrorResponseErrorHandler())
    }

    @Bean
    @ConditionalOnMissingBean
    fun authorisedClientProvider(
        restClientBuilder: RestClient.Builder,
        customizers: ObjectProvider<Oauth2RestClientCustomizer>,
    ): OAuth2AuthorizedClientProvider {
        customizers.forEach {
            it.customize(restClientBuilder)
        }
        @Suppress("DEPRECATION")
        return OAuth2AuthorizedClientProviderBuilder.builder()
            .refreshToken { it.restClient(restClientBuilder.build()) }
            .password { it.restClient(restClientBuilder.build()) }
            .build()
    }
}

fun OAuth2AuthorizedClientProviderBuilder.RefreshTokenGrantBuilder.restClient(restClient: RestClient) {
    accessTokenResponseClient(
        RestClientRefreshTokenTokenResponseClient().apply {
            setRestClient(restClient)
        },
    )
}

fun OAuth2AuthorizedClientProviderBuilder.PasswordGrantBuilder.restClient(restClient: RestClient) {
    accessTokenResponseClient(
        RestClientPasswordTokenResponseClient(restClient),
    )
}
