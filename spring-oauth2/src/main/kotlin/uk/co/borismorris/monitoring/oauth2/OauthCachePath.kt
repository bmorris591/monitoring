package uk.co.borismorris.monitoring.oauth2

import org.springframework.beans.factory.annotation.Qualifier
import kotlin.annotation.AnnotationTarget.FIELD
import kotlin.annotation.AnnotationTarget.FUNCTION
import kotlin.annotation.AnnotationTarget.VALUE_PARAMETER

@Target(FIELD, VALUE_PARAMETER, FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
@Qualifier
annotation class OauthCachePath
