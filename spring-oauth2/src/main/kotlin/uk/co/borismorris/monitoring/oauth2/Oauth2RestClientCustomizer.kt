package uk.co.borismorris.monitoring.oauth2

import org.springframework.web.client.RestClient

fun interface Oauth2RestClientCustomizer {
    fun customize(restClientBuilder: RestClient.Builder)
}
