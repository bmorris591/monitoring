package uk.co.borismorris.monitoring.oauth2

import org.springframework.boot.autoconfigure.AutoConfigurationImportFilter
import org.springframework.boot.autoconfigure.AutoConfigurationMetadata
import org.springframework.boot.autoconfigure.security.oauth2.client.servlet.OAuth2ClientAutoConfiguration
import uk.co.borismorris.monitoring.logging.KotlinLogging

private val logger = KotlinLogging.logger {}
private val SHOULD_SKIP = setOf<String>(OAuth2ClientAutoConfiguration::class.java.canonicalName)

class OAuth2AutoConfigurationFilter : AutoConfigurationImportFilter {
    override fun match(
        autoConfigurationClasses: Array<out String?>?,
        autoConfigurationMetadata: AutoConfigurationMetadata?,
    ) = autoConfigurationClasses?.asSequence().orEmpty().map {
        if (it.isNullOrBlank()) {
            true
        } else if (SHOULD_SKIP.contains(it)) {
            logger.info("Skipping $it")
            false
        } else {
            true
        }
    }.toList().toBooleanArray()
}
