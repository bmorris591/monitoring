package uk.co.borismorris.monitoring.oauth2

import org.springframework.security.oauth2.client.OAuth2AuthorizeRequest
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientManager
import java.util.concurrent.locks.ReentrantLock

class LockingOAuth2AuthorizedClientManager(val delegate: OAuth2AuthorizedClientManager) : OAuth2AuthorizedClientManager {
    private val lock = ReentrantLock()

    override fun authorize(authorizeRequest: OAuth2AuthorizeRequest): OAuth2AuthorizedClient? {
        lock.lock()
        try {
            return delegate.authorize(authorizeRequest)
        } finally {
            lock.unlock()
        }
    }
}
