plugins {
    `java-library`
}

group = "uk.co.borismorris.monitoring.influx-client"

dependencies {
    api(projects.restclientInfluxClient)
    api("org.testcontainers:junit-jupiter")
    api("org.testcontainers:influxdb")
    api("org.springframework.boot:spring-boot-testcontainers")
}
