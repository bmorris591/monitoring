package uk.co.borismorris.monitoring.influxclient.testcontainers

import org.springframework.boot.testcontainers.service.connection.ContainerConnectionDetailsFactory
import org.springframework.boot.testcontainers.service.connection.ContainerConnectionSource
import org.testcontainers.containers.InfluxDBContainer
import uk.co.borismorris.monitoring.influxclient.InfluxdbConnectionDetails

class InfluxDbContainerConnectionDetailsFactory : ContainerConnectionDetailsFactory<InfluxDBContainer<*>, InfluxdbConnectionDetails>() {
    override fun getContainerConnectionDetails(source: ContainerConnectionSource<InfluxDBContainer<*>>): InfluxdbConnectionDetails = InfluxDbContainerConnectionDetails(source)

    private class InfluxDbContainerConnectionDetails(source: ContainerConnectionSource<InfluxDBContainer<*>>) :
        ContainerConnectionDetails<InfluxDBContainer<*>>(source),
        InfluxdbConnectionDetails {
        override val url: String?
            get() = container.url
        override val token: String?
            get() = container.adminToken.orElse(null)
        override val bucket: String?
            get() = container.bucket
        override val org: String?
            get() = container.organization
    }
}
