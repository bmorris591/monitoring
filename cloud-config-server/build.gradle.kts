plugins {
    monitoring.`application-conventions`
    monitoring.`jib-conventions`
}

group = "uk.co.borismorris.cloudconfig"

dependencies {
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-data-jdbc")
    implementation("org.springframework.cloud:spring-cloud-config-server")

    implementation(libs.jdbc.micrometer)
    implementation(libs.mariadb.jdbc)
    implementation("com.zaxxer:HikariCP")
    implementation("org.flywaydb:flyway-mysql")

    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation(projects.wiremockSpringBoot)

    testImplementation("org.testcontainers:mariadb")
}

application {
    mainClass.set("uk.co.borismorris.cloudconfig.MonitoringConfigServerKt")
}

jib {
    container {
        environment = mapOf("SPRING_PROFILES_ACTIVE" to "prod,composite")
    }
}
