package uk.co.borismorris.cloudconfig

import org.hamcrest.Matchers.emptyString
import org.hamcrest.Matchers.equalTo
import org.hamcrest.Matchers.not
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.get
import org.springframework.transaction.annotation.Transactional

@SpringBootTest(
    properties = [
        "spring.datasource.url=jdbc:tc:mariadb:latest:///spring-config",
    ],
    webEnvironment = SpringBootTest.WebEnvironment.MOCK,
)
@AutoConfigureMockMvc
@ActiveProfiles("test", "composite")
@Transactional
class MonitoringConfigServerTest {
    @Autowired
    private lateinit var jdbcTemplate: JdbcTemplate

    @Autowired
    private lateinit var mockMvc: MockMvc

    @BeforeEach
    fun setup() {
        jdbcTemplate.batchUpdate(
            "INSERT INTO property(`application`, `profile`, `label`, `key`, `value`) VALUES(?, ?, ?, ?, ?)",
            listOf(
                arrayOf("test", "production", "main", "test.property.one", "value_one"),
                arrayOf("test", "uat", "main", "test.property.two", "value_two"),
            ),
        )
    }

    @Test
    fun `Get config metadata`() {
        mockMvc.get("/test/production,uat")
            .andExpect {
                status { isOk() }
                content {
                    jsonPath(
                        "$.propertySources[?(@.name=='test-production')].source",
                        equalTo<List<Any>>(listOf(mapOf("test.property.one" to "value_one"))),
                    )
                    jsonPath(
                        "$.propertySources[?(@.name=='test-uat')].source",
                        equalTo<List<Any>>(listOf(mapOf("test.property.two" to "value_two"))),
                    )
                }
            }
    }

    @Test
    fun `Get config yaml with profile`() {
        mockMvc.get("/test-production.yml")
            .andExpect {
                status { isOk() }
                content {
                    string(not(emptyString()))
                }
            }
    }
}
