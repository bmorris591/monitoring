package uk.co.borismorris.cloudconfig

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.config.server.EnableConfigServer

@SpringBootApplication(proxyBeanMethods = false)
@EnableConfigServer
class MonitoringConfigServer

@Suppress("SpreadOperator")
fun main(args: Array<String>) {
    runApplication<MonitoringConfigServer>(*args)
}
