CREATE TABLE `property`
(
    `application` CHAR(20)     NOT NULL,
    `profile`     CHAR(20)     NOT NULL,
    `label`       CHAR(20)     NOT NULL,
    `key`         VARCHAR(255) NOT NULL,
    `value`       VARCHAR(255) NOT NULL,
    PRIMARY KEY (`application`, `profile`, `label`, `key`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
